<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
		//FOR ADMINLTE TEMPLATE
        'css/bootstrap.min.css',
        //'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css',
        //'https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css',
        //'css/AdminLTE.min.css',
		'css/AdminLTE.css',
        'css/_all-skins.min.css',
        'css/blue.css',
        'css/morris.css',
        'css/jquery-jvectormap-1.2.2.css',
        'css/datepicker3.css',
        'css/daterangepicker.css',
        'css/datatables/dataTables.bootstrap.css',
        'css/bootstrap3-wysihtml5.min.css',
		
		//Tentacular INCLUDED css
		'css/forms/simple.css',
        'css/fontawesome/css/font-awesome.css',
		'css/fontawesome/css/main.css',
        //'css/fontawesome/css/font-awesome.min.css',
        'css/hover.css',
    ];
    public $js = [
		//'js/bootstrap.min.js',
		
        'js/datatables/jquery.dataTables.min.js',
        'js/datatables/dataTables.bootstrap.min.js',
        'plugins/slimScroll/jquery.slimscroll.min.js',
		'plugins/fastclick/fastclick.js',
		'dist/js/app.min.js',
		'dist/js/demo.js',
		'bootstrap/js/bootstrap.min.js',
		
		
		
		
		
    ];
	
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}



    
    
    
