<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SellAirtime */

$this->title = $model->sell_id;
$this->params['breadcrumbs'][] = ['label' => 'Sell Airtimes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sell-airtime-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->sell_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->sell_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'sell_id',
            'airtime_brand',
            'amount',
            'airtime_pin',
            'status',
            'option',
        ],
    ]) ?>

</div>
