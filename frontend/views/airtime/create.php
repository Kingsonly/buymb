<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SellAirtime */

$this->title = 'Create Sell Airtime';
$this->params['breadcrumbs'][] = ['label' => 'Sell Airtimes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sell-airtime-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
