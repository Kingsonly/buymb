<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use frontend\models\PlanType;
use frontend\models\Order;
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\models\SellAirtime */
/* @var $form ActiveForm */
$modelOrder = new Order();
?>
<style>
	#cash,#data{
		display: none;
	}
	#BtnLoader2{
		display:none;
	}
</style>
<div class="airtime-sellform">

    <?php $form = ActiveForm::begin(['id'=>'airtimeForm']); ?>

		<?= $form->field($model, 'airtime_brand')->dropDownList(ArrayHelper::map(planType::find()->where(['type'=>'airtime'])->all(), 'type_id', 'network'),[ 'prompt'=> 'Select...', 'class' => 'form-control']) ?>
        <?= $form->field($model, 'amount') ?>
        <?= $form->field($model, 'airtime_pin') ?>
        <? $payment = ['cash'=>'Cash','data'=>'data','wallet'=>'Wallet'] ?>
	
		<?= $form->field($model, 'option')->dropDownList($payment,[ 'prompt'=> 'Select...', 'class' => 'form-control', 'id' => 'option']) ?>
	<div id="cash">
		<?= $form->field($model, 'account_name',['options'=>['id'=>'nams']]) ?>
		<?= $form->field($model, 'account_number',['options'=>['id'=>'nams']]) ?>
	</div>
	<div id="data">
		<?= $form->field($modelOrder, 'plan_id_selector')->dropDownList(ArrayHelper::map(planType::find()->where(['type'=>'airtime'])->all(), 'type_id', 'network'),[ 'prompt'=> 'Select...', 'class' => 'form-control', 'id' => 'plantype2']) ?>
    
	
		<?= $form->field($modelOrder, 'plan_id')->widget(DepDrop::classname(), [
			'options'=>['id' => 'plan_id2'],
			'pluginOptions' => [
				'depends' => ['plantype2'],
				'placeholder' => 'Select...',
				'url'=>Url::to(['/data/list-items'])
			]
		]);
		?>
	</div>
    
        <div class="form-group">
           <?= Html::submitButton($model->isNewRecord ? '<span id="submitButtonText2">Submit</span>'.Html::img("@web/images/45.gif", ["alt" => "loader","id"=>"BtnLoader2", "class" => "user-image23" ]).' <span id="feedback2"><span>' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-basic' : 'btn btn-basic','id'=>'submitbtn2']) ?>

        </div>
    <?php ActiveForm::end(); ?>

</div><!-- airtime-sellform -->

<?
$walletUrl = Url::to(['order/converttowallet']);
$cashUrl = Url::to(['order/converttocash']);
$dataUrl = Url::to(['order/converttodata']);
$rate = Yii::$app->params['rate'];
$sell = <<<JS

$('#option').change(function(){
	value = $(this).val();
	if(value == 'data'){
		$('#data').show();
		$('#cash').hide();
	} else if (value == 'cash'){
		$('#cash').show();
		$('#data').hide();
	} else{
	 $('#data').hide();
	 $('#cash').hide();
	}
});


$('#airtimeForm').on('beforeSubmit', function (e) {
	$('#submitButtonText2').hide();
 	$('#BtnLoader2').show();
    var \$form = $(this);
	var planid = $('#plan_id').val();
	var option = $('#option').val();
	amount = $('#sellairtime-amount').val();
	getPer = ('$rate' / 100) * amount;
	mainActualAmount = amount - getPer;
	
	
	if(option == 'cash'){
		$.post('$cashUrl',\$form.serialize())
		.always(function(result){
	
			
   			if(result==1){
	   			$('#BtnLoader2').hide();
	   			$(document).find('#feedback2').html('Sent').show();
				$('#details2').modal('show').find('#actualamount2').html(mainActualAmount);
    		}else{
				$('#BtnLoader').hide();
				$(document).find('#feedback2').html('An error ocured').show();
	
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	} else if (option == 'data'){
		var plan = $('#plan_id2').val();
		var amount = $('#sellairtime-amount').val();
			$.post('$dataUrl'+'?plan='+plan+'& amount='+amount,\$form.serialize())
		.always(function(result){
	
			$(document).find('#loader').hide();
   			if(result==1){
			$('#BtnLoader2').hide();
	   
	   			$(document).find('#feedback2').html('sent').show();
				$('#details2').modal('show').find('#actualamount2').html(mainActualAmount);
    		}else if (result == 2){
				$('#BtnLoader').hide();
				$(document).find('#feedback2').html('your balance would be added to your wallet after pin confirmation .').show();
			}else{
				$('#BtnLoader').hide();
				$(document).find('#feedback2').html('Airtime amount does not match selected data plan').show();
	
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	} else if (option == 'wallet'){
			var pin = $('#voucher').val();
			$.post('$walletUrl',\$form.serialize())
		.always(function(result){
	
			$(document).find('#loader').hide();
   			if(result==1){
			$('#BtnLoader2').hide();
	   
	   			$(document).find('#feedback2').html('sent').show();
				$('#details2').modal('show').find('#actualamount2').html(mainActualAmount);
    		}else{
				$('#BtnLoader').hide();
				$(document).find('#feedback').html('An error ocured').show();
	
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	} else{
		alert('no option selected');
	}
    
	
	setTimeout(function(){ 
	$(document).find('#feedback2').hide();
	$(document).find('#BtnLoader2').hide();
	$(document).find('#submitButtonText2').show();
	$.pjax.reload({container:"#sell_airtime",async: false}); 
	$("#example2").DataTable({
        "aaSorting": [],
		"pagingType": "simple",
		"responsive": "true",
		
    });
	}, 5000);
    return false;
 	
    
});


JS;
 
$this->registerJs($sell);
?>



