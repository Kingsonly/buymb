<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\bootstrap\Modal;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Airtime';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
	.displays{
		display: none;
	}
	#sellbutton,#buybutton{
		cursor: pointer;
	} 
</style>
<div class="dasgboard-index">
	<h1 style="padding:0px 15px;">
		Buy Airtime / Sell Airtime
	</h1>

	<section class="content">
		<div class="row">
		 
		<div class="col-lg-6 col-xs-12" id="buybutton">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>Buy Airtime</h3>

              <p style="height:20px"> </p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a   class="small-box-footer">Click Here <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
		  
		  
		
			<div class="col-lg-6 col-xs-12" id="sellbutton">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>Sell Airtime</h3>

              <p style="height:20px"></p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a class="small-box-footer">Click Here <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
		  
        <!-- ./col -->
		 
		 
      </div>
      <!-- Small boxes (Stat box) -->
      <div class="row" id="buy">
		  <div class="col-sm-12 col-lg-12  ">
			  <div class="small-box bg-aqua text-center">
			  	<h3 > Buy Airtime</h3>
			  </div>
			 
		  </div>
		  
		  <div class="col-sm-6">
			  <?= $this->render('orderform', [
				'model' => $model,
				'contactModel' => $contactModel,
			]) ?>
		  </div>
		  
		  <div class="col-sm-6"> 
			  <div class="box" id = "project-title">
				  <div class="box-body" id = "project-title">
					  <?php Pjax::begin(['id' => 'buy_airtime']) ?>
			  	<table id="example1" class="table table-bordered table-striped">
							
                        	<thead>
                            <tr>
                              <th>SN</th>
                              <th>Plan</th>
                              <th>Payment Option</th>
                              <th>Status</th>
                              <th>Date</th>
                  
                            </tr>
                        
							</thead>
                        	<tbody>
                            <? $sn=1; foreach($myOrder as $k=>$v){ ?>
								
                                <tr>
								
                                  <td class="projecturl"><?=$sn;?></td>
                                  <td class="projecturl"><?=$v['plan']['plan_name'];?></td>
                                  <td class="projecturl"><?=$v['payment_mode'];?></td>
                                  <td class="projecturl <?= $v['status'] == 1?'danger':'success' ?>">
									  <?= $v['status'] == 1?'Pending':'Approved' ?>
									</td>
                                  <td class="projecturl"><?=$v['date_created'];?></td>

                                </tr>
								
                            <? $sn++; }?>
                   
                  
							</tbody>
                        	<tfoot>
								<tr>
								  <th>SN</th>
								  <th>Plan</th>
								  <th>Payment Option</th>
								  <th>Status</th>
								  <th>Date</th>

								</tr>
                			</tfoot>
                
                    
						</table>
					  <?php Pjax::end() ?>
			  </div>
			  </div>
		  </div>
        <!-- ./col -->
      </div>
		<div class="row displays" id="sell">
		  <div class="col-sm-12 col-lg-12  ">
			  <div class="small-box bg-green text-center">
			  	<h3 > Sell Airtime</h3>
			  </div>
			 
		  </div>
		
		  <div class="col-sm-6">
			  <?= $this->render('sellform', [
				'model' => $SellAirtime,
			]) ?>
		  </div>
		
		  
		  <div class="col-sm-6"> 
			  <div class="box" id = "project-title">
				  <div class="box-body" id = "project-title">
				<?php Pjax::begin(['id' => 'sell_airtime']); ?>
			  		<table id="example2" class="table table-bordered table-striped">
							
                        	<thead>
                            <tr>
                              
                                  <th>Network</th>
                                  <th>Amount</th>
                                  <th>Pin</th>
                                  <th>Converted To</th>
                                  <th>
									 Status
								  </th>
                  
                            </tr>
                        
							</thead>
                        	<tbody>
                            <? $sn=1; foreach($mySoldAirtime as $k=>$v){ ?>
								
                                <tr>
								
                                  <td class="projecturl"><?=$v['plantype']['network']; ?> </td>
                                  <td class="projecturl"><?=$v['amount']; ?></td>
                                  <td class="projecturl"><?=$v['airtime_pin']; ?></td>
                                  <td class="projecturl"><?=$v['option']; ?></td>
                                  <td class="projecturl">
									 <?=$v['status']; ?>
								  </td>
                                  
									

                                </tr>
								
                            <? $sn++; }?>
                   
                  
							</tbody>
                        	<tfoot>
								<tr>
									 <th>Network</th>
                                  <th>Amount</th>
                                  <th>Pin</th>
                                  <th>Converted To</th>
                                  <th>
									 Status
								  </th>

								</tr>
                			</tfoot>
                
                    
						</table>
				<?php Pjax::end(); ?>
			  </div>
			  </div>
		  </div>
        <!-- ./col -->
      </div>
	</section>
    
</div>

<? 
		Modal::begin([
			'header' =>'<h1 id="headers">Instructions</h1>',
			'id' => 'details',
			'size' => 'modal-md',  
		]);
?>
<div id="formcontent">
<div>You are expected to pay <em id="actualamount"></em></div>
<div>
<h4>Bank Details</h4>
<stong>Account Name </stong>Emeka Uchechukwu Michael<BR><HR/>
<stong>GTBANK</stong><em> 0215001020</em><BR><HR/>
<stong> ACCESS BANK</stong><em> 0042461814</em><BR><HR/>
<stong>FIRST BANK</stong><em> 3119596327</em><BR><HR/>
</div>
<div>
	<i>
		Please after payment send name of depositor, bank paid to , to 09037072904 for confirmation ( if you paid with ATM or bank transfer code kindly indicate).
	</i>
</div>
</div>

<?
	Modal::end();
?>
	
	
<? 
		Modal::begin([
			'header' =>'<h1 id="headers2">Airtime Sales Instructions</h1>',
			'id' => 'details2',
			'size' => 'modal-md',  
		]);
?>
<div id="formcontent2">


<div>
	<i>
		<div>Call or Whatsapp us for speedy reply (09039072904)<em><strong>Note</strong></em><small>Thank you for attempting to sell airtime to us Note: if airtime is approved you would recive an equivalent of   <em id="actualamount2"></em> naira</small></div>
<div>
	</i>
</div>
</div>

<?
	Modal::end();
?>






<?php 
	
$invoiceform = <<<JS
$("#example1").DataTable({
        "aaSorting": [],
		"pagingType": "simple",
		"responsive": "true",
		
    });

$("#example2").DataTable({
        "aaSorting": [],
		"pagingType": "simple",
		"responsive": "true",
		
    });
$('#sellbutton').click(function(){

  var sell = $('#sell');
  var buy = $('#buy');
  buy.removeClass('displays');
  sell.removeClass('displays');
  buy.addClass('displays');
  $('html, body').animate({
        scrollTop: sell.offset().top
    }, 2000);
  
});

$('#buybutton').click(function(){
  var sell = $('#sell');
  var buy = $('#buy');
  sell.removeClass('displays');
  buy.removeClass('displays');
  sell.addClass('displays');
  
  $('html, body').animate({
        scrollTop: buy.offset().top
    }, 2000);
});

$('#invoiceform').on('beforeSubmit', function (e) {
	$('#invoiceformbuttonText').hide();
 	$('#invoiceloader').show();
    var \$form = $(this);
	
    $.post(\$form.attr('action'),\$form.serialize())
    .always(function(result){
	
	$(document).find('#invoiceloader').hide();
   if(result.sent=='sent'){
	   
	   $(document).find('#invoiceloader1').html(result).show();
	    $(document).find('#flash').html(result.message).show();
	   
	 	$(document).find('.modal').modal('hide');
	   $(document).find('#invoiceform').trigger('reset');
	   $("html, body").delay(200).animate({
        scrollTop: $('#flash').offset().top
		
    }, 2000);
		
	   
	 	$.pjax.reload({container:"#supplier_reload",async: false
}); 
	  $("#example2").DataTable({
        "aaSorting": [],
		"pagingType": "simple",
		"responsive": "true",
		
    });
	
    }else{
    $(document).find('#invoiceloader1').html(result).show();
	
    }
    }).fail(function(){
    console.log('Server Error');
    });
	
	setTimeout(function(){ 
	alert('yes');
		$(document).find('#invoiceloader').hide();
		$(document).find('#invoiceloader1').hide();
		$(document).find('#invoicebuttonText').show();
		
		
		

	}, 5000);
    return false;
	
	

    
    
    
});
JS;
 
$this->registerJs($invoiceform);
?>
