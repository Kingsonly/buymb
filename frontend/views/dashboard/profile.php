<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Profile';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dasgboard-index">

	<section class="content">
		<div class="row">
      		<!-- Small boxes (Stat box) -->
			<div class="col-sm-4">
			put the content here
				<table>
				<? foreach($userDetails as $k => $v){?>
				<tr>
					<td>
					</td>
					<td>
					</td>
				</tr>
				<? }?>
				</table>
			</div>
			<div class="col-sm-8">
			
				<?php $form = ActiveForm::begin(['id'=>'profile']); ?>

				<?= $form->field($personModel, 'first_name')->textInput() ?>

				<?= $form->field($personModel, 'last_name')->textInput() ?>

				<?= $form->field($personModel, 'account_number')->textInput() ?>
				<?= $form->field($contactModel, 'email')->textInput() ?>
				<?= $form->field($contactModel, 'phone_number')->textInput() ?>


				<div class="form-group">
					<?= Html::submitButton($personModel->isNewRecord ? 'Create' : 'Update', ['class' => $personModel->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
				</div>
			</div>
			

			<?php ActiveForm::end(); ?>
		</div>
	
	<h2>Fund wallet</h2>
	
	<div class="row">
		
		<div class="col-sm-4">
			content goes here 
		</div>
		<div class="col-sm-8">
		
      <!-- Small boxes (Stat box) -->
      	<?php $form = ActiveForm::begin(['id'=>'wallet']); ?>

		<?= $form->field($userModel, 'wallet')->textInput(['id'=>'wallets']) ?>

		
		

		<div class="form-group">
			<?= Html::submitButton($userModel->isNewRecord ? 'Fund' : 'Fund', ['class' => $userModel->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		</div>

		<?php ActiveForm::end(); ?>
	</div>
	
	</div>
</section>
    
</div>

<?
$profileUrl = Url::to(['dashboard/profilesave']);
$fundWalet = Url::to(['dashboard/fundwallet']);
$this->registerJsFile(
    'https://js.paystack.co/v1/inline.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
$backendIndexAjax = <<<JS

  
$('#profile').on('beforeSubmit', function (e) {
	$('#submitButtonTextpt').hide();
 	$('#BtnLoaderpt').show();
    var \$form = $(this);
	
	
	
	$.post('$profileUrl',\$form.serialize())
	.always(function(result){


		if(result.status==1){
			$('#BtnLoaderpt').hide();
			$(document).find('#feedbackpt').html('Sent').show();
		}else{
			$('#BtnLoaderpt').hide();
			$(document).find('#feedbackpt').html('Not enought funds in wallet').show();

		}
	}).fail(function(){
		console.log('Server Error');
	});
	 
    
	
	setTimeout(function(){ 
	$(document).find('#feedbackpt').hide();
	$(document).find('#BtnLoaderpt').hide();
	$(document).find('#submitButtonTextpt').show();
	$.pjax.reload({container:"#plan_type_pjax",async: false}); 
	}, 5000);
    return false;
 	
    
});

$('#wallet').on('beforeSubmit', function (e) {
	$('#submitButtonTextpt').hide();
 	$('#BtnLoaderpt').show();
    var \$form = $(this);
	
		
	
		var handler = PaystackPop.setup({
			
			  key: 'pk_live_60e06110fdefa3e025e94acf315c0baf26f2ca63',
			  email: 'kingsonly13c@gmail.com',
			  amount: $('#wallets').val()+'00',
			  
			  metadata: {
				 custom_fields: [
					{
						display_name: "Mobile Number",
						variable_name: "mobile_number",
						value: "+2348012345678"
					}
				 ]
			  },
			  callback: function(response){
				  
				  
				  $.post('$fundWalet',\$form.serialize())
		.always(function(result){
	
			
   			if(result==1){
	   			$('#BtnLoader').hide();
	   			$(document).find('#feedback').html('Sent').show();
    		}else{
				$('#BtnLoader').hide();
				$(document).find('#feedback').html('Not enought funds in wallet').show();
	
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
		alert('yes');
			  },
			  onClose: function(){
				  //alert('window closed');
			  }
		});
		handler.openIframe();
 
	
	
	
	setTimeout(function(){ 
	$(document).find('#feedbackpt').hide();
	$(document).find('#BtnLoaderpt').hide();
	$(document).find('#submitButtonTextpt').show();
	$.pjax.reload({container:"#plan_type_pjax",async: false}); 
	}, 5000);
    return false;
 	
    
});



JS;
 
$this->registerJs($backendIndexAjax);
?>


