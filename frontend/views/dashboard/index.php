<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\bootstrap\Modal;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
	.dashicons{
		width: 100%;
		
	}
	.dashiconheightlast{
		height: 160px;
	}
</style>
<div class="dasgboard-index">

	<section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">

		  <a href='<?=Url::to(['data/index']) ?>' >

			  <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
			  <?=Html::img("@web/images/databundle.png", ["alt" => "loader","id"=>"", "class" => "dashicons" ]) ?>
            <div class="inner">
             <!-- <h3>Buy Data Bundle</h3>-->

              <p > Start enjoying this very low rates for your internet browsing databundle.
</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href='<?=Url::to(["data/index"]) ?>'  class="small-box-footer">Buy Now <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
		  </a>
		  

		  <a href='<?=Url::to(['airtime/index']) ?>' >

			  <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
              
              
			  <?=Html::img("@web/images/airtime.png", ["alt" => "loader","id"=>"", "class" => "dashicons" ]) ?>
            <div class="inner">
              <!--<h3>Air Time</h3>-->

              <p>Get up to 5% discount instantly when you purchase airtime.</p>
              Also you can sell airtime on our platform
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="<?=Url::to(['airtime/index']) ?>" class="small-box-footer">Buy/Sell <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
		  </a>
        <!-- ./col -->

		  <a href='<?=Url::to(['voucher/index']) ?>' >

			  <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
           
			   <?=Html::img("@web/images/moneyvoucher.jpg", ["alt" => "loader","id"=>"", "class" => "dashicons" ]) ?>
			  
            <div class="inner">
              <h3>Voucher</h3>

              <p>Buy/convert</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="<?=Url::to(['voucher/index']) ?>" class="small-box-footer">Buy Now <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
		  </a>
        <!-- ./col -->

		  <a href='<?=Url::to(['#']) ?>' class="bitcoin" >

			  <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
              
			  <?=Html::img("@web/images/opengraph.png", ["alt" => "loader","id"=>"", "class" => "dashicons dashiconheightlast" ]) ?>
            <div class="inner">
              <h3>Bit Coin</h3>

              <p>Buy/Sell Coming Soon...</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="<?=Url::to(['#']) ?>" class="small-box-footer bitcoin">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
		  </a>
        <!-- ./col -->
      </div>
	</section>
    
</div>

<? 
		Modal::begin([
			'header' =>'<h1 id="headers">Instructions</h1>',
			'id' => 'details',
			'size' => 'modal-md',  
		]);
?>
<div id="formcontent">
<div>You are expected to pay <em id="actualamount"></em></div>
<div>
<h4>Bank Details</h4>
<stong>Account Name </stong>Emeka Uchechukwu Michael<BR><HR/>
<stong>GTBANK</stong><em> 0215001020</em><BR><HR/>
<stong> ACCESS BANK</stong><em> 0042461814</em><BR><HR/>
<stong>FIRST BANK</stong><em> 3119596327</em><BR><HR/>
</div>
<div>
	<i>
		Please after payment send name of depositor, bank paid to and phone to receive the data to 09037072904 for confirmation ( if you paid with ATM or bank transfer code kindly indicate).
	</i>
</div>
</div>

<?
	Modal::end();
?>
<?
$contact = Yii::$app->user->identity->contacts->email;
$url = Url::to(['site/approve']);
$contactEmail = Url::to(['site/updatecontactemail']);
			 
$order = <<<JS

$('.bitcoin').click(function(e){
	e.preventDefault();
	alert ('Bitcoin option is presently not available');
	
});





JS;
 
$this->registerJs($order);

if(empty($contact)){

	$orderss = <<<JS
responce = prompt('Please Enter Email Address Below');
if(responce){
 
 $.ajax({
        	url: "$contactEmail"+"?id="+responce,
			type: "POST",
			
			success: function(data)
		    {
			alert ('Your email has been updated successfull');
		    },
		  	error: function() 
	    	{
	    	} 	        
	   });
}





JS;
 
$this->registerJs($orderss);

}
		
?>

