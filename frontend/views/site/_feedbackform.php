<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\rating\StarRating;

/* @var $this yii\web\View */
/* @var $model app\models\Rate */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
	.btnloading,.btnsent{
		display:none;
		color:#fff !important;
	}
</style>
<div class="rate-form">
<h2 style="text-align:center"> We love feedback </h2>
    <?php $form = ActiveForm::begin(['id'=>'feedbackform'.$mainId]); ?>

    <?= $form->field($model, 'email')->textInput() ?>
    <?= $form->field($model, 'hidden')->hiddenInput(['id'=>'hiden'])->label(false) ?>
    <?= $form->field($model, 'startRating')->widget(StarRating::classname(), [
        'pluginOptions' => [
		
        'min' => 0,
        'max' => 12,
        'step' => 2,
        'size' => 'lg',
        'starCaptions' => [
            0 => 'Extremely Poor',
            2 => 'Very Poor',
            4 => 'Poor',
            6 => 'Ok',
            8 => 'Good',
            10 => 'Very Good',
            12 => 'Extremely Good',
        ],
        'starCaptionClasses' => [
            0 => 'text-danger',
            2 => 'text-danger',
            4 => 'text-warning',
            6 => 'text-info',
            8 => 'text-primary',
            10 => 'text-success',
            12 => 'text-success'
        ],
    ],
	'options' => [
	'id' => 'feedbackinput'.$mainId,
]
]);
 ?>
    <?= $form->field($model, 'body')->textarea(['rows' => '6']) ?>

    <div class="form-group">
        <?= Html::submitButton('<span class="btnnormal">Send<span><span class="btnloading" >Loading .......<span> <span class="btnsent">Sent<span>', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?
$url = Url::to(['site/usersfeedback']);
$order = <<<JS
$('#feedbackform'+$mainId).on('beforeSubmit', function (e) {
	$(document).find('.btnnormal').hide();
	$(document).find('.btnloading').show();
	
	$('#submitButtonText').hide();
 	$('#BtnLoader').show();
    var \$form = $(this);
	var customermail = \$form.find('.customer_email').val();
	var planid = $('#plan_id').val();
	var paymentMode = $('#paymentmode').val();	
	$.post(\$form.attr('action'),\$form.serialize())
		.always(function(result){
	
			
   			if(result==1){
				$(document).find('.btnloading').hide();
				$(document).find('.btnsent').show();
	   			$('.formcontentotherfeedback').hide();
	   			$('.cardthanks').show();
				\$form.parent().parent().parent().parent().find('.cardthanks').show();
				
					$('#test1').load(' #test1');
					$('#test2').load(' #test2');
					$('#test3').load(' #test3');
					$('#firstcart3').load(' #firstcart3');
					$('#firstcart2').load(' #firstcart2');
					$('#firstcart').load(' #firstcart');
    		}else{
				$(document).find('.btnloading').hide();
				$(document).find('.btnsent').show();
				$('.formcontentotherfeedback').hide();
				\$form.parent().parent().parent().parent().find('.formcontenttransferfeedback').show();
				
					$('#test1').load(' #test1');
					$('#test2').load(' #test2');
					$('#test3').load(' #test3');
					$('#firstcart3').load(' #firstcart3');
					$('#firstcart2').load(' #firstcart2');
					$('#firstcart').load(' #firstcart');
				
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	
    
	
	
    return false;
 	
    
});


JS;
 
$this->registerJs($order);
?>


