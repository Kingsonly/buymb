<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use frontend\models\planType;

use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\models\order */
/* @var $form ActiveForm */
?>

<style>
	<? 
if (!Yii::$app->user->isGuest) {
           
        
?>
	
	.numbers{
		display: none;
		
	}
	
<? }?>
	.voucher{
		display: none;
		margin-bottom: 10px;
	}
	#BtnLoader3{
		display:none;
	}
</style>
<div class="data-orderform">

    <?php $form = ActiveForm::begin(['action' =>['folder/create'],'id' => 'dataforms3']); ?>
	
        <? if (!Yii::$app->user->isGuest) {
				$payment = ['card'=>'ATM Card','voucher'=>'Voucher','wallet'=>'Wallet','transfer'=>'Bank Transfer'];
			}else{
				$payment = ['card'=>'ATM Card','voucher'=>'Voucher','transfer'=>'Bank Transfer'];
			}
	?>
	
        
		
		
	
	
		<?= $form->field($model, 'payment_mode')->dropDownList($payment,[ 'prompt'=> 'Select...', 'class' => 'form-control', 'id' => 'paymentmode3']) ?>
	
	<div class="voucher">
		<label  class="control-label">Enter Voucher</label>
		<input type="text" class="form-control pin" id = "pin3" />
	</div>
		
	
        
    <?= $form->field($model, 'customer_email')->textInput(['class'=>'customer_email']);
		?>
        <div class="form-group">
            <? //= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
			<?= Html::submitButton($model->isNewRecord ? '<span id="submitButtonText3">Submit</span>'.Html::img("@web/images/45.gif", ["alt" => "loader","id"=>"BtnLoader3", "class" => "user-image23 BtnLoader" ]).' <span id="feedback3"><span>' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-basic' : 'btn btn-basic','id'=>'submitbtn3']) ?>

        </div>
    <?php ActiveForm::end(); ?>
	
	
</div><!-- data-orderform -->
<?
$voucheUrl = Url::to(['order/bulk-paymentwithvoucher']);
$walletUrl = Url::to(['order/paymentwithwallet']);
$cardValidationUrl = Url::to(['order/bulk-paymentwithcardvalidation']);
$cashAprovedUrl = Url::to(['order/bulkpaymentwithcarddata']);
$transferUrl = Url::to(['order/bulk-paymentbytransfer']);
$actualAmountUrl = Url::to(['site/actualamountdata']);
$actualAmountUrlmain = \Yii::$app->cart->getCost();
$feedbackUrl = Url::to(['site/usersfeedback','mainId'=>3]);

$this->registerJsFile(
    'https://js.paystack.co/v1/inline.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
$order = <<<JS
$('#formcontentotherfeedback3').load('$feedbackUrl');
$('#paymentmode3').change(function(){
	value = $(this).val();
	if(value == 'voucher'){
		$('.voucher').show();
	} else{
		$('.voucher').hide();
	}
});

$('#buyfriend3').click(function(){
	value = $(this).checked;
	if($(this).is(":checked")){
		$('.numbers').show();
	} else{
		$('.numbers').hide();
	}
});


  
$('#dataforms3').on('beforeSubmit', function (e) {
	$('#submitButtonText3').hide();
 	$('#BtnLoader3').show();
    var \$form = $(this);
	var customermail = \$form.find('.customer_email').val();
	var planid = $('#plan_id3').val();
	var paymentMode = $('#paymentmode3').val();
	
	var mainActualAmount = 0;
	
	
	function payWithPaystack(customerEmail,amount,createOrder){
	//alert(amount);
		var handler = PaystackPop.setup({
			
			  //key: 'pk_live_60e06110fdefa3e025e94acf315c0baf26f2ca63',
			  key: 'pk_test_9fffdfb13c6194a5a236b933ddb00aa29a78dd78',
			  email: customermail,
			  amount: amount+'00',
			  
			  metadata: {
				 custom_fields: [
					{
						display_name: "Mobile Number",
						variable_name: "mobile_number",
						value: "+2348012345678"
					}
				 ]
			  },
			  callback: function(response){
				  
				  
				  $.post('$cashAprovedUrl'+'?ordertype=data',$('#dataform').serialize())
		.always(function(result){
	
			
   			if(result==1){
	   			$('#BtnLoader3').hide();
	   			$(document).find('#feedback3').html('Sent').show();
				
				\$form.parent().parent().parent().parent().parent().parent().find('.bgc-gray-darkest').hide();
			 \$form.parent().parent().parent().parent().parent().parent().find('.formcontentotherfeedback').show();
				\$form.parent().parent().parent().parent().parent().parent().find('.formcontentotherfeedback #hiden').val(1);
				
    		}else{
				$('#BtnLoader3').hide();
				$(document).find('#feedback3').html('Not enought funds in wallet').show();
	
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
		
			  },
			  onClose: function(){
				  location.reload();
			  }
		});
		handler.openIframe();
  }
  
	
	function createOrder(planid){
		$.post('$cashAprovedUrl'+'?amountid='+planid+'& ordertype=data',\$form.serialize())
		.always(function(result){
	
			
   			if(result==1){
	   			$('#BtnLoader3').hide();
	   			$('#BtnLoader3').hide();
	   			$(document).find('#feedback3').html('Sent').show();
				$(document).find('.bgc-gray-darkest').hide();
				$(document).find('.formcontentotherfeedback').slideDown();
    		}else{
				$('#BtnLoader3').hide();
				$(document).find('#feedback3').html('Not enought funds in wallet').show();
	
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	}
	
	if(paymentMode == 'wallet'){
	
		$.post('$walletUrl'+'?amountid='+planid+'& ordertype=data',\$form.serialize())
		.always(function(result){
	
			
   			if(result==1){
	   			$('#BtnLoader3').hide();
	   			$(document).find('#feedback3').html('Sent').show();
				countdownexploder();
    		}else{
				$('#BtnLoader3').hide();
				$(document).find('#feedback3').html('Not enought funds in wallet').show();
				countdownexploder();
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	}else if(paymentMode == 'transfer'){
	   
	
		$.post('$transferUrl'+'?ordertype=data',\$form.serialize())
		.always(function(result){
			$(document).find('.actualamount').html('$actualAmountUrlmain');
			
   			if(result==1){
	   			$('#BtnLoader3').hide();
	   			$(document).find('#feedback3').html('Sent').show();
			\$form.parent().parent().parent().parent().parent().parent().find('.bgc-gray-darkest').hide();
			 \$form.parent().parent().parent().parent().parent().parent().find('.formcontentotherfeedback').show();
				\$form.parent().parent().parent().parent().parent().parent().find('.formcontentotherfeedback #hiden').val(2);
				
    		}else{
				alert('error')
	
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	} else if (paymentMode == 'card'){
		$.post('$cardValidationUrl'+'?amountid='+planid,\$form.serialize())
		.always(function(result){
	
			
   			if(result.success==1){
				
				customerEmail = result.email;
				if(customerEmail.lenght == 0){
				 customerEmail = 'kingsonly13c@gmail.com';
				}
				
				payWithPaystack(customerEmail,result.amount,createOrder);
				
				
	   			
    		}else{
				$('#BtnLoader3').hide();
				$(document).find('#feedback3').html('Not enought funds in wallet').show();
				setTimeout(function(){ 
				$(document).find('#feedback3').hide();
				$(document).find('#BtnLoader3').hide();
				$(document).find('#submitButtonText3').show();
				$.pjax.reload({container:"#buy_data",async: false}); 
				}, 5000);
	
				
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	} else if (paymentMode == 'voucher'){
			var pin = $('#pin3').val();
			$.post('$voucheUrl'+'?pin='+pin+'& amountid='+planid+'& ordertype=data',\$form.serialize())
		.always(function(result){
	
			$(document).find('#loader3').hide();
   			if(result==1){
			$('#BtnLoader3').hide();
	   
				$('#BtnLoader3').hide();
	   			$(document).find('#feedback3').html('Sent').show();
				$(document).find('.bgc-gray-darkest').hide();
				$(document).find('.formcontentotherfeedback').slideDown();
				countdownexploder();
    		}else{
				$('#BtnLoader3').hide();
				$(document).find('#feedback3').html('Invalid or Pin used').show();
				countdownexploder();
	
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	} else{
		alert('no option selected');
	}
    
	
	setTimeout(function(){

	//$.pjax.reload({container:"#firstcart",async: false}); 
	//$.pjax.reload({container:"#firstcart2",async: false}); 
	//$.pjax.reload({container:"#firstcart3",async: false}); 
	$(document).find('#feedback3').hide();
	$(document).find('#BtnLoader3').hide();
	$(document).find('#submitButtonText3').show();
	$.pjax.reload({container:"#buy_data3",async: false}); 
	}, 3000);
    return false;
 	
    
});


			 
JS;
 
$this->registerJs($order);
?>




