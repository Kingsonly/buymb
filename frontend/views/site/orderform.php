<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use frontend\models\planType;

use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\models\order */
/* @var $form ActiveForm */
?>

<style>
	<? 
if (!Yii::$app->user->isGuest) {
           
        
?>
	
	#numbersguest{
		display: none;
		
	}
	
<? }?>
	#voucherguest{
		display: none;
		margin-bottom: 10px;
	}
	#BtnLoaderguest{
		display:none;
	}
</style>
<div class="data-orderform">

    <?php $form = ActiveForm::begin(['action' =>['folder/create'],'id'=>'dataformguest']); ?>
	
		<?= $form->field($model, 'plan_id_selector')->hiddenInput(['id' => 'networkid'])->label(false); ?>
    
	
		<?= $form->field($model, 'plan_id')->hiddenInput(['id'=>'plan_id'])->label(false);
		?>
	
        <? if (!Yii::$app->user->isGuest) {
				$payment = ['card'=>'ATM Card','voucher'=>'Voucher','wallet'=>'Wallet','transfer'=>'Bank Transfer'];
			}else{
				$payment = ['card'=>'ATM Card','voucher'=>'Voucher','transfer'=>'Bank Transfer'];
			}
	?>
	
        
		
		<? if (!Yii::$app->user->isGuest) {?>
		Buy for a friend <input type="checkbox" value="1" name="friend" id="buyfriendguest" />
		<? }?>
	<div id="numbers">
			<?= $form->field($contactModel, 'phone_number')->textInput(['id'=>'phone_numberguest'])->label(Yii::$app->user->isGuest?'Enter  Phone Number':'Enter Friend Phone Number') ?>
		</div>
	
		<?= $form->field($model, 'payment_mode')->dropDownList($payment,[ 'prompt'=> 'Select...', 'class' => 'form-control', 'id' => 'paymentmodeguest']) ?>
	
	<div id="voucherguest">
		<label  class="control-label">Enter Voucher</label>
		<input type="text" class="form-control" id='pinguest'/>
	</div>
		
	<?= $form->field($model, 'customer_email')->textInput(['class'=>'customer_email']);
		?>
        
    
        <div class="form-group">
            <? //= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
			<?= Html::submitButton($model->isNewRecord ? '<span id="submitButtonTextguest">Submit</span>'.Html::img("@web/images/45.gif", ["alt" => "loader","id"=>"BtnLoaderguest", "class" => "user-image23" ]).' <span id="feedbackguest"><span>' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-basic' : 'btn btn-basic','id'=>'submitbtnguest']) ?>
        </div>
    <?php ActiveForm::end(); ?>
	
	
</div><!-- data-orderform -->
<?
$voucheUrl = Url::to(['order/paymentwithvoucher']);
$walletUrl = Url::to(['order/paymentwithwallet']);
$cardValidationUrl = Url::to(['order/paymentwithcardvalidation']);
$cashAprovedUrl = Url::to(['order/paymentwithcarddata']);
$transferUrl = Url::to(['order/paymentbytransfer']);
$actualAmountUrl = Url::to(['site/actualamountdata']);
$feedbackUrl = Url::to(['site/usersfeedback','mainId'=>0]);

$this->registerJsFile(
    'https://js.paystack.co/v1/inline.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
$order = <<<JS
$('#formcontentotherfeedback0').load('$feedbackUrl');
function countdownexploder(){

	// Set the date we're counting down to
var countDownDate = new Date().getTime()+10000;
$(document).find('#destroy').show()
// Update the count down every 1 second
var x = setInterval(function() {

    // Get todays date and time
    var now = new Date().getTime();
    
    // Find the distance between now an the count down date
    var distance = countDownDate - now;
    
    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
    // Output the result in an element with id="demo"
    document.getElementById("demo").innerHTML = days + "d " + hours + "h "
    + minutes + "m " + seconds + "s ";
    
    // If the count down is over, write some text 
    if (distance < 0) {
        clearInterval(x);
		$(document).find('#destroy').hide()
        
    
		
		
		
	$('#details').modal().find('#formcontent').show();
	$('#details').modal().find('#formcontentsent').hide();
	$('#approvetxt').show();
	$(document).find('#submitButtonText').show();
	$('#details').modal('hide');
	$('#test').show();
	 function Particle( x, y, radius ) {
            this.init( x, y, radius );
        }

        Particle.prototype = {

            init: function( x, y, radius ) {

                this.alive = true;

                this.radius = radius || 10;
                this.wander = 0.15;
                this.theta = random( TWO_PI );
                this.drag = 0.92;
                this.color = '#fff';

                this.x = x || 0.0;
                this.y = y || 0.0;

                this.vx = 0.0;
                this.vy = 0.0;
            },

            move: function() {

                this.x += this.vx;
                this.y += this.vy;

                this.vx *= this.drag;
                this.vy *= this.drag;

                this.theta += random( -0.5, 0.5 ) * this.wander;
                this.vx += sin( this.theta ) * 0.1;
                this.vy += cos( this.theta ) * 0.1;

                this.radius *= 0.96;
                this.alive = this.radius > 0.5;
            },

            draw: function( ctx ) {

                ctx.beginPath();
                ctx.arc( this.x, this.y, this.radius, 0, TWO_PI );
                ctx.fillStyle = this.color;
                ctx.fill();
            }
        };

        // ----------------------------------------
        // Example
        // ----------------------------------------

        var MAX_PARTICLES = 280;
        var COLOURS = [ '#69D2E7', '#A7DBD8', '#E0E4CC', '#F38630', '#FA6900', '#FF4E50', '#F9D423' ];

        var particles = [];
        var pool = [];

        var demo = Sketch.create({
            container: document.getElementById( 'test' ),
            retina: 'auto'
        });

        demo.setup = function() {

            // Set off some initial particles.
            var i, x, y;

            for ( i = 0; i < 100; i++ ) {
                x = ( demo.width * 0.5 ) + random( -100, 100 );
                y = ( demo.height * 0.5 ) + random( -100, 100 );
                demo.spawn( x, y );
            }
        };

        demo.spawn = function( x, y ) {
            
            var particle, theta, force;

            if ( particles.length >= MAX_PARTICLES )
                pool.push( particles.shift() );

            particle = pool.length ? pool.pop() : new Particle();
            particle.init( x, y, random( 5, 40 ) );

            particle.wander = random( 0.5, 2.0 );
            particle.color = random( COLOURS );
            particle.drag = random( 0.9, 0.99 );

            theta = random( TWO_PI );
            force = random( 2, 8 );

            particle.vx = sin( theta ) * force;
            particle.vy = cos( theta ) * force;

            particles.push( particle );
        };


        demo.update = function() {

            var i, particle;

            for ( i = particles.length - 1; i >= 0; i-- ) {

                particle = particles[i];

                if ( particle.alive ) particle.move();
                else pool.push( particles.splice( i, 1 )[0] );
            }
        };

        demo.draw = function() {

            demo.globalCompositeOperation  = 'lighter';

            for ( var i = particles.length - 1; i >= 0; i-- ) {
                particles[i].draw( demo );
            }
        };
		
	
	$('#test').delay(1000).hide(0);
	
	
	
	}
}, 1000);
}
	

$('#paymentmodeguest').change(function(){
	value = $(this).val();
	if(value == 'voucher'){
		$('#voucherguest').show();
	} else{
		$('#voucherguest').hide();
	}
});

$('#buyfriendguest').click(function(){
	value = $(this).checked;
	if($(this).is(":checked")){
		$('#numbersguest').show();
	} else{
		$('#numbersguest').hide();
	}
});


  
$('#dataformguest').on('beforeSubmit', function (e) {
	$('#submitButtonTextguest').hide();
 	$('#BtnLoaderguest').show();
    var \$form = $(this);
	var customermail = \$form.find('.customer_email').val();
	var planid = $('#plan_id').val();
	var paymentMode = $('#paymentmodeguest').val();
	
	var mainActualAmount = 0;
	$.post('$actualAmountUrl'+'?id='+planid)
		.always(function(actualAmount){
	
			
   			if(actualAmount >= 1){
			mainActualAmount = actualAmount;
				}else{
			
				alert('Could not fetch actual amount');
				return false;
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	
	function payWithPaystack(customerEmail,amount,createOrder){
	//alert(amount);
		var handler = PaystackPop.setup({
			
			  key: 'pk_live_60e06110fdefa3e025e94acf315c0baf26f2ca63',
			  email: customermail,
			  amount: amount+'00',
			  
			  metadata: {
				 custom_fields: [
					{
						display_name: "Mobile Number",
						variable_name: "mobile_number",
						value: "+2348083430800"
					}
				 ]
			  },
			  callback: function(response){
				  
		
		$.post('$cashAprovedUrl'+'?amountid='+planid+'& ordertype=data',\$form.serialize())
		.always(function(result){
	
			
   			if(result==1){
	   			$('#BtnLoaderguest').hide();
	   			$(document).find('#feedbackguest').html('Sent').show();
				\$form.parent().parent().parent().find('.bgc-gray-darkest').hide();
			 \$form.parent().parent().parent().find('.formcontentotherfeedback').show();
				\$form.parent().parent().parent().find('.formcontentotherfeedback #hiden').val(1);
    		}else{
				$('#BtnLoaderguest').hide();
				$(document).find('#feedbackguest').html('Not enought funds in wallet').show();
	
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
		
			  },
			  onClose: function(){
				  location.reload();
			  }
		});
		handler.openIframe();
  }
  
	
	function createOrder(planid){
		$.post('$cashAprovedUrl'+'?amountid='+planid+'& ordertype=data',\$form.serialize())
		.always(function(result){
	
			
   			if(result==1){
	   			$('#BtnLoaderguest').hide();
	   			$('#submitButtonTextguest').hide();
	   			$(document).find('#feedbackguest').html('Sent').show();
    		}else{
				$('#BtnLoader').hide();
				$(document).find('#feedbackguest').html('Not enought funds in wallet').show();
	
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	}
	
	if(paymentMode == 'wallet'){
	
		$.post('$walletUrl'+'?amountid='+planid+'& ordertype=data',\$form.serialize())
		.always(function(result){
	
			
   			if(result==1){
	   			$('#BtnLoaderguest').hide();
	   			$(document).find('#feedbackguest').html('Sent').show();
				countdownexploder();
    		}else{
				$('#BtnLoaderguest').hide();
				$(document).find('#feedbackguest').html('Not enought funds in wallet').show();
				countdownexploder();
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	}else if(paymentMode == 'transfer'){
	
		$.post('$transferUrl'+'?amountid='+planid+'& ordertype=data',\$form.serialize())
		.always(function(result){
	
			
   			if(result==1){
	   			$('#BtnLoaderguest').hide();
	   			$('#formcontent').hide();
				//$(document).find('.formcontenttransferfeedback').slideDown();
	   			$(document).find('#feedbackguest').html('Sent').show();
	   			$(document).find('#formcontentguest').slideUp();
	   			
				//$('#actualamountmain').hide(fast)
				$('#actualamountmain').html(mainActualAmount +' Naira');
				//$(document).find('.actualamount').html(mainActualAmount);
				//countdownexploder();
				
				//\$form.parent().parent().parent().find('.bgc-gray-darkest').hide();
			 \$form.parent().parent().parent().find('.formcontentotherfeedback').show();
				\$form.parent().parent().parent().find('.formcontentotherfeedback #hiden').val(2);
    		}else{
				$('#BtnLoaderguest').hide();
				$(document).find('#feedbackguest').html('Not enought funds in wallet').show();
				$(document).find('#formcontentguest').slideUp();
	   			$(document).find('#errorTransferguest').slideDown();
				//countdownexploder();
	
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	} else if (paymentMode == 'card'){
		$.post('$cardValidationUrl'+'?amountid='+planid,\$form.serialize())
		.always(function(result){
	
			
   			if(result.success==1){
				
				customerEmail = result.email;
				if(customerEmail.lenght == 0){
				 customerEmail = 'kingsonly13c@gmail.com';
				}
				
				payWithPaystack(customerEmail,result.amount,createOrder);
				
				
	   			
    		}else{
				$('#BtnLoaderguest').hide();
				$(document).find('#feedbackguest').html('Not enought funds in wallet').show();
				setTimeout(function(){ 
				$(document).find('#feedbackguest').hide();
				$(document).find('#BtnLoaderguest').hide();
				$(document).find('#submitButtonTextguest').show();
				$.pjax.reload({container:"#buy_data",async: false}); 
				}, 5000);
	
				
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	} else if (paymentMode == 'voucher'){
			var pin = $('#pinguest').val();
			$.post('$voucheUrl'+'?pin='+pin+'& amountid='+planid+'& ordertype=data',\$form.serialize())
		.always(function(result){
	
			$(document).find('#loader').hide();
   			if(result==1){
			$('#BtnLoaderguest').hide();
	   
	   			$(document).find('#feedbackguest').html('sent').show();
				countdownexploder();
    		}else{
				$('#BtnLoader').hide();
				$(document).find('#feedbackguest').html('Invalid or Pin used').show();
				countdownexploder();
	
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	} else{
		alert('no option selected');
	}
    
	
	setTimeout(function(){ 
	$(document).find('#feedbackguest').hide();
	$(document).find('#BtnLoaderguest').hide();
	$(document).find('#submitButtonTextguest').show();
	$.pjax.reload({container:"#buy_data",async: false}); 
	}, 5000);
    return false;
 	
    
});


JS;
 
$this->registerJs($order);
?>




