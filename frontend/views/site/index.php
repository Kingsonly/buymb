<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
//use kartik\widgets\ActiveForm;
use frontend\models\planType;
use kartik\popover\PopoverX;
use yii\widgets\Pjax;
use yii\bootstrap\Alert;

?> 
<style>
	#successTransfer,#destroy,#errorTransfer,.formcontenttransferfeedback,#formcontent,.loaderdive,.cardthanks{
		display: none;
	}
	.cardthanks{
		height:400px;
		background: green;
	}
	
	.cardthanks h3 {
		color:#fff;
	}
	
	.loaderdive{
		height: 320px;
		padding-top: 100px
	}
	
	#fountainTextG{
	width:180px;
	margin:auto;
}
	.customer_email{
		color: #000 !important;
	}

.fountainTextG{
	color:rgb(0,0,0);
	font-family:Arial;
	font-size:19px;
	text-decoration:none;
	font-weight:normal;
	font-style:normal;
	float:left;
	animation-name:bounce_fountainTextG;
		-o-animation-name:bounce_fountainTextG;
		-ms-animation-name:bounce_fountainTextG;
		-webkit-animation-name:bounce_fountainTextG;
		-moz-animation-name:bounce_fountainTextG;
	animation-duration:2.09s;
		-o-animation-duration:2.09s;
		-ms-animation-duration:2.09s;
		-webkit-animation-duration:2.09s;
		-moz-animation-duration:2.09s;
	animation-iteration-count:infinite;
		-o-animation-iteration-count:infinite;
		-ms-animation-iteration-count:infinite;
		-webkit-animation-iteration-count:infinite;
		-moz-animation-iteration-count:infinite;
	animation-direction:normal;
		-o-animation-direction:normal;
		-ms-animation-direction:normal;
		-webkit-animation-direction:normal;
		-moz-animation-direction:normal;
	transform:scale(.5);
		-o-transform:scale(.5);
		-ms-transform:scale(.5);
		-webkit-transform:scale(.5);
		-moz-transform:scale(.5);
}#fountainTextG_1{
	animation-delay:0.75s;
		-o-animation-delay:0.75s;
		-ms-animation-delay:0.75s;
		-webkit-animation-delay:0.75s;
		-moz-animation-delay:0.75s;
}
#fountainTextG_2{
	animation-delay:0.9s;
		-o-animation-delay:0.9s;
		-ms-animation-delay:0.9s;
		-webkit-animation-delay:0.9s;
		-moz-animation-delay:0.9s;
}
#fountainTextG_3{
	animation-delay:1.05s;
		-o-animation-delay:1.05s;
		-ms-animation-delay:1.05s;
		-webkit-animation-delay:1.05s;
		-moz-animation-delay:1.05s;
}
#fountainTextG_4{
	animation-delay:1.2s;
		-o-animation-delay:1.2s;
		-ms-animation-delay:1.2s;
		-webkit-animation-delay:1.2s;
		-moz-animation-delay:1.2s;
}
#fountainTextG_5{
	animation-delay:1.35s;
		-o-animation-delay:1.35s;
		-ms-animation-delay:1.35s;
		-webkit-animation-delay:1.35s;
		-moz-animation-delay:1.35s;
}
#fountainTextG_6{
	animation-delay:1.5s;
		-o-animation-delay:1.5s;
		-ms-animation-delay:1.5s;
		-webkit-animation-delay:1.5s;
		-moz-animation-delay:1.5s;
}
#fountainTextG_7{
	animation-delay:1.64s;
		-o-animation-delay:1.64s;
		-ms-animation-delay:1.64s;
		-webkit-animation-delay:1.64s;
		-moz-animation-delay:1.64s;
}
#fountainTextG_8{
	animation-delay:1.79s;
		-o-animation-delay:1.79s;
		-ms-animation-delay:1.79s;
		-webkit-animation-delay:1.79s;
		-moz-animation-delay:1.79s;
}
#fountainTextG_9{
	animation-delay:1.94s;
		-o-animation-delay:1.94s;
		-ms-animation-delay:1.94s;
		-webkit-animation-delay:1.94s;
		-moz-animation-delay:1.94s;
}
#fountainTextG_10{
	animation-delay:2.09s;
		-o-animation-delay:2.09s;
		-ms-animation-delay:2.09s;
		-webkit-animation-delay:2.09s;
		-moz-animation-delay:2.09s;
}
#fountainTextG_11{
	animation-delay:2.24s;
		-o-animation-delay:2.24s;
		-ms-animation-delay:2.24s;
		-webkit-animation-delay:2.24s;
		-moz-animation-delay:2.24s;
}
#fountainTextG_12{
	animation-delay:2.39s;
		-o-animation-delay:2.39s;
		-ms-animation-delay:2.39s;
		-webkit-animation-delay:2.39s;
		-moz-animation-delay:2.39s;
}
#fountainTextG_13{
	animation-delay:2.54s;
		-o-animation-delay:2.54s;
		-ms-animation-delay:2.54s;
		-webkit-animation-delay:2.54s;
		-moz-animation-delay:2.54s;
}
#fountainTextG_14{
	animation-delay:2.69s;
		-o-animation-delay:2.69s;
		-ms-animation-delay:2.69s;
		-webkit-animation-delay:2.69s;
		-moz-animation-delay:2.69s;
}
#fountainTextG_15{
	animation-delay:2.84s;
		-o-animation-delay:2.84s;
		-ms-animation-delay:2.84s;
		-webkit-animation-delay:2.84s;
		-moz-animation-delay:2.84s;
}
#fountainTextG_16{
	animation-delay:2.99s;
		-o-animation-delay:2.99s;
		-ms-animation-delay:2.99s;
		-webkit-animation-delay:2.99s;
		-moz-animation-delay:2.99s;
}
#fountainTextG_17{
	animation-delay:3.14s;
		-o-animation-delay:3.14s;
		-ms-animation-delay:3.14s;
		-webkit-animation-delay:3.14s;
		-moz-animation-delay:3.14s;
}
#fountainTextG_18{
	animation-delay:3.29s;
		-o-animation-delay:3.29s;
		-ms-animation-delay:3.29s;
		-webkit-animation-delay:3.29s;
		-moz-animation-delay:3.29s;
}
#fountainTextG_19{
	animation-delay:3.44s;
		-o-animation-delay:3.44s;
		-ms-animation-delay:3.44s;
		-webkit-animation-delay:3.44s;
		-moz-animation-delay:3.44s;
}
#fountainTextG_20{
	animation-delay:3.59s;
		-o-animation-delay:3.59s;
		-ms-animation-delay:3.59s;
		-webkit-animation-delay:3.59s;
		-moz-animation-delay:3.59s;
}




@keyframes bounce_fountainTextG{
	0%{
		transform:scale(1);
		color:rgba(204,34,97,0.98);
	}

	100%{
		transform:scale(.5);
		color:rgb(255,255,255);
	}
}

@-o-keyframes bounce_fountainTextG{
	0%{
		-o-transform:scale(1);
		color:rgba(204,34,97,0.98);
	}

	100%{
		-o-transform:scale(.5);
		color:rgb(255,255,255);
	}
}

@-ms-keyframes bounce_fountainTextG{
	0%{
		-ms-transform:scale(1);
		color:rgba(204,34,97,0.98);
	}

	100%{
		-ms-transform:scale(.5);
		color:rgb(255,255,255);
	}
}

@-webkit-keyframes bounce_fountainTextG{
	0%{
		-webkit-transform:scale(1);
		color:rgba(204,34,97,0.98);
	}

	100%{
		-webkit-transform:scale(.5);
		color:rgb(255,255,255);
	}
}

@-moz-keyframes bounce_fountainTextG{
	0%{
		-moz-transform:scale(1);
		color:rgba(204,34,97,0.98);
	}

	100%{
		-moz-transform:scale(.5);
		color:rgb(255,255,255);
	}
}
	
	<? 
if (!Yii::$app->user->isGuest) {
           
        
?>
	
	.numbers{
		display: none;
		
	}
	
<? }?>
	.addedtocartshow{
		text-align: center;
	}
	.voucher{
		display: none;
		margin-bottom: 10px;
	}
	.BtnLoader{
		display:none;
	}
	.close{
		display: none;
	}
	.addedtocart{
		display: none;
	}
	.formcontentotherfeedback{
		display:none;
	}
</style>

<div class="page-content">
	
      <!-- Page Navigation-->
      <div class="main-nav main-nav-onepage easing-click-group bgc-transparent nav-absolute border-separator large-container">
        <div class="__header">
          <div class="container">
            <div class="nav-content-wrapper">
              <div class="pull-left">
                <div class="nav-info nav-item">
                  <div class="__info select-language dropdown" style="color:green"><a href="#" data-target="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-globe color-primary"></i>Language: English</a>
                    <div class="dropdown-menu menu menu-link-block">
                      <ul>
                        <li><a href="#"><img src="assets/images/flag/united_kingdom.png" alt="flag" class="flag"/>ENGLISH</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="__info"style="color:green"><i class="icon icon-phone-1 color-primary"></i>(234) 7035776850</div>
                  <div class="__info" style="color:green"><i class="fa fa-clock-o color-primary"></i>Mon - Sat: 9:00 - 18:00</div>
                </div>
              </div>
              <div class="pull-right">
                <div class="nav-info nav-item mr-20" style="color:green"><span class="fz-6-s"><i class="fa fa-send color-primary"></i>info@buymb.ng</span></div>
                <ul class="social nav-item">
                  <li><a href="#"><i class="icon icon-facebook-1"></i></a></li>
                  <li><a href="#"><i class="icon icon-pinterest"></i></a></li>
                  <li><a href="#"><i class="icon icon-twitter-1"></i></a></li>
                  <li><a href="#"><i class="icon icon-linkedin-1"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="__middle bgc-dark-o-2">
          <div class="container">
            <div class="nav-content-wrapper">
              <div class="pull-left">
                <div class="megatron inline logo-light logo-only">
                  <div class="cell-vertical-wrapper">
                    <div class="cell-middle"><a href="index.html">
                        <div class="logo" style="width:100px">
							
							<?=Html::img("@web/images/trans_logow.png", ["alt" => "loader","id"=>"logo", "class" => "" ]) ?>
                        </div></a></div>
                  </div>
                </div>
              </div>
              <div class="pull-right">
                <nav class="main-menu easing-link-group">
                  <ul>
                    <li><a href="#home">HOME </a></li>
                    <li><a href="#buy-data">BUY DATA</a></li>
                    <li><a href="#buy-airtime">BUY AIRTIME</a></li>
                    <li><a href="#pricing">RESELLERS</a></li>
                    <li><a href="#vouchers">VOUCHERS</a></li>
                    <li><a href="#bitcoin">BITCOIN</a></li>
                    <li><a href="#footer">CONTACT</a></li>
					<li>
						
						<?
							 PopoverX::begin([	
								'placement' => PopoverX::ALIGN_BOTTOM,
								 
								'toggleButton' => ['label'=>'Register','tag'=>'a', 'class'=>''],
								'header' => '<i class="glyphicon glyphicon-lock"></i> Enter credentials',
								'footer' => Html::button('Submit', [
										'class' => 'btn btn-sm btn-primary', 
										'onclick' => '$("#form-signup").trigger("submit")'
									]) . Html::button('Reset', [
										'class' => 'btn btn-sm btn-default', 
										'onclick' => '$("#form-signup").trigger("reset")'
									])
								]);
// form with an id used for action buttons in footer
							   ?>
						<?php $form = ActiveForm::begin(['action' => ['signup'],'id' => 'form-signup']); ?>
         

              <?= $form->field($signupModel, 'username')->textInput(['autofocus' => true]) ?>
			
				<?= $form->field($signupModel, 'phone')->textInput(['autofocus' => true]) ?>

                
                <?= $form->field($signupModel, 'password')->passwordInput() ?>

                
			
		

                

            <?php ActiveForm::end(); ?>



								
						 <?
							   PopoverX::end();
						?>
					</li>
					  <? if(!Yii::$app->user->isGuest){ ?>
					  <?= Html::tag('li',Html::a('Logout', Url::to(['logout']), ['data-method' => 'POST','class' => 'text-danger']))  ?>
					  <? }else{?>
					  
                    <li>
						<?
							 PopoverX::begin([	
								'placement' => PopoverX::ALIGN_BOTTOM,
								 
								'toggleButton' => ['label'=>'Login','tag'=>'a', 'class'=>''],
								'header' => '<i class="glyphicon glyphicon-lock"></i> Enter credentials',
								'footer' => Html::button('Submit', [
										'class' => 'btn btn-sm btn-primary', 
										'onclick' => '$("#kv-login-form2").trigger("submit")'
									]) . Html::button('Reset', [
										'class' => 'btn btn-sm btn-default', 
										'onclick' => '$("#kv-login-form2").trigger("reset")'
									])
								]);
// form with an id used for action buttons in footer
							   $form = ActiveForm::begin(['action' => ['login'],'options' => ['id'=>'kv-login-form2']]); ?>

								<?= $form->field($loginModel, 'username',['options'=>[
									'tag'=>'div',
									'class'=>'form-group has-feedback field-loginform-username required'],

									'template'=>'{input}<span class="glyphicon glyphicon-user form-control-feedback"></span>{error}{hint}'
										])->textInput(['placeholder'=>'User Name']) ?>

								<?= $form->field($loginModel, 'password',['options'=>[
									'tag'=>'div',
									'class'=>'form-group has-feedback field-loginform-password required'],
									'template'=>'{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span>{error}{hint}'
										])->passwordInput(['placeholder'=>'Password']) ?>



								<div class="col-xs-8 pull-left" style="padding-left: 0px !important">
							  <div class="checkbox icheck">
								<label>
								  <?= $form->field($loginModel, 'rememberMe')->checkbox() ?>
								</label>
							  </div>
							</div>



								<div style="color:#999;margin:1em 0; clear:both;">
										If you forgot your password you can <?= Html::a('reset it', ['site/request-password-reset']) ?>.
									</div>

                

            					<?php ActiveForm::end(); ?>
						 <?
							   PopoverX::end();
						?>
					  </li>
					  <? }?>
					  <? if(!Yii::$app->user->isGuest){ ?>
                    <li class="text-danger"><a href="<?= Url::to(['dashboard/index']);?>">Client area</a></li>
					  <? }?>
					 <!-- <div class="__cart dropdown"><a href="#" data-target="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="cart-open icon-shopping111">
                      <div class="__quantity bgc-primary"><? var_dump(Yii::$app->cart->positions);?><?= Yii::$app->cart->getCount(); ?> | <?= \Yii::$app->cart->getCost();?></div></a>
                    <div class="dropdown-menu bgc-gray-darkest">
                      <div class="__cart-item">
                        <div class="__image"><img src="assets/images/shop/product-img-1-s.jpg" alt="Shop Product" class="img-responsive"/></div>
                        <div class="__info">
                          <div class="__category font-serif-italic fz-6-s">Women / Clothe</div>
                          <div class="font-heading fz-6-ss"><a href="#">SKATER DRESS IN LEAF</a></div><span class="__price color-primary font-heading">1 x $280</span>
                        </div><a href="#" class="__remove icon-185090-delete-garbage-streamline"></a>
                      </div>
                      <div class="__cart-item">
                        <div class="__image"><img src="assets/images/shop/product-img-2-s.jpg" alt="Shop Product" class="img-responsive"/></div>
                        <div class="__info">
                          <div class="__category font-serif-italic fz-6-s">Women / Clothe</div>
                          <div class="font-heading fz-6-ss"><a href="#">SKATER DRESS IN LEAF</a></div><span class="__price color-primary font-heading">1 x $280</span>
                        </div><a href="#" class="__remove icon-185090-delete-garbage-streamline"></a>
                      </div>
                      <div class="__cart-item">
                        <div class="__image"><img src="assets/images/shop/product-img-3-s.jpg" alt="Shop Product" class="img-responsive"/></div>
                        <div class="__info">
                          <div class="__category font-serif-italic fz-6-s">Women / Clothe</div>
                          <div class="font-heading fz-6-ss"><a href="#">SKATER DRESS IN LEAF</a></div><span class="__price color-primary font-heading">1 x $280</span>
                        </div><a href="#" class="__remove icon-185090-delete-garbage-streamline"></a>
                      </div>
                      <div class="__middle-row"><a href="#"><i class="icon-185090-delete-garbage-streamline mr-5"></i>Empty Cart</a>
                        <div class="__total">Subtotal : &nbsp;<span class="color-primary">$740</span></div>
                      </div>
                      <div class="__button-group"><a href="shop-cart.html" class="btn btn-primary"><i class="icon icon-svg-icon-16"></i>VIEW CART</a><a href="shop-checkout.html" class="pull-right btn btn-secondary">CHECKOUT</a></div>
                    </div>
                  </div> -->
					   <li>
						<!-- this is the first header cart -->
						<? 
							 PopoverX::begin([	
								'placement' => PopoverX::ALIGN_LEFT_TOP,
								'size' => PopoverX::SIZE_LARGE,
								 
								'toggleButton' => ['label'=>'<i class="cart-open icon-shopping111"></i> <span id="test1">'.Yii::$app->cart->getCount().'</span>','tag'=>'a', 'class'=>'firstcart'],
								'header' => 'Cart',
							
								]);
						  
						   ?>
						    
						   <?php Pjax::begin(['id' => 'firstcart']); ?>
						   <div class="bgc-gray-darkest">
							   <? if(Yii::$app->cart->getCount() != 0){?>
						 <? foreach(Yii::$app->cart->positions as $key => $value){?>
						   <div class="__cart-item" style="border-bottom:1px solid #ccc">
                        <div class="__image" style="display:inline"><img src="assets/images/shop/product-img-1-s.jpg" alt="Shop Product" class="img-responsive"/></div>
                        <div class="__info">
                          <div class="__category font-serif-italic fz-6-s">network/plan: <?= $value->plan_name;?></div>
                          <div class="font-heading fz-6-ss"><a href="#">Phone number:<?= $value->phone_number;?></a></div><span class="__price color-primary font-heading">1 x $<?= $value->plan_amount;?></span>
                        </div><a href="#" class="__remove icon-185090-delete-garbage-streamline"></a>
                      </div>
					 <? } ?>
						<div class="__cart-item" style="border-bottom:1px solid #ccc">
                        <div class="__image" style="display:inline">
							
							clear cart
						</div>
                        <div class="__info">
                          subtotal : <?= \Yii::$app->cart->getCost();?>
                        </div>
                      </div>	   
					<?= $this->render('guestorder', ['model' => $model]) ?>
							   <? }else{?>
							 <h2>
							   Cart is empty.
							 </h2>  
							   <? }?>
                      </div>
						   <?php Pjax::end(); ?>
						   <div class="formcontenttransferfeedback">
	
	<div>You are expected to pay <em class="actualamount"><?= Yii::$app->cart->getCost(); ?></em></div>
<div>
<h4>Bank Details</h4>
<stong>Account Name </stong>Emeka Uchechukwu Michael<BR><HR/>
<stong>GTBANK</stong><em> 0215001020</em><BR><HR/>
<stong> ACCESS BANK</stong><em> 0042461814</em><BR><HR/>
<stong>FIRST BANK</stong><em> 3119596327</em><BR><HR/>
</div>
<div>
	<i>
		Please after payment send name of depositor, bank paid to , to 09037072904 for confirmation ( if you paid with ATM or bank transfer code kindly indicate).
	</i>
</div>

</div>
							<div id="formcontentotherfeedback1" class="formcontentotherfeedback"></div>
							<div class="cardthanks">
	<div style="text-align:center"><?= Html::img('@web/images/smiley-face-thumbs-up.png', ['alt' => 'My logo']) ?></div>
	<h3 style="text-align:center">We hope to see you soon </h3>
	</div>
						
						 <?
							   PopoverX::end();
						    
						?>
						  
	
					  </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>
	<!-- Nav 2 -->
      <div class="main-nav small-nav bgc-light fixed-tranformed-bg-light nav-fixed border-bottom easing-click-group">
        <div class="container">
          <div class="nav-content-wrapper">
            <div class="pull-left">
              <div class="megatron inline logo-dark logo-only">
                <div class="cell-vertical-wrapper">
                  <div class="cell-middle"><a href="index.html">
                      <div class="logo" style="width:200px">
                          <?=Html::img("@web/images/trans_logo.png", ["alt" => "loader","id"=>"logo", "class" => "" ]) ?>
                      </div></a>
					</div>
                </div>
              </div>
            </div>
            <div class="pull-right visible-lg">
              <nav class="main-menu easing-link-group">
                <ul>
                    <li><a href="#home">HOME</a></li>
                    <li><a href="#buy-data">BUY DATA</a></li>
                    <li><a href="#buy-airtime">BUY AIRTIME</a></li>
                    <li><a href="#vouchers">VOUCHERS</a></li>
                    <li><a href="#bitcoin">BITCOIN</a></li>
                    <li><a href="#footer">CONTACT</a></li>
					
                    <li>
						
						<?
							 PopoverX::begin([	
								'placement' => PopoverX::ALIGN_BOTTOM,
								 
								'toggleButton' => ['label'=>'Register','tag'=>'a', 'class'=>''],
								'header' => '<i class="glyphicon glyphicon-lock"></i> Enter credentials',
								'footer' => Html::button('Submit', [
										'class' => 'btn btn-sm btn-primary', 
										'onclick' => '$("#form-signup2").trigger("submit")'
									]) . Html::button('Reset', [
										'class' => 'btn btn-sm btn-default', 
										'onclick' => '$("#form-signup2").trigger("reset")'
									])
								]);
// form with an id used for action buttons in footer
							   ?>
						<?php $form = ActiveForm::begin(['action' => ['signup'],'id' => 'form-signup2']); ?>
         

              <?= $form->field($signupModel, 'username')->textInput(['autofocus' => true]) ?>
			
				<?= $form->field($signupModel, 'phone')->textInput(['autofocus' => true]) ?>

                
                <?= $form->field($signupModel, 'password')->passwordInput() ?>

                
			
		

                

            <?php ActiveForm::end(); ?>



								
						 <?
							   PopoverX::end();
						?>
					</li>
                    <? if(!Yii::$app->user->isGuest){ ?>
					  <?= Html::tag('li',Html::a('Logout', Url::to(['/site/logout']), ['data-method' => 'POST','class' => 'text-danger']))  ?>
					  <? }else{?>
					  
                   		<li>
					<?
							 PopoverX::begin([	
								'placement' => PopoverX::ALIGN_BOTTOM,
								'toggleButton' => ['label'=>'Login','tag'=>'a', 'class'=>''],
								'header' => '<i class="glyphicon glyphicon-lock"></i> Enter credentials',
								'footer' => Html::button('Submit', [
										'class' => 'btn btn-sm btn-primary', 
										'onclick' => '$(".kv-login-form2").trigger("submit")'
									]) . Html::button('Reset', [
										'class' => 'btn btn-sm btn-default', 
										'onclick' => '$(".kv-login-form2").trigger("reset")'
									])
								]);
// form with an id used for action buttons in footer
							   $form = ActiveForm::begin(['action' => ['login'],'options' => ['class'=>'kv-login-form2']]); ?>

								<?= $form->field($loginModel, 'username',['options'=>[
									'tag'=>'div',
									'class'=>'form-group has-feedback field-loginform-username required'],

									'template'=>'{input}<span class="glyphicon glyphicon-user form-control-feedback"></span>{error}{hint}'
										])->textInput(['placeholder'=>'User Name']) ?>

								<?= $form->field($loginModel, 'password',['options'=>[
									'tag'=>'div',
									'class'=>'form-group has-feedback field-loginform-password required'],
									'template'=>'{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span>{error}{hint}'
										])->passwordInput(['placeholder'=>'Password']) ?>



								<div class="col-xs-8 pull-left" style="padding-left: 0px !important">
							  <div class="checkbox icheck">
								<label>
								  <?= $form->field($loginModel, 'rememberMe')->checkbox() ?>
								</label>
							  </div>
							</div>



								<div style="color:#999;margin:1em 0; clear:both;">
										If you forgot your password you can <?= Html::a('reset it', ['site/request-password-reset']) ?>.
									</div>

                

            					<?php ActiveForm::end(); ?>
						 <?
							   PopoverX::end();
						?>
					</li>
					  <? }?>
					  <? if(!Yii::$app->user->isGuest){ ?>
                    <li class="text-danger"><a href="<?= Url::to(['dashboard/index']);?>">Client area</a></li>
					  <? }?>
					
					<li>
						
						<?
							 PopoverX::begin([	
								'placement' => PopoverX::ALIGN_LEFT_TOP,
								'size' => PopoverX::SIZE_LARGE,
								 
								'toggleButton' => ['label'=>'<i class="cart-open icon-shopping111"></i> <span id="test2">'.Yii::$app->cart->getCount().'</span>','tag'=>'a', 'class'=>'secondcart'],
								'header' => 'Cart',
							
								
								]); ?>
						<?php Pjax::begin(['id' => 'firstcart2']); ?>
						   <div class=" bgc-gray-darkest">
							<? if(Yii::$app->cart->getCount() != 0){?>
						 <? foreach(Yii::$app->cart->positions as $key => $value){?>
						   <div class="__cart-item" style="border-bottom:1px solid #ccc">
                        <div class="__image" style="display:inline"><img src="assets/images/shop/product-img-1-s.jpg" alt="Shop Product" class="img-responsive"/></div>
                        <div class="__info">
                          <div class="__category font-serif-italic fz-6-s">network/plan: <?= $value->plan_name;?></div>
                          <div class="font-heading fz-6-ss"><a href="#">Phone number:<?= $value->phone_number;?></a></div><span class="__price color-primary font-heading">1 x $<?= $value->plan_amount;?></span>
                        </div><a href="#" class="__remove icon-185090-delete-garbage-streamline"></a>
                      </div>
					 <? } ?>
						<div class="__cart-item" style="border-bottom:1px solid #ccc">
                        <div class="__image" style="display:inline">
							
							clear cart
						</div>
                        <div class="__info">
                          subtotal : <?= \Yii::$app->cart->getCost();?>
                        </div>
                      </div>
						
					<?= $this->render('guestorder2', ['model' => $model]) ?>
							   <? }else{?>
							 <h2>
							   Cart is empty. 
							   </h2>  
							   <? }?>
                      </div>
						<?php Pjax::end(); ?>
						
							   <div class="formcontenttransferfeedback">
	
	<div>You are expected to pay <em class="actualamount"></em></div>
<div>
<h4>Bank Details</h4>
<stong>Account Name </stong>Emeka Uchechukwu Michael<BR><HR/>
<stong>GTBANK</stong><em> 0215001020</em><BR><HR/>
<stong> ACCESS BANK</stong><em> 0042461814</em><BR><HR/>
<stong>FIRST BANK</stong><em> 3119596327</em><BR><HR/>
</div>
<div>
	<i>
		Please after payment send name of depositor, bank paid to , to 09037072904 for confirmation ( if you paid with ATM or bank transfer code kindly indicate).
	</i>
</div>

</div>
								<div class="formcontentotherfeedback"></div>
								<div class="cardthanks">
	<div style="text-align:center"><?= Html::img('@web/images/smiley-face-thumbs-up.png', ['alt' => 'My logo']) ?></div>
	<h3 style="text-align:center">We hope to see you soon </h3>
	</div>
	<div id="formcontentotherfeedback2" class="formcontentotherfeedback"></div>
	<div class="question"></div>
	
	
						
						
						 <?
							   PopoverX::end();
						?>
					  </li>
                </ul>
              </nav>
            </div>
			  
            <div class="pull-right nav-item hidden-lg">
					
						<li id="mobile_cart" style="padding-right:4px;"> 
							<?php Pjax::begin(['id' => 'firstcartcounter3']); ?>
							<i class="cart-open icon-shopping111">
								
								<span id="test3">
									<?= Yii::$app->cart->getCount();?> 
								</span>
							</i>
							<?php Pjax::end(); ?>
				</li>
				
						
					 
				<a href="#" class="mobile-nav-toggle nav-hamburger"><span></span></a></div>
          </div>
        </div>
      </div>
      <div class="mobile-nav easing-click-group">
		  <a href="#" class="mobile-nav-toggle"><i class="icon-close47"></i></a><a href="index.html" class="megatron inline logo-light">
          <div class="logo"><?=Html::img("@web/images/trans_logow.png", ["alt" => "loader","id"=>"logo", "class" => "" ]) ?>
          </div>
          <div class="brand">BUYMB.ng</div></a>
        <nav class="mobile-menu easing-link-group">
          <ul>
                    <li><a href="#home">HOME</a></li>
                    <li><a href="#buy-data">BUY DATA</a></li>
                    <li><a href="#buy-airtime">BUY AIRTIME</a></li>
                    
                    <li><a href="#vouchers">VOUCHERS</a></li>
                    <li><a href="#bitcoin">BITCOIN</a></li>
                    <li><a href="#footer">CONTACT</a></li>
			  		<li><a href="<?=Url::to('site/signup');?>">Register</a></li>
                    <? if(!Yii::$app->user->isGuest){ ?>
					  <?= Html::tag('li',Html::a('Logout', Url::to(['/site/logout']), ['data-method' => 'POST','class' => 'text-danger']))  ?>
					  <? }else{?>
					  
                   <li>
					<?
							 PopoverX::begin([	
								'placement' => PopoverX::ALIGN_BOTTOM,
								'toggleButton' => ['label'=>'Login','tag'=>'a', 'class'=>''],
								'header' => '<i class="glyphicon glyphicon-lock"></i> Enter credentials',
								'footer' => Html::button('Submit', [
										'class' => 'btn btn-sm btn-primary', 
										'onclick' => '$(".kv-login-form2").trigger("submit")'
									]) . Html::button('Reset', [
										'class' => 'btn btn-sm btn-default', 
										'onclick' => '$(".kv-login-form2").trigger("reset")'
									])
								]);
// form with an id used for action buttons in footer
							   $form = ActiveForm::begin(['action' => ['login'],'options' => ['class'=>'kv-login-form2']]); ?>

								<?= $form->field($loginModel, 'username',['options'=>[
									'tag'=>'div',
									'class'=>'form-group has-feedback field-loginform-username required'],

									'template'=>'{input}<span class="glyphicon glyphicon-user form-control-feedback"></span>{error}{hint}'
										])->textInput(['placeholder'=>'User Name']) ?>

								<?= $form->field($loginModel, 'password',['options'=>[
									'tag'=>'div',
									'class'=>'form-group has-feedback field-loginform-password required'],
									'template'=>'{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span>{error}{hint}'
										])->passwordInput(['placeholder'=>'Password']) ?>



								<div class="col-xs-8 pull-left" style="padding-left: 0px !important">
							  <div class="checkbox icheck">
								<label>
								  <?= $form->field($loginModel, 'rememberMe')->checkbox() ?>
								</label>
							  </div>
							</div>



								<div style="color:#999;margin:1em 0; clear:both;">
										If you forgot your password you can <?= Html::a('reset it', ['site/request-password-reset']) ?>.
									</div>

                

            					<?php ActiveForm::end(); ?>
						 <?
							   PopoverX::end();
						?>
					</li>
					  <? }?>
					  <? if(!Yii::$app->user->isGuest){ ?>
                    <li class="text-white" style="background:red;"><a href="<?= Url::to(['dashboard/index']);?>">Client area</a></li>
					  <? }?>
          </ul>
        </nav>
      </div>
      <!-- Page Navigation-->
      <!--Page Header-->
			 
      <header id="home" class="page-header home home-slider-1">
        <div class="slider caption-slider control-nav">
          <div class="block-caption-slider overlay-container">
            <div style="background-image: url(assets2/images/background/header1.jpg); background-position: undefined" class="__img-background"></div>
            <div class="overlay bgc-dark-o-2">
              <div class="cell-vertical-wrapper">
                <div class="cell-middle">
                  <div class="caption-preset-simple-7 text-center">
                    <div class="container">
                      <div class="caption-wrapper">
						  <a  class="megatron logo-light size-3 caption">
                            <div class="logo" style="width:400px">
							  <?=Html::img("@web/images/trans_logow.png", ["alt" => "loader","id"=>"logo", "class" => "" ]) ?>
                          </div></a>
                        <div>
                          <!--<h1 class="text-responsive size-ll caption">BUYMB.NG</h1>-->
                        </div>
                        <p class="font-serif-italic fz-3 caption">Nigeria's number one solution to your data needs</p>
                        <div class="__buttons caption">
                        <!-- <a href="index.html" class="btn btn-primary fullwidth">SELECT DEMO</a><br/><a href="http://goo.gl/fi9Eas" target="_blank" class="btn-border btn-light fullwidth">PURCHASE NOW</a>
                         --></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="block-caption-slider overlay-container">
            <div style="background-image: url(assets2/images/background/header2.jpg); background-position: undefined" class="__img-background"></div>
            <div class="overlay bgc-dark-o-2">
              <div class="cell-vertical-wrapper">
                <div class="cell-middle">
                  <div class="caption-preset-simple-7 text-center">
                    <div class="container">
                      <div class="caption-wrapper"><a  class="megatron logo-light size-3 caption">
                          <div class="logo" style="width:400px">
							  <?=Html::img("@web/images/trans_logow.png", ["alt" => "loader","id"=>"logo", "class" => "" ]) ?>
                          </div></a>
                        <div>
                          <!--<h1 class="text-responsive size-ll caption">BUYMB.NG</h1>-->
                        </div>
                        <p class="font-serif-italic fz-3 caption">Nigeria's number one solution to your network needs</p>
                        <div class="__buttons caption"><a href="#footer" class="btn btn-primary fullwidth">CONTACT US</a></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="block-caption-slider overlay-container">
            <div style="background-image: url(assets2/images/background/header3.jpg); background-position: undefined" class="__img-background"></div>
            <div class="overlay bgc-dark-o-4">
              <div class="cell-vertical-wrapper">
                <div class="cell-middle">
                  <div class="caption-preset-simple-7 text-center">
                    <div class="container">
                      <div class="caption-wrapper"><a  class="megatron logo-light size-3 caption">
                          <div class="logo" style="width:400px">
							  <?=Html::img("@web/images/trans_logow.png", ["alt" => "loader","id"=>"logo", "class" => "" ]) ?>
                          </div></a>
                        <div>
                          <!--<h1 class="text-responsive size-ll caption">BUYMB.NG</h1>-->
                        </div>
                        <p class="font-serif-italic fz-3 caption">Nigeria's number one solution to your network needs</p>
                        <div class="__buttons caption"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="block-caption-slider overlay-container">
            <div style="background-image: url(assets2/images/background/header4.jpg); background-position: undefined" class="__img-background"></div>
            <div class="overlay bgc-dark-o-2">
              <div class="cell-vertical-wrapper">
                <div class="cell-middle">
                  <div class="caption-preset-simple-7 text-center">
                    <div class="container">
                      <div class="caption-wrapper"><a  class="megatron logo-light size-3 caption">
                            <div class="logo" style="width:400px">
							  <?=Html::img("@web/images/trans_logow.png", ["alt" => "loader","id"=>"logo", "class" => "" ]) ?>
                          </div></a>
                        <div>
                          <h1 class="text-responsive size-ll caption">Cheap data plan </h1>
                        </div>
                        <p class="font-serif-italic fz-3 caption">Never stay low on airtime or data </p>
                        </div>
                        
                        <div class="__buttons caption"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
      <!--End Page Header-->
      <!--Page Body-->
      <div id="page-body" class="page-body" style="background-color: #f2f4f2 !important;">
        <!-- Services-->
        <section id="buy-data" class="page-section one-child">
			<?= Alert::widget([
				'options' => [
					'class' => 'alert-info addedtocart',
				],
				'body' => '<div class="addedtocartshow"></div>',
				]);?>
          <!--  <div class="container">
            <div class="group-icon-box-border-container">
              <div class="__container-inner clearfix">
				  <?php foreach($dataType as $v){ ?>
                <div class="__border-item">
                  <div data-wow-delay="0.3s" class="block-icon-box-vertical wow fadeInUp">
                    <div class="__icon simple-icon icon-layers"></div>
                    <h4 class="__heading smb"><?=$v['network'];?></h4>
                    <p class="__caption font-serif italic">TARIFF PLANS (3G)</p>
                    <p class="__content">
                     <table class="table-info table display responsive nowrap">
                    <thead>
                      <th>Price </th>
                      <th>Data</th>
                      <th>Bonus</th>
                      <th>Validity</th>
                      <th>Availability</th>
                    </thead>
                    <tbody>
						<?php foreach($v['plans'] as $z){ ?>
                      <tr>
                        <td>N<?=$z['plan_amount'];?></td>
                        <td><?=$z['plan_name'];?></td>
                        <td><?=$z['bonus'];?></td>
                        <td><?=$z['validity'];?></td>
                        <td class='addtocart' data-urlid='<?= Url::to(['order/add-to-cart','id'=>$z->plan_id])?>'><?=($v['status_type'] == 0)?'Available':'Not Available';?></td>
                      </tr>
                      <?php }?>
                    </tbody>

                    </table>
					  <div class="buy_data btn btn-info" data-url="<?= Url::to(['site/guest']);?>">Buy Data Now</div>
					 </p>
                  </div>
                </div>
				<?php }?>
               
              </div>
            </div>
          </div>-->
		  
		   <div class="row product-listing">
                
				<?php foreach($dataType as $v){ ?>
					<?php foreach($v['plans'] as $z){ ?>
                  <div class="col-md-3 col-sm-6 col-xs-12 shop-product-wrapper">
                    <div class="block-shop-product font-heading">
                      <div class="__image overlay-container">
						  <div class="imagediv">
						  	<?= Html::img('@web/images/network_logo/'.$v->type_img, ['alt' => 'Network Image', 'class' => '' ]); ?>
						  </div>
						  <div class="loaderdive">
						  	<div id="fountainTextG"><div id="fountainTextG_1" class="fountainTextG">A</div><div id="fountainTextG_2" class="fountainTextG">d</div><div id="fountainTextG_3" class="fountainTextG">d</div><div id="fountainTextG_4" class="fountainTextG">i</div><div id="fountainTextG_5" class="fountainTextG">n</div><div id="fountainTextG_6" class="fountainTextG">g</div><div id="fountainTextG_7" class="fountainTextG"> </div><div id="fountainTextG_8" class="fountainTextG">T</div><div id="fountainTextG_9" class="fountainTextG">o</div><div id="fountainTextG_10" class="fountainTextG"> </div><div id="fountainTextG_11" class="fountainTextG">C</div><div id="fountainTextG_12" class="fountainTextG">a</div><div id="fountainTextG_13" class="fountainTextG">r</div><div id="fountainTextG_14" class="fountainTextG">t</div><div id="fountainTextG_15" class="fountainTextG">.</div><div id="fountainTextG_16" class="fountainTextG">.</div><div id="fountainTextG_17" class="fountainTextG">.</div><div id="fountainTextG_18" class="fountainTextG">.</div><div id="fountainTextG_19" class="fountainTextG">.</div><div id="fountainTextG_20" class="fountainTextG">.</div></div>
						  </div>
						  
                        <div class="overlay text-center">
                          <div class="__layer bgc-dark-o-3"></div>
                          <ul>
                            <li class="clearfix">
								<a href="#" class="compare">
									
									<i class="icon-buy-now" data-planid = '<?= $z->plan_id; ?>' data-url = '<?= Url::to(['site/guest']); ?>' data-networkid = '<?= $v->type_id; ?>'><?=Html::img("@web/images/buynow.png", ["alt" => "loader","id"=>"", "class" => "dashicons" ]) ?></i>
								</a>
								
								<a class='addtocart' data-urlid='<?= Url::to(['order/add-to-cart','id'=>$z->plan_id])?>'>
									<i class=""><?=Html::img("@web/images/addtocart.jpg", ["alt" => "loader","id"=>"", "class" => "dashicons" ]) ?></i>
								</a>
							  </li>
                          </ul>
                        </div>
                      </div>
                      <div class="__info text-center"><a href="#"><?=$v->network;?> <?=$z->plan_name;?></a>
                        <div class="__price"><span>N<?=$z->plan_amount;?></span>
                        </div>
                        
                      </div>
                    </div>
			   </div>
                  <?php }?>
                  <?php }?>
                
              </div>
        </section>
        <!-- End Services-->
        <!-- Call To Action-->
        <section class="page-section no-padding">
          <div class="call-to-action-common bgc-secondary">
            <div class="container">
              <div class="__content-wrapper row">
                <div class="col-lg-10 col-md-9 col-xs-12 __content-left">
                  <div class="cell-vertical-wrapper">
                    <div class="cell-middle">
                      <p class="font-serif-italic mb-0 fz-3 color-light">Like what you see? Want to sell airtime or buy data ? Get started now  <a href="#footer">Register Now</a></p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- End Call To Action-->
        <!-- About-->
        <section id="buy-airtime" class="page-section">
          <div class="container">
            <div class="row">
              <div data-wow-delay="0.3s" class="col-md-6 section-block wow fadeInUp">
                <header class="hr-header">
                  <h2 class="smb">OUT OF AIRTIME? NO PROBLEM</h2>
                  <p class="common-serif __caption">We've got you covered</p>
                  <div class="separator-2-color"></div>
                </header>
                <p class="hmb">Buy your airtime here from any desired network of your choice. Within seconds you can recharge your phone and continue that call and sent that text! Never be stranded again. <br>Your network not represented here? We apologize for that, please contact us and let us know your preferred network. We ensure the best service!</p>
              </div>
              <div data-wow-delay="0.3s" class="col-md-6 wow fadeInRight">
                <div class="accordion highlight-caret">
				<? foreach($airtimeType as $k => $v){?>
                  <div class="accordion-header">
                    <div class="__icon"><i></i></div>
                    <div class="__title">
                      <p class="font-heading mb-0"><? echo $v->network;?></p>
                    </div>
                  </div>
                  <div class="accordion-content">
                    <p class="mb-0">
					  
					  <div class="data-orderform">

    <?php $form = ActiveForm::begin(['action' =>['folder/create'],'options'=>['class'=>'dataform']]); ?>
	
		<?= $form->field($model, 'plan_id_selector')->hiddenInput([ 'class' => 'form-control plantype','value' => $v->type_id])->label(false); ?>
    
	<?= $form->field($model, 'plan_id')->textInput(['class'=>'plan_id','data-content'=>'pls enter amount here','data-placement'=>'top'])->label('Enter amount') ?>
	
       <? if (!Yii::$app->user->isGuest) {
				$payment = ['card'=>'Card','voucher'=>'Voucher','wallet'=>'Wallet','transfer'=>'Bank Transfer'];
			}else{
				$payment = ['card'=>'Card','voucher'=>'Voucher','transfer'=>'Bank Transfer'];
			}
	?>
		
		
		<? if (!Yii::$app->user->isGuest) {?>
		Buy for a friend <input type="checkbox" value="1" name="friend" class="buyfriend" />
		<? }?>
						  
	<div class="numbers">
			<?= $form->field($contactModel, 'phone_number')->textInput(['class'=>'phone_number'])->label(Yii::$app->user->isGuest?'Enter Phone Number':'Enter Friend Phone Number') ?>
		</div>
	
		<?= $form->field($model, 'payment_mode')->dropDownList($payment,[ 'prompt'=> 'Select...', 'class' => 'form-control paymentmode' ]) ?>
	<div class="voucher">
		<label  class="control-label">Enter Voucher</label>
		<input type="text" class="form-control pin" />
	</div>
		
	
        
    
        <div class="form-group">
            <? //= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
			<?= Html::submitButton($model->isNewRecord ? '<span class="submitButtonText">Submit</span>'.Html::img("@web/images/45.gif", ["alt" => "loader","class" => "user-image23 BtnLoader" ]).' <span class="feedback"><span>' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-basic submitbtn' : 'btn btn-basic submitbtn']) ?>

        </div>
    <?php ActiveForm::end(); ?>
	
</div>
					  
					  </p>
                  </div>
                  
                 
                 <? };?>
                 
                  
                  
                </div>
              </div>
            </div>
          </div>
          
         
        </section>
        <!-- End About-->
        <!-- Team-->
        
        <!-- End Team-->
        <!-- Testimonial-->
        <section id="vouchers" class="page-section bgc-dark-o-5 no-padding">
          <div data-parallax="scroll" data-image-src="assets/images/background/background-33.jpg" data-speed="0.3" class="parallax-background"></div>
          <div class="testimonial style-3 text-center color-light">
            <div class="container">
              <div class="row">
                <div class="col-lg-10 col-lg-offset-1 col-xs-12">
                  <h1 class="size-ll text-responsive">Buy And Sell<br/> Voucher</h1>
                  
                  <div class="slider slide block-testimonial-wrapper direction-nav mb-65">
                    <div class="block-testimonial">
                      <div class="__content text-center">
                        <p>Do you need an instant Cash? buy our voucher and resell to friends and family</p>
                        
                        
                      </div>
                    </div>
                    <div class="block-testimonial">
                      <div class="__content text-center">
                        <p>Use our voucher to buy airtime and data</p>
                        
                        
                      </div>
                    </div>
                    
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
        </section>
        
        <section id="bitcoin" class="page-section bgc-gray-lightest">
          <div class="container text-center section-header">
            <header class="hr-header">
              <h2 class="smb">Bitcoin</h2>
              <p class="common-serif __caption">Buy and Sell Bitcoin</p>
              <div class="separator-2-color"></div>
            </header>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-md-12 col-xs-12 pt-10">
                
                  
                  <div class="__content">
                    
                    
                    <p class="__text">Coming soon...  </p>
                  </div>
                
              </div>
              
            </div>
          </div>
        </section>
        <?
		Modal::begin([
			'header' =>'<h1 id="headers">Checkout</h1>',
			'id' => 'details3',
			
			'size' => 'modal-md',  
		]);
?>
<div id="formcontent">

</div>
<div id="formcontentotherfeedback0" class="formcontentotherfeedback"></div>
<div class="cardthanks">
	<div style="text-align:center"><?= Html::img('@web/images/smiley-face-thumbs-up.png', ['alt' => 'My logo']) ?></div>
	<h3 style="text-align:center">We hope to see you soon </h3>
	</div>

<div class="formcontenttransferfeedback">
	
	<div>You are expected to pay <em id="actualamountmain"></em></div>
<div>
<h4>Bank Details</h4>
<stong>Account Name </stong>Emeka Uchechukwu Michael<BR><HR/>
<stong>GTBANK</stong><em> 0215001020</em><BR><HR/>
<stong> ACCESS BANK</stong><em> 0042461814</em><BR><HR/>
<stong>FIRST BANK</stong><em> 3119596327</em><BR><HR/>
</div>
<div>
	<i>
		Please after payment send name of depositor, bank paid to , to 09037072904 for confirmation ( if you paid with ATM or bank transfer code kindly indicate).
	</i>
</div>

</div>

<?
	Modal::end();
?>
	
	
	
<? 
		Modal::begin([
			'header' =>'<h1 id="mobilecartheader">cart </h1>',
			'id' => 'mobilecart',
			
			'size' => 'modal-md',  
		]);
?>
<div id="cartformcontent">

							 
						<?php Pjax::begin(['id' => 'firstcart3']); ?>
						   <div class="bgc-gray-darkest">
							<? if(Yii::$app->cart->getCount() != 0){?>
						 <? foreach(Yii::$app->cart->positions as $key => $value){?>
						   <div class="__cart-item" style="border-bottom:1px solid #ccc">
                        <div class="__image" style="display:inline"><img src="assets/images/shop/product-img-1-s.jpg" alt="Shop Product" class="img-responsive"/></div>
                        <div class="__info">
                          <div class="__category font-serif-italic fz-6-s">network/plan: <?= $value->plan_name;?></div>
                          <div class="font-heading fz-6-ss"><a href="#">Phone number:<?= $value->phone_number;?></a></div><span class="__price color-primary font-heading">1 x $<?= $value->plan_amount;?></span>
                        </div><a href="#" class="__remove icon-185090-delete-garbage-streamline"></a>
                      </div>
					 <? } ?>
						<div class="__cart-item" style="border-bottom:1px solid #ccc">
                        <div class="__image" style="display:inline">
							
							clear cart
						</div>
                        <div class="__info">
                          subtotal : <?= \Yii::$app->cart->getCost();?>
                        </div>
                      </div>
						
					<?= $this->render('guestorder3', ['model' => $model]) ?>
							   <? }else{?>
							 <h2>
							   Cart is empty. 
							   </h2>  
							   <? }?>
                      </div>
						<?php Pjax::end(); ?>
						
						 
</div>
	
		   <div id="mobilefeedback" class="formcontenttransferfeedback">
	
	<div>You are expected to pay <em class="actualamount"></em></div>
<div>
<h4>Bank Details</h4>
<stong>Account Name </stong>Emeka Uchechukwu Michael<BR><HR/>
<stong>GTBANK</stong><em> 0215001020</em><BR><HR/>
<stong> ACCESS BANK</stong><em> 0042461814</em><BR><HR/>
<stong>FIRST BANK</stong><em> 3119596327</em><BR><HR/>
</div>
<div>
	<i>
		Please after payment send name of depositor, bank paid to , to 09037072904 for confirmation ( if you paid with ATM or bank transfer code kindly indicate).
	</i>
</div>

</div>
			<div id="formcontentotherfeedback3" class="formcontentotherfeedback"></div>
			<div class="cardthanks">
	<div style="text-align:center"><?= Html::img('@web/images/smiley-face-thumbs-up.png', ['alt' => 'My logo']) ?></div>
	<h3 style="text-align:center">We hope to see you soon </h3>
	</div>
<?
	Modal::end();
?>

<?

$url = Url::to(['site/approve']);
$contactUrl = Url::to(['site/contact']);
$voucheUrl = Url::to(['order/paymentwithvoucher']);
$walletUrl = Url::to(['order/paymentwithwallet']);
$cardValidationUrl = Url::to(['order/paymentwithcardvalidation']);
$cashAprovedUrl = Url::to(['order/paymentwithcardairtime']);
$transferUrl = Url::to(['order/paymentbytransfer']);
$actualAmountUrl = Url::to(['site/actualamount']);
$totalCount = Yii::$app->cart->getCount();

$percentage = Yii::$app->params['rate'];


$this->registerJsFile(
    'https://js.paystack.co/v1/inline.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
$order = <<<JS

$('.cart-open').click(function(){
 
$('.formcontenttransferfeedback').hide();
$('.bgc-gray-darkest').show();

})

$('.addtocart').click(function(){
	phoneNumber = prompt('Please Enter Phone number Below');
	if(phoneNumber){
	
	$(this).parent().parent().parent().parent().find('.imagediv').hide()
	$(this).parent().parent().parent().parent().find('.loaderdive').show();
		url = $(this).data('urlid')+'&phonenumber='+phoneNumber;
   
		 $.post(url)
			.always(function(result){
			
			$(document).find('.imagediv').show();
			$(document).find('.loaderdive').hide();
			 $.pjax.reload({container:"#firstcart",async: false}); 
			 $.pjax.reload({container:"#firstcart2",async: false}); 
			 $.pjax.reload({container:"#firstcart3",async: false}); 
			 $.pjax.reload({container:"#firstcartcounter3",async: false}); 
			 $('.addedtocartshow').html('product have been added to the cart')
			 $('#test1').load(' #test1');
			 $('#test2').load(' #test2');
			 $('#test3').load(' #test3');
			 
			 $('.addedtocart').show();
			$(".addedtocart")[0].scrollIntoView({
					behavior: "smooth", // or "auto" or "instant"
					block: "end" // or "end"
				});

					setTimeout(function(){
					 $('.addedtocart').hide();
					}, 20000);
	

			}).fail(function(){
				console.log('Server Error');
			});
		
	
	}
   
})
$('.icon-buy-now').click(function(){
	
	var url=$(this).data('url');
	var network=$(this).data('networkid');
	var plan=$(this).data('planid');
	$('.hideall').hide();
	$('#successTransfer').hide();
	$('#errorTransfer').hide();
	$('.formcontenttransferfeedback').hide();
	
	$('#formcontent').show();
	
	
	$('#details3').modal('show').find('#formcontent').html('<img class="loadergif" src="images/loader.gif"  />');
	$('#details3').modal('show').find('#formcontent').load(url,function() {
  $('#formcontent').find('#networkid').val(network);
	$('#formcontent').find('#plan_id').val(plan);
});
	
	
});

$('#mobile_cart').click(function(){
	
	var datas=$(this).data('url');
	$('.hideall').hide();
	$('#successTransfer').hide();
	$('#errorTransfer').hide();
	$('#formcontent').show();
	
	$('#mobilecart').modal('show');
	
});

$('#contact_form').on('submit', function (event) {
            event.preventDefault();

            var submit_button = $(this).find('button[type="submit"]');
            var backup_button = submit_button.html();
            var data = $(this).serialize();

            submit_button.html('PROCESSING').attr('disabled', 'disabled');

            $.ajax({
                type: "POST",
                url: '$contactUrl',
                data: data,

                // Notify result
                success : function (result) {
                    if(result == 1){
                        submit_button.html('SUCCESSFUL <i class="fa fa-check"></i>');
                        setTimeout(function(){
                            submit_button.removeAttr('disabled').html(backup_button);
                        },2000)
                    }
                    else{
                        alert(result);
                        submit_button.removeAttr('disabled').html(backup_button);
                    }
                }
            });
        })


// used to  process the airtime form 

$('.paymentmode').change(function(){
	value = $(this).val();
	if(value == 'voucher'){
		$('.voucher').show();
	} else{
		$('.voucher').hide();
	}
});



$('.plan_id').change(function(){
	planid = $(this).val();
	currentValue = $(this);
	language = 0;
	breakdown = ($percentage / 100) * planid;
	amoutToPay = planid - breakdown;
	currentValue.attr('data-content','you are to pay '+amoutToPay+' Naira for this plan');
	 currentValue.popover('show');
   

});



$('.buyfriend').click(function(){

	value = $(this).checked;
	if($(this).is(":checked")){
		$('.numbers').show();
	} else{
		$('.numbers').hide();
	}
});


function payWithPaystackfake(test){
		alert(test);
  }
  
$('.dataform').on('beforeSubmit', function (e) {
	$('.submitButtonText').hide();
 	$('.BtnLoader').show();
    var \$form = $(this);
	var planid = $('.plan_id').val();
	breakdown = ($percentage / 100) * planid;
	amoutToPay = planid - breakdown;
	var paymentMode = $('.paymentmode').val();
	
	function payWithPaystack(customerEmail,amount,createOrder){
		var handler = PaystackPop.setup({
			
			  key: 'pk_live_60e06110fdefa3e025e94acf315c0baf26f2ca63',
			  email: customerEmail,
			  amount: amount+'00',
			  
			  metadata: {
				 custom_fields: [
					{
						display_name: "Mobile Number",
						variable_name: "mobile_number",
						value: "+2348012345678"
					}
				 ]
			  },
			  callback: function(response){
				  
				  
				  $.post('$cashAprovedUrl'+'?amountid='+$('.plan_id').val()+'& ordertype=airtime',$('.dataform').serialize())
		.always(function(result){
	
			
   			if(result==1){
	   			$('.BtnLoader').hide();
	   			$(document).find('.feedback').html('Sent').show();
				setTimeout(function(){ 
						$(document).find('.feedback').hide();
						$(document).find('.BtnLoader').hide();
						$(document).find('.submitButtonText').show();
						$.pjax.reload({container:"#buy_airtime",async: false}); 

						$("#example1").DataTable({
							"aaSorting": [],
							"pagingType": "simple",
							"responsive": "true",

						});
					}, 5000);
    		}else{
				$('.BtnLoader').hide();
				$(document).find('.feedback').html('Not enought funds in wallet').show();
				setTimeout(function(){ 
						$(document).find('.feedback').hide();
						$(document).find('.BtnLoader').hide();
						$(document).find('.submitButtonText').show();
						$.pjax.reload({container:"#buy_airtime",async: false}); 

						$("#example1").DataTable({
							"aaSorting": [],
							"pagingType": "simple",
							"responsive": "true",

						});
					}, 5000);
	
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
		
			  },
			  onClose: function(){
				  //alert('window closed');
			  }
		});
		handler.openIframe();
  }
  
	
	function createOrder(planid){
		$.post('$cashAprovedUrl'+'?amountid='+planid+'& ordertype=airtime',\$form.serialize())
		.always(function(result){
	
			
   			if(result==1){
	   			$('.BtnLoader').hide();
	   			$(document).find('.feedback').html('Sent').show();
				setTimeout(function(){ 
						$(document).find('.feedback').hide();
						$(document).find('.BtnLoader').hide();
						$(document).find('.submitButtonText').show();
						$.pjax.reload({container:"#buy_airtime",async: false}); 

						$("#example1").DataTable({
							"aaSorting": [],
							"pagingType": "simple",
							"responsive": "true",

						});
					}, 5000);
    		}else{
				$('.BtnLoader').hide();
				$(document).find('.feedback').html('Not enought funds in wallet').show();
				setTimeout(function(){ 
						$(document).find('.feedback').hide();
						$(document).find('.BtnLoader').hide();
						$(document).find('.submitButtonText').show();
						$.pjax.reload({container:"#buy_airtime",async: false}); 

						$("#example1").DataTable({
							"aaSorting": [],
							"pagingType": "simple",
							"responsive": "true",

						});
					}, 5000);
	
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	}
	
	
	
		
		
	
	if(paymentMode == 'wallet'){
		$.post('$walletUrl'+'?amountid='+planid+'& ordertype=airtime',\$form.serialize())
		.always(function(result){
	
			
   			if(result==1){
	   			$('.BtnLoader').hide();
	   			$(document).find('.feedback').html('Sent').show();
				setTimeout(function(){ 
						$(document).find('.feedback').hide();
						$(document).find('.BtnLoader').hide();
						$(document).find('.submitButtonText').show();
						$.pjax.reload({container:"#buy_airtime",async: false}); 

						$("#example1").DataTable({
							"aaSorting": [],
							"pagingType": "simple",
							"responsive": "true",

						});
					}, 5000);
    		}else{
				$('.BtnLoader').hide();
				$(document).find('.feedback').html('Not enought funds in wallet').show();
				setTimeout(function(){ 
						$(document).find('.feedback').hide();
						$(document).find('.BtnLoader').hide();
						$(document).find('.submitButtonText').show();
						$.pjax.reload({container:"#buy_airtime",async: false}); 

						$("#example1").DataTable({
							"aaSorting": [],
							"pagingType": "simple",
							"responsive": "true",

						});
					}, 5000);
	
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	} else if(paymentMode == 'transfer'){
	
		$.post('$transferUrl'+'?amountid='+planid+'& ordertype=airtime',\$form.serialize())
		.always(function(result){
	
			
   			if(result==1){
				$('#details3').modal('show').find('.actualamount').html(amoutToPay +' Naira');
	   			$('.BtnLoader').hide();
	   			$(document).find('.feedback').html('Sent').show();
				setTimeout(function(){ 
						$(document).find('.feedback').hide();
						$(document).find('.BtnLoader').hide();
						$(document).find('.submitButtonText').show();
						$.pjax.reload({container:"#buy_airtime",async: false}); 

						$("#example1").DataTable({
							"aaSorting": [],
							"pagingType": "simple",
							"responsive": "true",

						});
					}, 5000);
				
    		}else{
				$('.BtnLoader').hide();
				$(document).find('.feedback').html('An Error occured pls try again').show();
				setTimeout(function(){ 
						$(document).find('.feedback').hide();
						$(document).find('.BtnLoader').hide();
						$(document).find('.submitButtonText').show();
						$.pjax.reload({container:"#buy_airtime",async: false}); 

						$("#example1").DataTable({
							"aaSorting": [],
							"pagingType": "simple",
							"responsive": "true",

						});
					}, 5000);
				
				
	
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	}else if (paymentMode == 'card'){
		$.post('$cardValidationUrl'+'?amountid='+planid+'&ordertype=airtime',\$form.serialize())
		.always(function(result){
	
			
   			if(result.success==1){
				
				customerEmail = result.email;
				if(customerEmail.lenght == 0){
					 customerEmail = 'kingsonly13c@gmail.com';
					 setTimeout(function(){ 
						$(document).find('.feedback').hide();
						$(document).find('.BtnLoader').hide();
						$(document).find('.submitButtonText').show();
						$.pjax.reload({container:"#buy_airtime",async: false}); 

						$("#example1").DataTable({
							"aaSorting": [],
							"pagingType": "simple",
							"responsive": "true",

						});
					}, 5000);
				}
				
				payWithPaystack(customerEmail,result.amount,createOrder);
	   			
    		}else{
				$('.BtnLoader').hide();
				$(document).find('.feedback').html('Not enought funds in wallet').show();
				setTimeout(function(){ 
						$(document).find('.feedback').hide();
						$(document).find('.BtnLoader').hide();
						$(document).find('.submitButtonText').show();
						$.pjax.reload({container:"#buy_airtime",async: false}); 

						$("#example1").DataTable({
							"aaSorting": [],
							"pagingType": "simple",
							"responsive": "true",

						});
					}, 5000);
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	} else if (paymentMode == 'voucher'){
			var pin = $('.pin').val();
			$.post('$voucheUrl'+'?pin='+pin+'& amountid='+planid+'& ordertype=airtime',\$form.serialize())
		.always(function(result){
	
			$(document).find('.loader').hide();
   			if(result==1){
			$('.BtnLoader').hide();
	   
	   			$(document).find('.feedback').html('sent').show();
				
				setTimeout(function(){ 
						$(document).find('.feedback').hide();
						$(document).find('.BtnLoader').hide();
						$(document).find('.submitButtonText').show();
						$.pjax.reload({container:"#buy_airtime",async: false}); 

						$("#example1").DataTable({
							"aaSorting": [],
							"pagingType": "simple",
							"responsive": "true",

						});
					}, 5000);
    		}else{
				$('.BtnLoader').hide();
				$(document).find('.feedback').html('Invalid or Pin used').show();
				
				setTimeout(function(){ 
						$(document).find('.feedback').hide();
						$(document).find('.BtnLoader').hide();
						$(document).find('.submitButtonText').show();
						$.pjax.reload({container:"#buy_airtime",async: false}); 

						$("#example1").DataTable({
							"aaSorting": [],
							"pagingType": "simple",
							"responsive": "true",

						});
					}, 5000);
	
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	} else{
		alert('no option selected');
	}
    
	
	
    return false;
 	
    
});


JS;
 
$this->registerJs($order);
?>

   