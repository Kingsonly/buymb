<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use frontend\models\planType;

use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\models\order */
/* @var $form ActiveForm */
?>

<style>
	<? 
if (!Yii::$app->user->isGuest) {
           
        
?>
	
	.numbers{
		display: none;
		
	}
	
<? }?>
	.voucher{
		display: none;
		margin-bottom: 10px;
	}
	#BtnLoader2{
		display:none;
	}
</style>
<div class="data-orderform">

    <?php $form = ActiveForm::begin(['action' =>['folder/create'],'id' => 'dataforms2']); ?>
	
        <? if (!Yii::$app->user->isGuest) {
				$payment = ['card'=>'ATM Card','voucher'=>'Voucher','wallet'=>'Wallet','transfer'=>'Bank Transfer'];
			}else{
				$payment = ['card'=>'ATM Card','voucher'=>'Voucher','transfer'=>'Bank Transfer'];
			}
	?>
	
        
		
		
	
	
		<?= $form->field($model, 'payment_mode')->dropDownList($payment,[ 'prompt'=> 'Select...', 'class' => 'form-control', 'id' => 'paymentmode2']) ?>
	
	<div class="voucher">
		<label  class="control-label">Enter Voucher</label>
		<input type="text" class="form-control pin" id = "pin2" />
	</div>
		
	
        
    <?= $form->field($model, 'customer_email')->textInput(['class'=>'customer_email']);
		?>
        <div class="form-group">
            <? //= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
			<?= Html::submitButton($model->isNewRecord ? '<span id="submitButtonText2">Submit</span>'.Html::img("@web/images/45.gif", ["alt" => "loader","id"=>"BtnLoader2", "class" => "user-image23 BtnLoader" ]).' <span id="feedback2"><span>' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-basic' : 'btn btn-basic','id'=>'submitbtn2']) ?>

        </div>
    <?php ActiveForm::end(); ?>
	
	
</div><!-- data-orderform -->
<?
$voucheUrl = Url::to(['order/bulk-paymentwithvoucher']);
$walletUrl = Url::to(['order/paymentwithwallet']);
$cardValidationUrl = Url::to(['order/bulk-paymentwithcardvalidation']);
$cashAprovedUrl = Url::to(['order/bulkpaymentwithcarddata']);
$transferUrl = Url::to(['order/bulk-paymentbytransfer']);
$actualAmountUrl = Url::to(['site/actualamountdata']);
$actualAmountUrlmain = \Yii::$app->cart->getCost();
$feedbackUrl = Url::to(['site/usersfeedback','mainId'=>2]);

$this->registerJsFile(
    'https://js.paystack.co/v1/inline.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
$order = <<<JS
$('#formcontentotherfeedback2').load('$feedbackUrl');
$('#paymentmode2').change(function(){
	value = $(this).val();
	if(value == 'voucher'){
		$('.voucher').show();
	} else{
		$('.voucher').hide();
	}
});

$('#buyfriend2').click(function(){
	value = $(this).checked;
	if($(this).is(":checked")){
		$('.numbers').show();
	} else{
		$('.numbers').hide();
	}
});


  
$('#dataforms2').on('beforeSubmit', function (e) {
	$('#submitButtonText2').hide();
 	$('#BtnLoader2').show();
    var \$form = $(this);
	var customermail = \$form.find('.customer_email').val();
	var planid = $('#plan_id2').val();
	var paymentMode = $('#paymentmode2').val();
	
	var mainActualAmount = 0;
	
	
	function payWithPaystack(customerEmail,amount,createOrder){
	//alert(amount);
		var handler = PaystackPop.setup({
			
			  //key: 'pk_live_60e06110fdefa3e025e94acf315c0baf26f2ca63',
			  key: 'pk_test_9fffdfb13c6194a5a236b933ddb00aa29a78dd78',
			  email: customermail,
			  amount: amount+'00',
			  
			  metadata: {
				 custom_fields: [
					{
						display_name: "Mobile Number",
						variable_name: "mobile_number",
						value: "+2348012345678"
					}
				 ]
			  },
			  callback: function(response){
				  
				  
				  $.post('$cashAprovedUrl'+'?ordertype=data',$('#dataform').serialize())
		.always(function(result){
	
			
   			if(result==1){
	   			$('#BtnLoader2').hide();
	   			$(document).find('#feedback2').html('Sent').show();
				\$form.parent().parent().parent().parent().find('.bgc-gray-darkest').hide();
			 \$form.parent().parent().parent().parent().find('.formcontentotherfeedback').show();
				\$form.parent().parent().parent().parent().find('.formcontentotherfeedback #hiden').val(1);
    		}else{
				$('#BtnLoader2').hide();
				$(document).find('#feedback2').html('Not enought funds in wallet').show();
	
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
		
			  },
			  onClose: function(){
				  location.reload();
			  }
		});
		handler.openIframe();
  }
  
	
	function createOrder(planid){
		$.post('$cashAprovedUrl'+'?amountid='+planid+'& ordertype=data',\$form.serialize())
		.always(function(result){
	
			
   			if(result==1){
	   			$('#BtnLoader2').hide();
	   			$(document).find('#feedback2').html('Sent').show();
    		}else{
				$('#BtnLoader2').hide();
				$(document).find('#feedback2').html('Not enought funds in wallet').show();
	
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	}
	
	if(paymentMode == 'wallet'){
	
		$.post('$walletUrl'+'?amountid='+planid+'& ordertype=data',\$form.serialize())
		.always(function(result){
	
			
   			if(result==1){
	   			$('#BtnLoader2').hide();
	   			$(document).find('#feedback2').html('Sent').show();
				countdownexploder();
    		}else{
				$('#BtnLoader2').hide();
				$(document).find('#feedback2').html('Not enought funds in wallet').show();
				countdownexploder();
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	}else if(paymentMode == 'transfer'){
	   
	$(document).find('.actualamount').html('$actualAmountUrlmain');
		$.post('$transferUrl'+'?ordertype=data',\$form.serialize())
		.always(function(result){
		
			
   			if(result==1){
			$('#BtnLoader2').hide();
	   			$(document).find('#feedback2').html('Sent').show();
				\$form.parent().parent().parent().parent().find('.bgc-gray-darkest').hide();
			 \$form.parent().parent().parent().parent().find('.formcontentotherfeedback').show();
				\$form.parent().parent().parent().parent().find('.formcontentotherfeedback #hiden').val(2);
				
    		}else{
				alert('error')
	
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	} else if (paymentMode == 'card'){
		$.post('$cardValidationUrl'+'?amountid='+planid,\$form.serialize())
		.always(function(result){
	
			
   			if(result.success==1){
				
				customerEmail = result.email;
				if(customerEmail.lenght == 0){
				 customerEmail = 'kingsonly13c@gmail.com';
				}
				
				payWithPaystack(customerEmail,result.amount,createOrder);
				
				
	   			
    		}else{
				$('#BtnLoader2').hide();
				$(document).find('#feedback2').html('Not enought funds in wallet').show();
				setTimeout(function(){ 
				$(document).find('#feedback2').hide();
				$(document).find('#BtnLoader2').hide();
				$(document).find('#submitButtonText2').show();
				$.pjax.reload({container:"#buy_data",async: false}); 
				}, 5000);
	
				
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	} else if (paymentMode == 'voucher'){
			var pin = $('#pin2').val();
			$.post('$voucheUrl'+'?pin='+pin+'& amountid='+planid+'& ordertype=data',\$form.serialize())
		.always(function(result){
	
			$(document).find('#loader2').hide();
   			if(result==1){
			$('#BtnLoader2').hide();
	   
	   			$(document).find('#feedback2').html('sent').show();
				$('#BtnLoader2').hide();
				$(document).find('.bgc-gray-darkest').hide();
				$(document).find('.formcontentotherfeedback').slideDown();
				countdownexploder();
    		}else{
				$('#BtnLoader2').hide();
				$(document).find('#feedback2').html('Invalid or Pin used').show();
				countdownexploder();
	
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	} else{
		alert('no option selected');
	}
    
	
	setTimeout(function(){
	$('#test1').load(' #test1');
	$('#test2').load(' #test2');
	$('#test3').load(' #test3');
	$('#firstcart2').load(' #firstcart2');
	//$.pjax.reload({container:"#firstcart",async: false}); 
	//$.pjax.reload({container:"#firstcart2",async: false}); 
	$(document).find('#feedback2').hide();
	$(document).find('#BtnLoader2').hide();
	$(document).find('#submitButtonText2').show();
	$.pjax.reload({container:"#buy_data2",async: false}); 
	}, 3000);
    return false;
 	
    
});


JS;
 
$this->registerJs($order);
?>




