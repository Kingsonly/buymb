<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Voucher */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
	
	#BtnLoader{
		display:none;
	}
	
</style>
<div class="voucher-form">

    <?php $form = ActiveForm::begin(['id'=>'voucherform']); ?>

    <?= $form->field($model, 'amount')->textInput(['maxlength' => true,'id'=>'unit']) ?>

    <?= $form->field($model, 'quantity')->textInput(['id'=>'quantity']) ?>
	
	<? $payment = ['card'=>'Card','wallet'=>'Wallet'] ?>
	
	<?= $form->field($model, 'payment_mode')->dropDownList($payment,[ 'prompt'=> 'Select...', 'class' => 'form-control', 'id' => 'paymentmode']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '<span id="submitButtonText">Submit</span>'.Html::img("@web/images/45.gif", ["alt" => "loader","id"=>"BtnLoader", "class" => "user-image23" ]).' <span id="feedback"><span>' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-basic' : 'btn btn-basic','id'=>'submitbtn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?
$walletUrl = Url::to(['voucher/walletpayment']);
$cardUrl = Url::to(['voucher/cardpayment']);
$this->registerJsFile(
    'https://js.paystack.co/v1/inline.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
$voucher = <<<JS

$('#voucherform').on('beforeSubmit', function (e) {
	$('#submitButtonText').hide();
 	$('#BtnLoader').show();
    var \$form = $(this);
	var unit = $('#unit').val();
	var quantity = $('#quantity').val();
	
	var paymentMode = $('#paymentmode').val();
	
	
	function payWithPaystack(customerEmail,amount){
		var handler = PaystackPop.setup({
			
			  key: 'pk_live_60e06110fdefa3e025e94acf315c0baf26f2ca63',
			  email: customerEmail,
			  amount: amount+'00',
			  
			  metadata: {
				 custom_fields: [
					{
						display_name: "Mobile Number",
						variable_name: "mobile_number",
						value: "+2348012345678"
					}
				 ]
			  },
			  callback: function(response){
				  
				  
				  $.post('$cardUrl'+'?unit='+$('#unit').val()+'& quantity='+$('#quantity').val(),$('#voucherform').serialize())
		.always(function(result){
	
			
   			if(result==1){
	   			$('#BtnLoader').hide();
	   			$(document).find('#feedback').html('Sent').show();
    		}else{
				$('#BtnLoader').hide();
				$(document).find('#feedback').html('Not enought funds in wallet').show();
	
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
		alert('yes');
			  },
			  onClose: function(){
				  //alert('window closed');
			  }
		});
		handler.openIframe();
  }
  
  
	if(paymentMode == 'wallet'){
		$.post('$walletUrl'+'?unit='+unit+'& quantity='+quantity,\$form.serialize())
		.always(function(result){
	
			
   			if(result==1){
	   			$('#BtnLoader').hide();
	   			$(document).find('#feedback').html('Sent').show();
    		}else{
				$('#BtnLoader').hide();
				$(document).find('#feedback').html('Not enought funds in wallet').show();
	
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	} else if (paymentMode == 'card'){
	amount = unit *  quantity;
		payWithPaystack('kingsonly13c@gmail.com',amount);
	} else{
		alert('no option selected');
	}
    
	
	setTimeout(function(){ 
	$(document).find('#feedback').hide();
	$(document).find('#BtnLoader').hide();
	$(document).find('#submitButtonText').show();
	$.pjax.reload({container:"#buy_voucher",async: false}); 
	}, 5000);
    return false;
 	
    
});


JS;
 
$this->registerJs($voucher);
?>

