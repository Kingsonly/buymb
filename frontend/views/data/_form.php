<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Plan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="plan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'plan_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'plan_amount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'plan_type_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?
$this->registerJsFile(
    'https://js.paystack.co/v1/inline.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
$voucheUrl = Url::to(['order/paymentwithvoucher']);
$walletUrl = Url::to(['order/paymentwithwallet']);
$cardValidationUrl = Url::to(['order/paymentwithcardvalidation']);
$cashAprovedUrl = Url::to(['order/paymentwithcarddata']);

$order = <<<JS

$('#paymentmode').change(function(){
	value = $(this).val();
	if(value == 'voucher'){
		$('#voucher').show();
	} else{
		$('#voucher').hide();
	}
});


$('#dataform').on('beforeSubmit', function (e) {
	$('#submitButtonText').hide();
 	$('#BtnLoader').show();
    var \$form = $(this);
	var planid = $('#plan_id').val();
	var paymentMode = $('#paymentmode').val();
	
	function payWithPaystack(customerEmail,amount,createOrder){
		var handler = PaystackPop.setup({
			
			  key: 'pk_test_9fffdfb13c6194a5a236b933ddb00aa29a78dd78',
			  email: customerEmail,
			  amount: amount+'00',
			  
			  metadata: {
				 custom_fields: [
					{
						display_name: "Mobile Number",
						variable_name: "mobile_number",
						value: "+2348012345678"
					}
				 ]
			  },
			  callback: function(response){
				  
				  
				  $.post('$cashAprovedUrl'+'?amountid='+$('#plan_id').val()+'& ordertype=airtime',$('#dataform').serialize())
		.always(function(result){
	
			
   			if(result==1){
	   			$('#BtnLoader').hide();
	   			$(document).find('#feedback').html('Sent').show();
    		}else{
				$('#BtnLoader').hide();
				$(document).find('#feedback').html('Not enought funds in wallet').show();
	
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
		alert('yes');
			  },
			  onClose: function(){
				  //alert('window closed');
			  }
		});
		handler.openIframe();
  }
  
  
  function createOrder(planid){
		$.post('$cashAprovedUrl'+'?amountid='+planid+'& ordertype=airtime',\$form.serialize())
		.always(function(result){
	
			
   			if(result==1){
	   			$('#BtnLoader').hide();
	   			$(document).find('#feedback').html('Sent').show();
    		}else{
				$('#BtnLoader').hide();
				$(document).find('#feedback').html('Not enought funds in wallet').show();
	
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	}
	
	if(paymentMode == 'wallet'){
		$.post('$walletUrl'+'?amountid='+planid+'& ordertype=airtime',\$form.serialize())
		.always(function(result){
	
			
   			if(result==1){
	   			$('#BtnLoader').hide();
	   			$(document).find('#feedback').html('Sent').show();
    		}else{
				$('#BtnLoader').hide();
				$(document).find('#feedback').html('Not enought funds in wallet').show();
	
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	} else if (paymentMode == 'card'){
	
		$.post('$cardValidationUrl'+'?amountid='+planid,\$form.serialize())
		.always(function(result){
	
			
   			if(result.success==1){
				
				customerEmail = result.email;
				if(customerEmail.lenght == 0){
				 customerEmail = 'kingsonly13c@gmail.com';
				}
				
				payWithPaystack(customerEmail,result.amount,createOrder);
	   			
    		}else{
				$('#BtnLoader').hide();
				$(document).find('#feedback').html('Not enought funds in wallet').show();
	
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	} else if (paymentMode == 'voucher'){
			var pin = $('#voucher').val();
			$.post('$voucheUrl'+'?pin='+pin+'& amountid='+planid+'& ordertype=airtime',\$form.serialize())
		.always(function(result){
	
			$(document).find('#loader').hide();
   			if(result==1){
			$('#BtnLoader').hide();
	   
	   			$(document).find('#feedback').html('sent').show();
    		}else{
				$('#BtnLoader').hide();
				$(document).find('#feedback').html('Voucher used or Voucher is not correct').show();
	
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	} else{
		alert('no option selected');
	}
    
	
	setTimeout(function(){ 
	$(document).find('#feedback').hide();
	$(document).find('#BtnLoader').hide();
	$(document).find('#submitButtonText').show();
	
		$.pjax.reload({container:"#buy_airtime",async: false}); 
	}, 5000);
    return false;
 	
    
});


JS;
 
$this->registerJs($order);
?>



