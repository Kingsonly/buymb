<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\bootstrap\Modal;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dasgboard-index">
	<h1 style="padding:0px 15px;">
		Buy Data
	</h1>

	<section class="content">
      <!-- Small boxes (Stat box) -->
		
      <div class="row">
		  <div class="col-sm-12 col-lg-6">
			  <?= $this->render('orderform', [
				'model' => $model,
				'contactModel' => $contactModel,
			]) ?>
		  </div>
		  
		  <div class="col-sm-12 col-lg-6"> 
			  <div class="box" id = "project-title">
				  <div class="box-body" id = "project-title">
					  <?php Pjax::begin(['id' => 'buy_data']) ?>
			  	<table id="example1" class="table table-bordered table-striped">
							
                        	<thead>
                            <tr>
                              <th>SN</th>
                              <th>Plan</th>
                              <th>Payment Option</th>
                              <th>Status</th>
                              <th>Date</th>
                  
                            </tr>
                        
							</thead>
                        	<tbody>
                            <? $sn=1; foreach($myOrder as $k=>$v){ ?>
								
                                <tr>
								
                                  <td class="projecturl"><?=$sn;?></td>
                                  <td class="projecturl"><?=$v['plan']['plan_name'].' ('.$v['plan']['plan_amount'].')';?></td>
                                  <td class="projecturl"><?=$v['payment_mode'];?></td>
                                  <td class="projecturl <?= $v['status'] == 1?'danger':'success' ?>">
									  <?= $v['status'] == 1?'Pending':'Approved' ?>
									</td>
                                  <td class="projecturl"><?=$v['date_created'];?></td>

                                </tr>
								
                            <? $sn++; }?>
                   
                  
							</tbody>
                        	<tfoot>
								<tr>
								  <th>SN</th>
								  <th>Plan</th>
								  <th>Payment Option</th>
								  <th>Status</th>
								  <th>Date</th>

								</tr>
                			</tfoot>
                
                    
						</table>
					  <?php Pjax::end() ?>
			  </div>
			  </div>
		  </div>
        <!-- ./col -->
      </div>
	</section>
    
</div>

<? 
		Modal::begin([
			'header' =>'<h1 id="headers">Instructions</h1>',
			'id' => 'details',
			'size' => 'modal-md',  
		]);
?>
<div id="formcontent">
<div>You are expected to pay <em id="actualamount"></em></div>
<div>
<h4>Bank Details</h4>
<stong>Account Name </stong>Emeka Uchechukwu Michael<BR><HR/>
<stong>GTBANK</stong><em> 0215001020</em><BR><HR/>
<stong> ACCESS BANK</stong><em> 0042461814</em><BR><HR/>
<stong>FIRST BANK</stong><em> 3119596327</em><BR><HR/>
</div>
<div>
	<i>
		Please after payment send name of depositor, bank paid to , to 09037072904 for confirmation ( if you paid with ATM or bank transfer code kindly indicate).
	</i>
</div>
</div>

<?
	Modal::end();
?>



<?

			 
$order3 = <<<JS
 $("#example1").DataTable({
        "aaSorting": [],
		"pagingType": "simple",
		"responsive": "true",
		
    });


JS;
 $this->registerJs($order3);

		
?>



