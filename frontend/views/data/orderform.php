<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use frontend\models\planType;

use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\models\order */
/* @var $form ActiveForm */
?>

<style>
	#voucher{
		display: none;
		margin-bottom: 10px;
	}
	#numbers{
		display: none;
		
	}
	#BtnLoader{
		display:none;
	}
	
</style>
<div class="data-orderform">

    <?php $form = ActiveForm::begin(['action' =>['folder/create'],'id'=>'dataform']); ?>
	
		<?= $form->field($model, 'plan_id_selector')->dropDownList(ArrayHelper::map(planType::find()->where(['type'=>'data'])->all(), 'type_id', 'network'),[ 'prompt'=> 'Select...', 'class' => 'form-control', 'id' => 'plantype']) ?>
    
	
		<?= $form->field($model, 'plan_id')->widget(DepDrop::classname(), [
			'options'=>['id' => 'plan_id',"data-toggle" => "popover","data-placement" => "top", "data-trigger" => "change", "data-content" => "Change content to view amount to pay"],
			'pluginOptions' => [
				'depends' => ['plantype'],
				'placeholder' => 'Select...',
				
				'url'=>Url::to(['/data/list-items'])
			]
		]);
		?>
	
        <? $payment = ['card'=>'Card','voucher'=>'Voucher','wallet'=>'Wallet','transfer'=>'Bank Transfer'] ?>
		
		
		Buy for a friend <input type="checkbox" value="1" name="friend" id="buyfriend" />
	<div id="numbers">
			<?= $form->field($contactModel, 'phone_number')->textInput(['id'=>'phone_number'])->label('Enter Friend Phone Number') ?>
		</div>
	
		<?= $form->field($model, 'payment_mode')->dropDownList($payment,[ 'prompt'=> 'Select...', 'class' => 'form-control', 'id' => 'paymentmode']) ?>
	<div id="voucher">
		<label  class="control-label">Enter Voucher</label>
		<input type="text" class="form-control" id='pin'/>
	</div>
		
	
        
    
        <div class="form-group">
            <? //= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
			<?= Html::submitButton($model->isNewRecord ? '<span id="submitButtonText">Submit</span>'.Html::img("@web/images/45.gif", ["alt" => "loader","id"=>"BtnLoader", "class" => "user-image23" ]).' <span id="feedback"><span>' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-basic' : 'btn btn-basic','id'=>'submitbtn']) ?>

        </div>
    <?php ActiveForm::end(); ?>
	
	
</div><!-- data-orderform -->
<?
$voucheUrl = Url::to(['order/paymentwithvoucher']);
$walletUrl = Url::to(['order/paymentwithwallet']);
$cardValidationUrl = Url::to(['order/paymentwithcardvalidation']);
$cashAprovedUrl = Url::to(['order/paymentwithcarddata']);
$transferUrl = Url::to(['order/paymentbytransfer']);
$actualAmountUrl = Url::to(['site/actualamountdata']);

$this->registerJsFile(
    'https://js.paystack.co/v1/inline.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
$order = <<<JS


	
$('[data-toggle="popover"]').popover(); 

$('#paymentmode').change(function(){
	value = $(this).val();
	if(value == 'voucher'){
		$('#voucher').show();
	} else{
		$('#voucher').hide();
	}
});


$('#plan_id').change(function(){
	planid = $(this).val();
	currentValue = $(this);
	language = 0;
	$.post('$actualAmountUrl'+'?id='+planid)
		.always(function(actualAmount){
	
			
   			if(actualAmount >= 1){
				
				currentValue.attr('data-content','you are to pay '+actualAmount+' Naira for this plan');
				 currentValue.popover('show');
				}else{
			
				currentValue.attr('data-content','could not fetch amount.');
				 currentValue.popover('show');
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
   

});



$('#buyfriend').click(function(){
	value = $(this).checked;
	if($(this).is(":checked")){
		$('#numbers').show();
	} else{
		$('#numbers').hide();
	}
});

function payWithPaystackfake(test){
		alert(test);
  }
  
$('#dataform').on('beforeSubmit', function (e) {

	$('#submitButtonText').hide();
 	$('#BtnLoader').show();
    var \$form = $(this);
	var planid = $('#plan_id').val();
	var paymentMode = $('#paymentmode').val();
	
	function payWithPaystack(customerEmail,amount,createOrder){
		var handler = PaystackPop.setup({
			
			  key: 'pk_live_60e06110fdefa3e025e94acf315c0baf26f2ca63',
			  email: customerEmail,
			  amount: amount+'00',
			  
			  metadata: {
				 custom_fields: [
					{
						display_name: "Mobile Number",
						variable_name: "mobile_number",
						value: "+2348012345678"
					}
				 ]
			  },
			  callback: function(response){
				  
				  
				  $.post('$cashAprovedUrl'+'?amountid='+$('#plan_id').val()+'& ordertype=data',$('#dataform').serialize())
		.always(function(result){
	
			
   			if(result==1){
	   			$('#BtnLoader').hide();
	   			$(document).find('#feedback').html('Sent').show();
    		}else{
				$('#BtnLoader').hide();
				$(document).find('#feedback').html('Not enought funds in wallet').show();
	
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
		alert('yes');
			  },
			  onClose: function(){
				  //alert('window closed');
			  }
		});
		handler.openIframe();
  }
  
	
	function createOrder(planid){
		$.post('$cashAprovedUrl'+'?amountid='+planid+'& ordertype=data',\$form.serialize())
		.always(function(result){
	
			
   			if(result==1){
	   			$('#BtnLoader').hide();
	   			$(document).find('#feedback').html('Sent').show();
    		}else{
				$('#BtnLoader').hide();
				$(document).find('#feedback').html('Not enought funds in wallet').show();
	
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	}
	
	
	
	var mainActualAmount = 0;
	$.post('$actualAmountUrl'+'?id='+planid)
		.always(function(actualAmount){
	
			
   			if(actualAmount >= 1){
			mainActualAmount = actualAmount;
				}else{
			
				alert('Could not fetch actual amount');
				return false;
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
		
	   			
				if(paymentMode == 'wallet'){
		$.post('$walletUrl'+'?amountid='+planid+'& ordertype=data',\$form.serialize())
		.always(function(result){
	
			
   			if(result==1){
	   			$('#BtnLoader').hide();
	   			$(document).find('#feedback').html('Sent').show();
    		}else{
				$('#BtnLoader').hide();
				$(document).find('#feedback').html('Not enought funds in wallet').show();
	
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	} else if(paymentMode == 'transfer'){
	
		$.post('$transferUrl'+'?amountid='+planid+'& ordertype=data',\$form.serialize())
		.always(function(result){
	
			
   			if(result==1){
	   			$('#BtnLoader').hide();
	   			$(document).find('#feedback').html('Sent').show();
	   			
	   			$(document).find('#successTransfer').slideDown();
				
				$('#details').modal('show').find('#actualamount').html(mainActualAmount);
				
    		}else{
				$('#BtnLoader').hide();
				$(document).find('#feedback').html('An Error occured pls try again').show();
				$(document).find('#formcontent').slideUp();
	   			$(document).find('#successTransfer').slideDown();
	
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	}else if (paymentMode == 'card'){
		$.post('$cardValidationUrl'+'?amountid='+planid,\$form.serialize())
		.always(function(result){
	
			
   			if(result.success==1){
				
				customerEmail = result.email;
				if(customerEmail.lenght == 0){
				 customerEmail = 'kingsonly13c@gmail.com';
				}
				
				payWithPaystack(customerEmail,result.amount,createOrder);
	   			
    		}else{
				$('#BtnLoader').hide();
				$(document).find('#feedback').html('Not enought funds in wallet').show();
	
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	} else if (paymentMode == 'voucher'){
			var pin = $('#pin').val();
			$.post('$voucheUrl'+'?pin='+pin+'& amountid='+planid+'& ordertype=data',\$form.serialize())
		.always(function(result){
	
			$(document).find('#loader').hide();
   			if(result==1){
			$('#BtnLoader').hide();
	   
	   			$(document).find('#feedback').html('sent').show();
    		}else{
				$('#BtnLoader').hide();
				$(document).find('#feedback').html('Invalid or Pin used').show();
	
    		}
    	}).fail(function(){
    		console.log('Server Error');
    	});
	} else{
		alert('no option selected');
	}
    
	
	setTimeout(function(){ 
	$(document).find('#feedback').hide();
	$(document).find('#BtnLoader').hide();
	$(document).find('#submitButtonText').show();
	$.pjax.reload({container:"#buy_data",async: false}); 
	$("#example1").DataTable({
        "aaSorting": [],
		"pagingType": "simple",
		"responsive": "true",
		
    });
	}, 5000);
    return false;
				
				
    	
	
	
	
	
	
 	
    
});


JS;
 
$this->registerJs($order);
?>




