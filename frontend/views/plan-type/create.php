<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PlanType */

$this->title = 'Create Plan Type';
$this->params['breadcrumbs'][] = ['label' => 'Plan Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plan-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
