<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PlanType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="plan-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'type')->dropDownList([ 'data' => 'Data', 'airtime' => 'Airtime', 'bitcoin' => 'Bitcoin', '' => '', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'network')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
