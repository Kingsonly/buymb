<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\SiteAsset;
use common\widgets\Alert;
use frontend\models\ContactForm;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$model = new ContactForm();
SiteAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html class="no-js">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Buy MB</title>
    <meta name="description" content="Megatron - Multipurpose Responsive HTML5 Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="images/trans_logo.png">
    <link rel="apple-touch-icon" sizes="60x60" href="images/trans_logo.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/trans_logo.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/trans_logo.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/trans_logo.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/trans_logo.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/trans_logo.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/trans_logo.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/trans_logo.png">
    <link rel="icon" type="image/png" href="images/trans_logo.png" sizes="32x32">
    <link rel="icon" type="image/png" href="images/trans_logo.png" sizes="194x194">
    <link rel="icon" type="image/png" href="images/trans_logo.png" sizes="96x96">
    <link rel="icon" type="image/png" href="images/trans_logo.png" sizes="192x192">
    <link rel="icon" type="image/png" href="images/trans_logo.png" sizes="16x16">
    <link rel="manifest" href="favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="favicon/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">
	  <?php echo Html::csrfMetaTags(); ?>
	<?php $this->head() ?>
    <style>
		#tent_logo{
			width:30px;
		}  
	</style>
  </head>
  <body>
	   
    <!--[if lt IE 8]>
    <p class="browsenrupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <!--Page Content-->

<?php $this->beginBody() ?>

        <?= $content ?>
<!--Page Footer-->
      <footer id="footer" class="page-footer footer-preset-4">
        <div class="footer-body bgc-gray-darker">
          <div class="container">
            <div class="row">
              <div class="col-md-4">
                <div class="footer-widget-about"><a href="index.html" class="megatron inline size-2">
                    <div class="logo">
						<?=Html::img("@web/images/trans_logow.png", ["alt" => "loader","id"=>"logo", "class" => "" ]) ?>
                    </div>
                    <div class="brand">BUYMB</div></a>
                  <p>BUYMB Offers Wholesale/Company Price On Various Products and Services To All Its Members In Nigeria. Follow us on our social media platforms.</p>
                  <ul class="social circle">
                    <li><a href="#"><i class="icon icon-facebook-1"></i></a></li>
                    <li><a href="#"><i class="icon icon-twitter-1"></i></a></li>
                    <li><a href="#"><i class="icon icon-flickr"></i></a></li>
                    <li><a href="#"><i class="icon icon-linkedin-1"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="col-md-8">
                <div class="footer-widget-form">
                  
					
            <?php $form = ActiveForm::begin(['id' => 'contact_form']); ?>
 						<div class="__inputs clearfix">
						<span class="__name">
                <?= $form->field($model, 'name')->textInput() ?>
							</span>
						
						<span class="__phone">

                <?= $form->field($model, 'subject') ?>
							</span>
						<span class="__email">
							
                <?= $form->field($model, 'email') ?>
				</span>
					  </div>
                
					<div class="__message">
                <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>
						</div>
                <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                ]) ?>

                <div class="form-group">
                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="footer-foot-1 bgc-gray-darkest">
          <div class="container text-center">
            <p class="font-heading fz-6-s">BUYMB &copy; 2018 <span class="font-heading fz-6-s">DESIGNED BY <STRONG>TENTACULAR TECHNOLOGIES</STRONG></span> <?=Html::img("@web/images/tent.png", ["alt" => "loader","id"=>"tent_logo", "class" => "logo" ]) ?></p>
            
			  
          </div>
        </div>
      </footer>
      <!--End Page Footer-->
      <div id="shop-quickview" style="display:none;" class="shop-quick-view clearfix">
        <div class="row">
          <div class="col-sm-6 col-xs-12 product-thumbnail-slider-wrapper __content-left">
            <div class="product-thumbnail-slider">
              <div class="product-syn-slider-1-wrapper">
                <div class="product-syn-slider-1 syn-slider-1 direction-nav">
                  <div class="block-product-slider">
                    <div data-mfp-src="assets/images/shop/product-img-2-l.jpg" class="__image zoom-button img-wrapper"><img src="assets/images/shop/product-img-2-l.jpg" alt="Product slider image"/></div>
                  </div>
                  <div class="block-product-slider">
                    <div data-mfp-src="assets/images/shop/product-img-4-l.jpg" class="__image zoom-button img-wrapper"><img src="assets/images/shop/product-img-4-l.jpg" alt="Product slider image"/></div>
                  </div>
                  <div class="block-product-slider">
                    <div data-mfp-src="assets/images/shop/product-img-6-l.jpg" class="__image zoom-button img-wrapper"><img src="assets/images/shop/product-img-6-l.jpg" alt="Product slider image"/></div>
                  </div>
                  <div class="block-product-slider">
                    <div data-mfp-src="assets/images/shop/product-img-11-l.jpg" class="__image zoom-button img-wrapper"><img src="assets/images/shop/product-img-11-l.jpg" alt="Product slider image"/></div>
                  </div>
                  <div class="block-product-slider">
                    <div data-mfp-src="assets/images/shop/product-img-13-l.jpg" class="__image zoom-button img-wrapper"><img src="assets/images/shop/product-img-13-l.jpg" alt="Product slider image"/></div>
                  </div>
                  <div class="block-product-slider">
                    <div data-mfp-src="assets/images/shop/product-img-14-l.jpg" class="__image zoom-button img-wrapper"><img src="assets/images/shop/product-img-14-l.jpg" alt="Product slider image"/></div>
                  </div>
                </div>
              </div>
              <div class="product-syn-slider-2-wrapper">
                <div class="product-syn-slider-2 syn-slider-2">
                  <div class="block-product-slider">
                    <div class="__image text-center overlay-container"><img src="assets/images/shop/product-img-2-m.jpg" alt="Product slider image"/>
                      <div class="overlay bgc-light-o-5"></div>
                    </div>
                  </div>
                  <div class="block-product-slider">
                    <div class="__image text-center overlay-container"><img src="assets/images/shop/product-img-4-m.jpg" alt="Product slider image"/>
                      <div class="overlay bgc-light-o-5"></div>
                    </div>
                  </div>
                  <div class="block-product-slider">
                    <div class="__image text-center overlay-container"><img src="assets/images/shop/product-img-6-m.jpg" alt="Product slider image"/>
                      <div class="overlay bgc-light-o-5"></div>
                    </div>
                  </div>
                  <div class="block-product-slider">
                    <div class="__image text-center overlay-container"><img src="assets/images/shop/product-img-11-m.jpg" alt="Product slider image"/>
                      <div class="overlay bgc-light-o-5"></div>
                    </div>
                  </div>
                  <div class="block-product-slider">
                    <div class="__image text-center overlay-container"><img src="assets/images/shop/product-img-13-m.jpg" alt="Product slider image"/>
                      <div class="overlay bgc-light-o-5"></div>
                    </div>
                  </div>
                  <div class="block-product-slider">
                    <div class="__image text-center overlay-container"><img src="assets/images/shop/product-img-14-m.jpg" alt="Product slider image"/>
                      <div class="overlay bgc-light-o-5"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-xs-12 __content-right">
            <div class="product-detail">
              <h3 class="fz-3-l"><a href="shop-single-fullwidth.html">FITCH ZIP THROUGH HOODIE</a></h3>
              <div class="__rating clearfix">
                <div data-rating='4' class="star-ratings"><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span><span>&#9733;</span></div>
                <p class="color-secondary">(1 customer review)</p>
              </div>
              <div class="__price fz-4 font-heading">
                <del>$500</del><span class="color-primary">$300</span>
              </div>
              <div class="__text">
                <p>Nullam fringilla tristique elit id varius. Nulla lacinia quam nec venenatis dignissim. Vivamus volutpat tempus semper. Cras feugiat mi sit amet risus consectetur, non consectetur nisl finibus. Ut ac eros quis mi volutpat cursus vel non risus. In non neque lacinia, aliquet tortor sed, consectetur nibh. Nulla faucibus risus in ligula elementum bibendum.</p>
              </div>
              <form>
                <div class="__option-2 clearfix">
                  <div class="__quantity">
                    <div class="quantity-input">
                      <input type="text" value="1" class="number"/>
                      <button class="add icon-up-open-mini"></button>
                      <button class="subract icon-down-open-mini"></button>
                    </div>
                  </div>
                  <div class="__button"><a href="shop-cart.html" class="btn btn-primary"><i class="icon-shopping111"></i>ADD TO CART</a></div>
                </div>
              </form>
              <div class="__others">
                <p>SKU:&nbsp;<a href="#">0010</a></p>
                <p>CATEGORY:&nbsp;<a href="#">Other</a></p>
                <p>TAGS:&nbsp;<a href="#">Dress</a>,&nbsp;<a href="#">Fashion</a></p>
              </div>
              <ul class="social circle secondary responsive">
                <li><a href="#"><i class="icon icon-facebook-1"></i></a></li>
                <li><a href="#"><i class="icon icon-twitter-1"></i></a></li>
                <li><a href="#"><i class="icon icon-pinterest"></i></a></li>
                <li><a href="#"><i class="icon icon-stumbleupon"></i></a></li>
                <li><a href="#"><i class="icon icon-instagrem"></i></a></li>
                <li><a href="#"><i class="icon icon-dribbble-1"></i></a></li>
                <li><a href="#"><i class="icon icon-github"></i></a></li>
                <li><a href="#"><i class="icon icon-vimeo"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--End Page content-->
    <!--Javascript Library-->
    <button id="back-to-top-btn"><i class="icon-up-open-big"></i></button>
    
    <!-- Google analytics-->
    <script type="text/javascript">(function(b,o,i,l,e,r){b.GoogleAsnalyticsObject=l;b[l]||(b[l]=function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;e=o.createElement(i);r=o.getElementsByTagName(i)[0];e.src='//www.google-analytics.com/analytics.js';r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));ga('create','UA-57387972-3');ga('send','pageview');</script>
    <!--End Javascript Library-->
<?php $this->endBody() ?>
 
  </body>
</html>
<?php $this->endPage() ?>
