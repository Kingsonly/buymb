<?php

require_once 'vendor/autoload.php';
class admindashboard_Model extends Model {

	function __construct() {
		parent::__construct();
	}


	function dashboard($json){
	$return['consultants']= $this->getconsultants($json);
	$return['transactions']= $this->gettransactions($json);
	$return['clients']= $this->getclients($json);
	$return['properties']=$this->getproperties($json);
	return $return;
	}

	function getconsultants($json)
	{
		$consultant = $this->db->query("SELECT * FROM tbl_consultant where status = 1 ORDER BY id DESC")or die(mysql_error());
		if($json==true){
		$return = $this->returnjson($consultant->rows);
		print_r($return);
		exit;
		}else{
		return $consultant;
		}

	}
	
	function getpatchEmail()
	{
		$consultant = $this->db->query("SELECT email FROM tbl_consultant where id > 5187")or die(mysql_error());
		return $consultant->rows;

	}



	function getclients($json)
	{
		if(isset($_GET['user'])){
		$clients = $this->db->query("SELECT * FROM tbl_clients INNER JOIN tbl_transactions INNER JOIN tbl_properties ON tbl_clients.id=tbl_transactions.client_id AND tbl_clients.consultant_id=tbl_transactions.consultant_id AND tbl_clients.property_id=tbl_properties.property_id WHERE tbl_clients.consultant_id='".$_GET['user']."'")or die(mysql_error());
		}else{
		$clients = $this->db->query("SELECT * FROM tbl_clients INNER JOIN tbl_transactions INNER JOIN tbl_properties ON tbl_clients.id=tbl_transactions.client_id AND tbl_clients.consultant_id=tbl_transactions.consultant_id AND tbl_clients.property_id=tbl_properties.property_id")or die(mysql_error());
		}

		if($json==true){
		$return = $this->returnjson($clients->rows);
		print_r($return);
		exit;
		}else{
		return $clients;
		}

	}

	function getproperties($json)
	{
		/////////IF GET DETAILS ISSET////
		if(isset($_GET['getdetails'])){

		if(!isset($_GET['consultant'])){

		$return['properties'] = $this->db->query("SELECT * FROM tbl_properties ORDER BY tbl_properties.property_id DESC")or die(mysql_error());
		}

		elseif(isset($_GET['consultant'])){
		$consultant = $_GET['consultant'];
		$return['properties'] = $this->db->query("SELECT * FROM tbl_properties WHERE consultant_id='".$consultant."' ORDER BY tbl_properties.property_id DESC")or die(mysql_error());
		$return['properties'] = $this->returnjson($return['properties']->rows);
		print_r(json_encode($return));
		exit;
		}

		if($json==true){

		$return['properties'] = $this->returnjson($return['properties']->rows);

		print_r(json_encode($return));
		exit;
		}else{
		return $return;
		}
	}

	///////////IF GET DETAILS AND TABLE ARE NOR SET/////
	if((!isset($_GET['getdetails']))&&(!isset($_GET['table']))){
	$return = $this->db->query("SELECT * FROM tbl_properties ORDER BY property_id DESC")or die(mysql_error());

	return $return;
	}

	//////// IF THE TABLE ISSET////
	if(isset($_GET['table'])){
	$table = $_GET['table'];
	foreach(array_keys($_POST) as $key) {
		//if(in_array($key, $exclude) ) {
		$keys[] = $fields[] = "`$key`";
		$data[] = $values[] = "'" .$this->db->escape($_POST[$key])."'";
		//}
		}
	require_once('miscellenous_model.php');
	$misc = new miscModel;
	$uploaded=$_FILES['photo'];
	$uploaded_file=$_FILES['doc_file'];
	$array_list=array('jpg','png','jpeg');
	$array_list2=array('docs','pdf');
	$name=rand(10000,100000);
	if($uploaded['name']!=''){
	$move = $misc->file_upload($uploaded,'2000000',"views/images/uploads/",$array_list,$name);
	$move1 = $misc->file_upload($uploaded_file,'2000000',"views/images/uploads/",$array_list2,'file'.$name);
	$photoarray=array("'".$move[1]."','".$move1[1]."'");
	$fieldarray=array("`photo`,`file`");
	$data = $values = array_merge($values,$photoarray);
	$keys = $fields = array_merge($fields,$fieldarray);
	$fields = implode(",", $fields);
    $values = implode(",", $values);

	}else{
	$move[0]='1';
	$move[1]='';
	}
	///////////IF UPLOADED FILE IS SUCCESSFUL
	if($move[0]=='1'){

	///////////IF IT IS AN EDIT OPERATION/////
	if(isset($_GET['edit'])){
	// Check form fields
	$sets = array();
	$combine = array_combine($keys, $data);

	foreach($combine as $column => $value){
	 $sets[] = "" .$column. " = ".$value." ";
	}
	$whereSQL = "WHERE property_id='".$_GET['edit']."'";
	//print_r($sets);exit;
	$sql ="UPDATE $table " . " SET " . implode("," ,$sets) . $whereSQL;
	if($this->db->query($sql)) {
	 $msg = false;
    }else {
        $msg = mysql_error();
    }
	echo json_encode($msg);
	return;
	}
	//////////ELSE IF IT IS AN INSERT OPERATION//////
	else{
	if($this->db->query("INSERT INTO `$table` ($fields) VALUES ($values)")) {
	 $msg = false;
    }else {
        $msg = mysql_error();
    }
	}

	}
	////////ELSE IF UPLOADED FILE IS NOT SUCCESSFUL
	else{
		$msg = $move[0];
	}
		echo json_encode($msg);
	}


	}

	function gettransactions($json)
	{
		if(!isset($_GET['table'])){
		if(!isset($_GET['user'])){
		$transactions = $this->db->query("SELECT * FROM tbl_transactions INNER JOIN tbl_clients INNER JOIN tbl_properties INNER JOIN tbl_consultant ON tbl_transactions.client_id=tbl_clients.id AND tbl_transactions.property_id=tbl_properties.property_id AND tbl_transactions.consultant_id=tbl_consultant.new_id ORDER BY tbl_transactions.id DESC")or die(mysql_error());
		}

		elseif(isset($_GET['user'])){
		$column=$_GET['column']."_id";
		$transactions = $this->db->query("SELECT * FROM tbl_transactions INNER JOIN tbl_clients INNER JOIN tbl_properties INNER JOIN tbl_consultant ON tbl_transactions.client_id=tbl_clients.id AND tbl_transactions.property_id=tbl_properties.property_id AND tbl_transactions.consultant_id=tbl_consultant.new_id WHERE tbl_transactions".".".$column."='".$_GET['user']."'")or die(mysql_error());
		}

		if($json==true){
		$return = $this->returnjson($transactions->rows);
		print_r($return);
		exit;
		}else{
		return $transactions;
		}

	}

		///////////IF THIS IS A POST METHOD. THEN IT SHOULD INSERT INTO THE DB
		if(isset($_GET['table'])){

	foreach(array_keys($_POST) as $key ) {
	$fields[] = "`$key`";
	$values[] = "'" .mysql_real_escape_string($_POST[$key])."'";
    }

	$fields = implode(",", $fields);
    $values = implode(",", $values);

	if($this->db->query("INSERT INTO `tbl_transactions` ($fields) VALUES ($values)") ) {
        $error = false;

    } else {
        $error = mysql_error();

    }
		echo json_encode($error);

	}
	}


	function getcommissions($json)
	{
		if(isset($_GET['user'])){
		$commissions = $this->db->query("SELECT * FROM tbl_commissions INNER JOIN tbl_consultant INNER JOIN tbl_properties INNER JOIN tbl_clients ON tbl_commissions.consultant_id=tbl_consultant.id AND tbl_commissions.property_id=tbl_properties.property_id AND tbl_clients.id=tbl_commissions.client_id WHERE tbl_commissions.consultant_id='".$_GET['user']."' ORDER BY tbl_commissions.id DESC")or die(mysql_error());

		}else{

		$commissions = $this->db->query("SELECT * FROM tbl_commissions INNER JOIN tbl_consultant INNER JOIN tbl_properties INNER JOIN tbl_clients ON tbl_commissions.consultant_id=tbl_consultant.id AND tbl_commissions.property_id=tbl_properties.property_id AND tbl_clients.id=tbl_commissions.client_id ORDER BY tbl_commissions.id DESC")or die(mysql_error());
		}
		if($json==true){
		$return=$this->returnjson($commissions->rows);
		print_r($return);
		exit;
		}else{
		return json_encode($commissions->rows);
		}

	}


	function viewproperty($json)
	{
		$id=$this->db->escape($_GET['get']);
		$return['consultant'] = $this->db->query("SELECT * FROM tbl_consultant ORDER BY tbl_consultant.id DESC")or die(mysql_error());
		$return['property'] = $this->db->query("SELECT * FROM tbl_properties WHERE property_id='".$id."'")or die(mysql_error());

		if($json==true){
		$return['property']=$this->returnjson($return['property']->rows[0]);
		$return['consultant'] = $this->returnjson($return['consultant']->rows);
		print_r(json_encode($return));
		exit;
		}else{
		return $property->rows;
		}

	}

	function registerconsultant($json){

	if(isset($_GET['table'])){
	$table = $_GET['table'];
	/////////////IF THE EDIT ID IS ACTIVE, MEANING IT SHOULD EDIT THE DATABASE////
	if(isset($_GET['edit'])){
	$editID = $this->db->escape($_GET['edit']);
	foreach(array_keys($_POST) as $key ) {
	$fields[] = "`$key`";
	$values[] = "'" .$this->db->escape($_POST[$key])."'";
    }

	// Check form fields
	$sets = array();
	$combine = array_combine($fields, $values);
	foreach($combine as $column => $value){
	 $sets[] = "" .$column. " = ".$value." ";
	}
	$whereSQL = "WHERE id='".$editID."'";
	//print_r($sets);exit;
	$sql ="UPDATE $table " . " SET " . implode("," ,$sets) . $whereSQL;
	if($this->db->query($sql)) {
	 $msg = false;
    }else {
        $msg = mysql_error();
    }
	echo json_encode($msg);
	return;

	}
	/////////////ELSE IT SHOULD POST AS A FRESH INSERT INTO THE DATABASE
	else{
	//////CHECK IF THE EMAIL ADDRESS ALREADY EXIST//////
	$rands = rand(100000,10000000);
	$check_table_for_email = $this->db->query("SELECT * FROM `$table` WHERE email='".$this->db->escape($_POST['email'])."'")or die(mysql_error());

	//$check_all_table = $this->db->query("SELECT * FROM `$table` ")or die(mysql_error());

	//$check_all_table = $this->db->query("SELECT AUTO_INCREMENT FROM information_schema.tables WHERE table_name = 'tbl_consultant' AND table_schema = DATABASE( ) ;");

	//////CHECK IF THE REFERRER LINK EXIST//////
	$check_table_for_ref_code = $this->db->query("SELECT * FROM `$table` WHERE ref_code='".$this->db->escape($_POST['ref_code'])."'")or die(mysql_error());
	/////////IF THE EMAIL ADDRESS EXIST/////
	if($check_table_for_email->num_rows>0){
	echo $error = "Sorry email address already exist! Please try again.";
	exit;
	}
	elseif($check_table_for_ref_code->num_rows==0){
	echo $error = "Invalid Referrer Link! Please enter a valid referer link.";
	exit;

	}
	///// IF THE EMAIL ADDRESS DOES NOT EXIST//////
	else{
	$exclude = array('ref_code','password','retype_password');
	$ref_code=array('ref_code'=>$rands);
	$getgen=$check_table_for_ref_code->row;
	//$numrows=$check_all_table->row['AUTO_INCREMENT'];
	$gen=$getgen['new_id'].','.$getgen['new_gen'];

	foreach(array_keys($_POST) as $key ) {
	if(!in_array($key, $exclude) ) {
	$fields[] = "`$key`";
	$values[] = "'" .$this->db->escape($_POST[$key])."'";
    }
	}

	$valuearray=array("'".$rands."','".$gen."','".md5($_POST['password'])."'");
	$fieldarray=array("`ref_code`,`gen`,`password`");
	$values = array_merge($values,$valuearray);
	$fields = array_merge($fields,$fieldarray);

	$fields = implode(",", $fields);
    $values = implode(",", $values);
	if($this->db->query("INSERT INTO `tbl_consultant` ($fields) VALUES ($values)") ) {

        require('controllers/Mailin.php');
        $mailin = new Mailin("https://api.sendinblue.com/v2.0","BqhUN9GLpJ13X56f");
        $email = $this->db->escape($_POST['email']);
        $email_name = $this->db->escape($_POST['f_name']).' '.$this->db->escape($_POST['l_name']);
        //Start pdf creation

        $data = array( "to" => array($email=>$email_name),
            "from" => array("noreply@adloyalty.com","Adloyalty Business Network"),
            "subject" => "Successful Registration",
            "html" => "<h4>Hello ".$email_name."</h4>,
            <br>
            <h2>Congratulations!</h2>
            <br/>
            Your Adloyalty Business Network registration is successful.
            <br/>
            <h4> Your referal link is <a href='http://portal.adloyaltybn.com/".$rands."'>portal.adloyaltybn.com/".$rands."</a></h4>
            <br>


            <br/>
            FOR EASE OF YOUR TRANSACTIONS, KINDLY DOWNLOAD ADLOYALTY BN MOBILE APP<a href=''>HERE NOW! <a/>
            <br/>
            For further enquiries, please contact :
            <br/>
            Adloyalty Business Network.
            <br/>
            Hotline : 09085822222
            <br/>
            Whats App: 09085922222
            <br/>
            Email: info@adloyaltybn.com
            <br/>
            Website: www.adloyaltybn.com
            <br/>
            Adloyalty Business Network. ..creating wealth, empowering people!
            ",
            "headers" => array("Content-Type"=> "text/html; charset=iso-8859-1","X-param1"=> "value1", "X-param2"=> "value2","X-Mailin-custom"=>"my custom value", "X-Mailin-IP"=> "102.102.1.2", "X-Mailin-Tag" => "My tag"),
            "inline_image" => array("myinlineimage1.png" => "your_png_files_base64_encoded_chunk_data","myinlineimage2.jpg" => "your_jpg_files_base64_encoded_chunk_data")
        );
        $mailin->send_email($data);


        $error = false;

    } else {
        $error = mysql_error();

    }
		echo json_encode($error);

	}
	}

	}

	if(isset($_GET['getdetails'])){
	$id=$this->db->escape($_GET['getdetails']);
	$return['consultant'] = $this->db->query("SELECT * FROM tbl_consultant WHERE id='".$id."'")or die(mysql_error());

	if($json==true){
	$return['consultant'] = $this->returnjson($return['consultant']->rows[0]);
	print_r(json_encode($return));
	exit;
	}else{
	return $return['consultant']->rows[0];
	}



	}

	}

	function registerclient($json){
	if(isset($_GET['getdetails'])){
		$return['consultant'] = $this->db->query("SELECT * FROM tbl_consultant ORDER BY tbl_consultant.id DESC")or die(mysql_error());
		$return['properties'] = $this->db->query("SELECT * FROM tbl_properties ORDER BY tbl_properties.property_id DESC")or die(mysql_error());

		if($json==true){
		$return['consultant'] = $this->returnjson($return['consultant']->rows);
		$return['properties'] = $this->returnjson($return['properties']->rows);

		print_r(json_encode($return));
		exit;
		}else{
		return json_encode($return->rows);
		}
	}

	if($_GET['table']){
	$table = $_GET['table'];
	//////CHECK IF THE EMAIL ADDRESS ALREADY EXIST//////
	$check_table_for_email = $this->db->query("SELECT * FROM `$table` WHERE email='".$this->db->escape($_POST['email'])."'")or die(mysql_error());

	//////GET THE COMMISSION SETTINGS FROM DATABASE//////
	$get_commission_rate = $this->db->query("SELECT * FROM tbl_properties WHERE property_id='".$this->db->escape($_POST['property_id'])."'")or die(mysql_error());
	$commission = $get_commission_rate->rows[0]['commission_percent'];

	/////////IF THE EMAIL ADDRESS EXIST/////
	if($check_table_for_email->num_rows>0){
	echo $error = "Sorry email address already exist! Please try again.";
	exit;
	}
	///// IF THE EMAIL ADDRESS DOES NOT EXIST//////
	else{
	$exclude = array('payment_method','amount_paid');
	foreach(array_keys($_POST) as $key) {
		if(!in_array($key, $exclude) ) {
		$fields[] = "`$key`";
		$values[] = "'" .$this->db->escape($_POST[$key])."'";
		}
		}
	$fields = implode(",", $fields);
    $values = implode(",", $values);
	if($this->db->query("INSERT INTO `$table` ($fields) VALUES ($values)")) {
	$consultant_id = $this->db->escape($_POST['consultant_id']);
	$property_id = $this->db->escape($_POST['property_id']);
	$payment_method = $this->db->escape($_POST['payment_method']);
	$amount_paid = $this->db->escape($_POST['amount_paid']);
	$status = $_POST['status'];
	$payment_rand = rand(1000000000,10000000000);
	$client_id = $this->db->getLastId();
	$payment_date = $_POST['date'];
	$insert_into_transactions = $this->db->query("INSERT INTO `tbl_transactions` (client_id, consultant_id, property_id, payment_method, amount_paid, payment_date, status, payment_rand) VALUES ('$client_id', '$consultant_id', '$property_id', '$payment_method', '$amount_paid', '$payment_date', '$status', '$payment_rand')")or die(mysql_error());

	$commission_paid = ($amount_paid * $commission)/100;

	$insert_into_commissions = $this->db->query("INSERT INTO `tbl_commissions` (client_id, consultant_id, property_id, commission_paid, total_amount, percentage, payment_date, status) VALUES ('$client_id', '$consultant_id', '$property_id', '$commission_paid','$amount_paid','$commission', '$payment_date', '$status')")or die(mysql_error());

	$msg = false;

    } else {
        $msg = mysql_error();

    }
		echo json_encode($msg);

	}


	}


	}


	function bulksms(){

/* call the URL 



if ($f = @fopen($url, "r"))
{
      $answer = fgets($f, 255);
      if (substr($answer, 0, 1) == "+")
      {
            echo "SMS to $dnr was successful.";
}
    elseif($answer){
        echo 'message sent';
    }
else {
echo "an error has occurred: [$answer].";
} }

else{
echo $_POST['message'];
}
		
		
		
		
		
		
		
	///////////SMS AND EMAIL API STARTS//////
	require('api/smsandemail.php');
	$smsandemail=new smsandemail;
	$sms_message=$this->db->escape($_POST['message']);
	$phonenumber=explode(",", $this->db->escape($_POST['phones']));
	$sender_id =$this->db->escape($_POST['sender_id']);
	$uploaded = $_FILES['recipient_upload'];
	$explode = explode('.',$uploaded['name']);
	$extension = end($explode);
	/////IF GENERATION IS SELECTED//////
	if($_POST['generation']!=0){
	$generation = $_POST['generation'];
	$selectgeneration = $this->db->query("SELECT * FROM tbl_consultant WHERE  gen LIKE '%".$generation."%'")or die(mysql_error());

	foreach($selectgeneration->rows as $rows ) {
	$generation_phones[] = $rows['phones'];
	}
	$phonenumber=array_merge($phonenumber,$generation_phones);
	}
	/////IF USERS IS SELECTED//////
	elseif($_POST['users']!=0){
	$table=	$_POST['users'];
	$selectusers = $this->db->query("SELECT * FROM ".$table." WHERE status='1'")or die(mysql_error());
	if($table=='tbl_consultant'){
	foreach($selectusers->rows as $rows ) {
	$user_phones[] = $rows['phones'];
	}
	}else{
	foreach($selectusers->rows as $rows ) {
	$user_phones[] = $rows['phone'];
	}
	}
	$phonenumber=array_merge($phonenumber,$user_phones);
	}

	//$ggeneration_phones=array();
	//$numbers='';

	/////IF CONTACT FILE IS UPLOADED//////
	if($uploaded['name']!=''){
	if(($extension!='txt')&&($extension!='')){
	echo "Invalid File Format! Please select a text file and try again";
	exit;
	}else{
	$file = fopen($uploaded['tmp_name'], "r");
	while (($data = fgetcsv($file, 8000, ",")) !== FALSE) {
	$numbers=$data;
	}
	$phonenumber=array_merge($phonenumber,$numbers);
	}
	}
  foreach($phonenumber as $phone ) {
	///////SEND SMS USING SMS API//////
	//echo $phone;
	$sms=$smsandemail->smsmessage($phone,$sms_message,$sender_id);
	}
    echo $sms;
	*/
				$owneremail="kingsonly13c@gmail.com";
				$subacct="KINGSONLY";
				$subacctpwd="firstoctober";
				$sendto=explode(",", $this->db->escape($_POST['phones']));
				$sender=$_POST['sender_id']; 
				$message=$_POST['message'];
				$phonenumber='';
				if($_POST['users']== 'tbl_consultant'){
					$userss = $_POST['users'];
					$selectgeneration = $this->db->query("SELECT * FROM ".$userss." WHERE status='1'  ")or die(mysql_error());
					$generation_phones = [];
					foreach($selectgeneration->rows as $rows ) {
					array_push($generation_phones,$rows['phones']);
					}
					$phonenumbers=array_merge($sendto,$generation_phones);
					$ARRAYVALUES =['/\s+/'];
					$phonenumber=preg_replace($ARRAYVALUES, '', implode(',',$phonenumbers));
					$phonenumber=str_replace([':', '\\', '/', '*',')','(','-'], '', $phonenumber);
					} else{
					$phonenumber = implode(',',$sendto);
				}
				/* create the required URL */
				/* destination number */
				/* sender id */
				/* message to be sent */
				$url =
				"http://www.smslive247.com/http/index.aspx?" . "cmd=sendquickmsg"
				. "&owneremail=" . UrlEncode($owneremail)
				. "&subacct=" . UrlEncode($subacct)
				. "&subacctpwd=" . UrlEncode($subacctpwd) . "&message=" . UrlEncode($message)."&sendto=".UrlEncode($phonenumber)."&sender=".UrlEncode($sender);
				if ($f = @fopen($url, "r"))
		{
			  $answer = fgets($f, 255);
			  if (substr($answer, 0, 1) == "+")
			  {
					echo "SMS to $dnr was successful.";
		}
			elseif($answer){
				//echo 'message sent';
				var_dump($answer);
			}
		else {
		echo "an error has occurred: [$answer].";
		} }

		else{
		var_dump($phonenumber);
		}
	}
	
	
	function sendemail(){
		$emails=[$_POST['recipient_email'] => $_POST['recipient_email']];//array($this->db->escape($_POST['recipient_email']));
		$message = $_POST['message'];
		$user_email = [];
		$subject = $this->db->escape($_POST['subject']);
		if($_POST['users']!='0'){
		$table=	$_POST['users'];
		$selectusers = $this->db->query("SELECT * FROM ".$table." WHERE status='1'")or die(mysql_error());
		foreach($selectusers->rows as $rows ) {
			$emailers = filter_var($rows['email'], FILTER_SANITIZE_EMAIL);
			if (filter_var($emailers, FILTER_VALIDATE_EMAIL)) {
    			$user_email[$emailers] =  $rows['email'];//preg_replace('/\s+/', '', $emailers);
				
			} 
		
		}
		
		}
		
		if(!empty($_POST['recipient_email']) and !empty($user_email)){
			$email=array_merge($emails,$user_email);
			
			//return 'false';
		}elseif(!empty($user_email)){
			$email=$user_email;
			
		}elseif(!empty($_POST['recipient_email'])){
			$email=$emails;
			
		} else{
			echo 123;
			return 'false';
		}
		
		//require('controllers/Mailin.php');
		
		// Create the Transport
		//$smtp_host_ip = gethostbyname('smtp.gmail.com');
		$smtp_host_ip = gethostbyname('mail.adloyaltybn.com');
		
			$transport = (new Swift_SmtpTransport('mail.adloyaltybn.com',25))
			->setUsername('info@adloyaltybn.com')
			->setPassword('P@55w)rd')
			;

			// Create the Mailer using your created Transport
			$mailer = new Swift_Mailer($transport);

			// Create a message
			$message = (new Swift_Message($subject))
			->setFrom(['infor@adloyaltybn.com' => 'Adloyalty Business Network'])->setBody($message,'text/html');
			if(  $_FILES['fileatt']['size'] == 0 && $_FILES['fileatt']['error'] == 0){
				$message->attach(Swift_Attachment::fromPath($_FILES['fileatt']['tmp_name'])->setFilename($_FILES['fileatt']['name']));
			}
			
		foreach($email as $address => $name){
			 try{
				 $message->setTo([$address => $name]);
    			}catch(\Swift_TransportException $e){
        $response = $e->getMessage() ;
    }
			$result = $mailer->send($message);
		}
			
		
				$error = false;
			echo json_encode($error);

	}

	function changeStatusPatch()
	{
        
        $sth = $this->db->query("UPDATE  tbl_consultant SET status = 0 , email = CONCAT(email,1) WHERE id>5182");
		
            
	}
	function generation($json)
	{
		////////FETCH FATHERS OF THIS CONSULTANT
		$id = $this->db->escape($_GET['getdetails']);
		$consultant = $this->db->query("SELECT * FROM tbl_consultant WHERE new_id='".$id."'")or die(mysql_error());
		$explode = explode(",", $consultant->rows[0]['gen']);
		$return[]='';
		array_pop($explode);
		foreach($explode as $data){
		$fathergeneration = $this->db->query("SELECT * FROM tbl_consultant WHERE new_id='".$data."'")or die(mysql_error());
			if(empty($fathergeneration->row)){
				$fathergeneration = $this->db->query("SELECT * FROM tbl_consultant_clone WHERE id='".$data."'")or die(mysql_error());
				
				//$fgenerations[] = $test->row;
			}
				$fgenerations[] = $fathergeneration->row;
			
		
		}
		///////////////FETCH SONS OF THIS CONSULTANT
		$songeneration = $this->db->query("SELECT * FROM tbl_consultant WHERE gen LIKE '%".$id."%'")or die(mysql_error());

		//print_r($return['son']);exit;

		if($json==true){
		$return['fathers']=$this->returnjson($fgenerations);
		$return['sons']=$this->returnjson($songeneration->rows);
		$return['consultant']=$this->returnjson($consultant->rows[0]);
		print_r(json_encode($return));
		exit;
		}else{
		return json_encode($return);
		}

	}
	
	public function deleteProperty($propID){
		
		$sth = $this->db->query("UPDATE tbl_properties SET status = 0 WHERE property_id = '$propID'");
		return ;
	}
public function addDeleteProperty($propID){
		
		$sth = $this->db->query("UPDATE tbl_properties SET status = 1 WHERE property_id = '$propID'");
		return ;
	}



}
