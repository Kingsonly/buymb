<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class SiteAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'assets2/fonts/megatron/styles.css',
        'assets2/css/main.css',
        'css/site.css',
        'vendors/animate.css/animate.min.css',
		 'https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css',
        'https://cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css',
        
    ];
    public $js = [
		//'vendors/jquery/dist/jquery.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js',
        'vendors/bootstrap/dist/js/bootstrap.min.js',
        'vendors/waypoints/lib/jquery.waypoints.min.js',
        'vendors/waypoints/lib/shortcuts/sticky.min.js',
        'vendors/smoothscroll/SmoothScroll.js',
        'vendors/wow/dist/wow.min.js',
        'vendors/parallax.js/parallax.js',
        'vendors/jquery-ui/jquery-ui.min.js',
        'vendors/slick-carousel/slick/slick.js',
        'vendors/Chart.js/Chart.min.js',
        'vendors/magnific-popup/dist/jquery.magnific-popup.min.js',
      	'dist/js/sketch.js',
        'assets2/js/main.js',
		'https://js.paystack.co/v1/inline.js',
		'js/datatables/jquery.dataTables.min.js',
        'js/datatables/dataTables.bootstrap.min.js',
		'https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
