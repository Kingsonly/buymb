<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

// Models 
use frontend\models\Order;
use frontend\models\SellAirtime;
use frontend\models\Contact;


/**
 * Site controller
 */
class AirtimeController extends Controller
{
    
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
		if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }
		
		$model = new Order();
		$SellAirtime = new SellAirtime();
		$myOrder = $model->find()->where(['user_id'=> Yii::$app->user->identity->id,'order_type'=>'airtime'])->all();
		$contactModel = new Contact();
		$contactModel->scenario = $contactModel::SCENARIO_DATA;
		$mySoldAirtime = $SellAirtime->find()->where(['user_id'=> Yii::$app->user->identity->id])->all();
        return $this->render('index',[
			'myOrder' => $myOrder,
			'model' => $model,
			'mySoldAirtime' => $mySoldAirtime,
			'SellAirtime' => $SellAirtime,
			'contactModel' => $contactModel,
		]);
    }
	
	

  
}

