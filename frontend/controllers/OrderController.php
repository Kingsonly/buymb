<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Order;
use frontend\models\User;
use frontend\models\Plan;
use frontend\models\Voucher;
use frontend\models\Contact;
use frontend\models\ContactForm;
use frontend\models\SellAirtime;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\CommonAdminUsers;
use yii\db\Expression;



/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Order::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Order();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->order_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->order_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
	/**
	 * Get randum admin users to handle request
	 */
	public function adminUser($params)
	{
		$user = CommonAdminUsers::find()
			->orderBy(new Expression('rand()'))
			->limit(1)->one();
		if($params == 'username'){
			return $user->username;
		}
		return $user->id;
	}
	
	
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
	
	/**
	 * if payment optioin is wallet then this method would be executed 
	 */
	public function actionPaymentwithwallet($amountid,$ordertype)
	{
		$model = new Order();
		$userId = Yii::$app->user->identity->id;
		$getUser = User::findOne($userId);
		// check what type of  order request it is eg data or airtime
		if($ordertype == "data"){
			$getPlanAmount = Plan::findOne($amountid);
			$amount = (int)$getPlanAmount->plan_amount;
		} else{
			$getPlanAmount = Plan::findOne($amountid);
			$amount = (int)$amountid;
		}
		
		// validate with discounted amount
		$wallet = (int)$getUser->wallet;
		$orderType = $ordertype;
		
		//Validate if wallet has enough funds 
		if($wallet > $amount){
			$newWalet = $wallet - $amount;
			$getUser->wallet = $newWalet;
			if($getUser->save(false)){
				$model->status = 1;
				$this->createorder($model,$orderType);
				\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
				return 1;
			} else{
				\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
				return 0;
			}
			
			
		} else{
			\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			return 0;
		}
		
	}
	
	/**
	 * if payment optioin is Atm card then this method would be executed to validate the amont 
	 */
	
	public function actionPaymentwithcardvalidation($amountid, $ordertype = 'data')
	{
		
		$model = new Order();//get order model
		$contact = new Contact(); // get contact model
		$userId = !empty( Yii::$app->user->identity->id)? Yii::$app->user->identity->id:User::find()->where(['username'=>'guest'])->one()->id; // get user id
		
		$initContact = $contact->find()->where(['user_id'=>$userId])->andWhere(['status'=>1])->one();
		
		$userEmail = !empty($initContact->email)?$initContact->email:Yii::$app->params['adminEmail'];// get user email 
		// check order type eg data or airtime etc 
		
		if($ordertype == 'data'){
			/**
			 * if it data and the plan amount is greater than 3000 add an extra 100 naira, this is due to 
			 *the paystack charges.
			 */
			$getPlanAmount = Plan::findOne($amountid);
			$amount = $getPlanAmount->plan_amount >= 3000? ($getPlanAmount->plan_amount*1.5/100)+$getPlanAmount->plan_amount+100:($getPlanAmount->plan_amount*1.5/100)+$getPlanAmount->plan_amount;
			
			// data does not come with a discount this sinply means it is sold at the amount displayed 
			$data = ['success'=>1,'amount'=>ceil($amount),'email'=>$userEmail];
		} else{
			// amount should be discount of amount 
			$percentage = Yii::$app->params['rate'];//discount rte 
			$ajaxAmount = (int)$amountid; //amount id  is not the id but the actuall amount in this regards
			
			$getPer = ($percentage / 100) * $ajaxAmount; // get the discount amount
			$newAmount = $ajaxAmount - $getPer; // subtract amount with discount
			$amount = $newAmount >= 3000? ($newAmount*1.5/100)+$newAmount+100:($newAmount*1.5/100)+$newAmount;
			$data = ['success'=>1,'amount'=>ceil($amount),'email'=>$userEmail];
		}
		
		
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $data;
			
		
	}	
	
	public function actionBulkPaymentwithcardvalidation($amountid = 0 , $ordertype = 'data')
	{
		
		$model = new Order();//get order model
		$contact = new Contact(); // get contact model
		$userId = !empty( Yii::$app->user->identity->id)? Yii::$app->user->identity->id:User::find()->where(['username'=>'guest'])->one()->id; // get user id
		
		$initContact = $contact->find()->where(['user_id'=>$userId])->andWhere(['status'=>1])->one();
		
		$userEmail = !empty($initContact->email)?$initContact->email:Yii::$app->params['adminEmail'];// get user email 
		// check order type eg data or airtime etc 
		
		if($ordertype == 'data'){
			/**
			 * if it data and the plan amount is greater than 3000 add an extra 100 naira, this is due to 
			 *the paystack charges.
			 */
			$getPlanAmount = \Yii::$app->cart->getCost();
			$amount = $getPlanAmount >= 3000? ($getPlanAmount*1.5/100)+$getPlanAmount + 100:($getPlanAmount * 1.5/100) + $getPlanAmount;
			
			// data does not come with a discount this sinply means it is sold at the amount displayed 
			$data = ['success'=>1,'amount'=>ceil($amount),'email'=>$userEmail];
		} else{
			// amount should be discount of amount 
			$percentage = Yii::$app->params['rate'];//discount rte 
			$ajaxAmount = (int)$amountid; //amount id  is not the id but the actuall amount in this regards
			
			$getPer = ($percentage / 100) * $ajaxAmount; // get the discount amount
			$newAmount = $ajaxAmount - $getPer; // subtract amount with discount
			$amount = $newAmount >= 3000? ($newAmount*1.5/100)+$newAmount+100:($newAmount*1.5/100)+$newAmount;
			$data = ['success'=>1,'amount'=>ceil($amount),'email'=>$userEmail];
		}
		
		
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $data;
			
		
	}
	
	
	/**
	 * if payment optioin is voucher then this method would be executed 
	 */
	public function actionPaymentwithvoucher($pin,$amountid,$ordertype)
	{
		$model = new Order(); // init order model
		
		$userId = !empty( Yii::$app->user->identity->id)? Yii::$app->user->identity->id:User::find()->where(['username'=>'guest'])->one()->id; // get user id
		
		$getPin = Voucher::find()->where(['pin'=>$pin])->one(); // check pin validity
		if($ordertype == 'data'){
			// if order type is data fetch amount from plan table.
			$getPlanAmount = Plan::findOne($amountid);
			$amount = $getPlanAmount->plan_amount;
		}else{
			$amount = $amountid;
		}
		
		$pinAmount = $getPin->amount;
		$orderType = $ordertype;
		//Validate if pin is valide
		if(!empty($getPin->pin) and $getPin->status != 1){
			/**
			 * due to the fact that amout some times can be smaller than the pin, we subtract from the pin amount and lv the remender if any, so the pin can still be reused
			 */
			if($pinAmount > $amount){
				$newPinAmount = $pinAmount - $amount;
				$getPin->amount = $newPinAmount;
				$getPin->save(false);
				$model->status = 1;
				$this->createorder($model,$orderType);
				\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
				return 1;
			
			} elseif($pinAmount == $amount){
				$getPin->status = 1;
				$getPin->amount = 0;
				$this->createorder($model,$orderType);
				$getPin->save(false);
				\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
				return 1;
				
			} else{
				\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
				return 0;
			}
		} else{
				\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
				return 0;
			}
		
		
	}
	
	public function actionBulkPaymentwithvoucher($pin,$amountid,$ordertype)
	{
		$model = new Order(); // init order model
		
		$userId = !empty( Yii::$app->user->identity->id)? Yii::$app->user->identity->id:User::find()->where(['username'=>'guest'])->one()->id; // get user id
		
		$getPin = Voucher::find()->where(['pin'=>$pin])->one(); // check pin validity
		if($ordertype == 'data'){
			// if order type is data fetch amount from plan table.
			$amount = \Yii::$app->cart->getCost();
		}else{
			$amount = $amountid;
		}
		
		$pinAmount = $getPin->amount;
		$orderType = $ordertype;
		//Validate if pin is valide
		if(!empty($getPin->pin) and $getPin->status != 1){
			/**
			 * due to the fact that amout some times can be smaller than the pin, we subtract from the pin amount and lv the remender if any, so the pin can still be reused
			 */
			$status = 1;
			if($pinAmount > $amount){
				$newPinAmount = $pinAmount - $amount;
				$getPin->amount = $newPinAmount;
				$getPin->save(false);
				$paymentMode = 'voucher';
				$this->createBulkorder($orderType,$paymentMode,$status);
				\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
				return 1;
			
			} elseif($pinAmount == $amount){
				$getPin->status = 1;
				$getPin->amount = 0;
				$paymentMode = 'voucher';
				$this->createBulkorder($orderType,$paymentMode,$status);
				$getPin->save(false);
				\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
				return 1;
				
			} else{
				\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
				return 0;
			}
		} else{
				\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
				return 0;
			}
		
		
	}
	
	/**
	 * if payment optioin is Atm card 
	 * then this method would be executed 
	 */
	public function actionPaymentwithcarddata($amountid,$ordertype)
	{
		/**
		 * At this point the payment has been done and 
		 * the order details is being inserted into the 
		 * database 
		 */
		$model = new Order();
		$userId = !empty( Yii::$app->user->identity->id)? Yii::$app->user->identity->id:User::find()->where(['username'=>'guest'])->one()->id;
		$orderType = $ordertype;
		//Validate if pin is valide
		$model->status = 1;
		$this->createorder($model,$orderType);
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return 1;	
	}
	
	/**
	 * if payment optioin is Atm card then this method would be executed exclusive to bulk order
	 */
	public function actionBulkpaymentwithcarddata($ordertype)
	{
		$model = new Order();
		$userId = !empty( Yii::$app->user->identity->id)? Yii::$app->user->identity->id:User::find()->where(['username'=>'guest'])->one()->id;;
		$orderType = $ordertype;
		//Validate if pin is valide
		//$model->status = 1;
		$status = 1;
		$paymentMode = 'card';
		$this->createBulkOrder($orderType,$paymentMode,$status);
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return 1;	
	}
	
	public function actionPaymentwithcarddataairtime($amountid,$ordertype)
	{
		$model = new Order();
		$userId = !empty( Yii::$app->user->identity->id)? Yii::$app->user->identity->id:User::find()->where(['username'=>'guest'])->one()->id;;
		$orderType = $ordertype;
		//Validate if pin is valide
		$model->status = 1;
		$this->createorder($model,$orderType);
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return 1;	
	}
	
	public function actionPaymentbytransfer($amountid,$ordertype)
	{
		$model = new Order();
		$userId = !empty( Yii::$app->user->identity->id)? Yii::$app->user->identity->id:User::find()->where(['username'=>'guest'])->one()->id;
		$orderType = $ordertype;
		//Validate if pin is valide
		$model->status = 1;
		$this->createorder($model,$orderType);
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return 1;	
	}
	
	public function actionBulkPaymentbytransfer($ordertype)
	{
		
		$userId = !empty( Yii::$app->user->identity->id)? Yii::$app->user->identity->id:User::find()->where(['username'=>'guest'])->one()->id;;
		$orderType = $ordertype;
		//Validate if pin is valide
		//$model->status = 1;
		$status = 1;
		$paymentMode = 'transfer';
		$this->createBulkOrder($orderType,$paymentMode,$status);
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return 1;	
	}


	
	public function actionConverttodata($plan,$amount)
	{
		$model = new SellAirtime();
		$modelOder = new Order();
		$getPlanAmount = Plan::findOne($plan);
		$planAmount = $getPlanAmount->plan_amount;
		$modelOder->payment_mode = 'airtime' ;
		
		if($amount == $planAmount){
			$this->createAirtimSellOrder($model,1);
			if($model->save(false)){
				$modelOder -> airtime_id = $model-> sell_id;
				$modelOder->status = 0;
				$this->createorder($modelOder,'data');
				\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
				return 1;
			}
			
		} elseif($amount > $planAmount){
			$model->balance = $amount - $planAmount;
			$this->createAirtimSellOrder($model,1);
			if($model->save(false)){
				$modelOder->status = 0;
				$modelOder -> airtime_id = $model-> sell_id;
				$this->createorder($modelOder,'data');
				\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
				return 2;
			}
			
		} else{
			\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
				return 1;
		}
		
	}
	
	public function actionConverttocash()
	{
		$model = new SellAirtime();
		$this->createAirtimSellOrder($model,1);
		if($model->save(false)){
			\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			return 1;
		}
		
	}
	
	public function actionConverttowallet()
	{
		
		$model = new SellAirtime();
		$this->createAirtimSellOrder($model,1);
		if($model->save(false)){
			\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			return 1;
		}
		
		
		
	}
	
    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
     * this section is a none action section and should all be private cause the are only used inside of this 			controller
     * createorder is used to create all of the orders 
     * 
     * createAirtimSellOrder is used to create sells order 
     * 
     */
	
	private function createorder($model,$orderType)
	{
		$contactFormModel = new ContactForm();
		$contactFormModel->name = !empty(Yii::$app->user->identity->username)?Yii::$app->user->identity->username:'Guest' ;
    	$contactFormModel->email = !empty(Yii::$app->user->identity->id)?!empty(Contact::find()->where(['user_id' => Yii::$app->user->identity->id,'status'=>0])->one()->email)?Contact::find()->where(['user_id' => Yii::$app->user->identity->id,'status'=>0])->one()->email:Yii::$app->params['supportEmail']:Yii::$app->params['supportEmail'];
    	$contactFormModel->subject = $orderType ;
    	$contactFormModel->body = 'Mr/Miss/Mrs '.empty(Yii::$app->user->identity->username)?Yii::$app->params['supportEmail']:Yii::$app->user->identity->username.'You have placed a new order for '.$orderType.'pls note that based on your payment option, validating your payment might take close to 24hr after which you seleceted'.$orderType.' plan would be sent to you ';
    	$contactFormModel->bodyAdmin ='an order has been made and duties have been given to '.$this->adminUser('username').'to handle the request' ;
    	

		if ($model->load(Yii::$app->request->post()) ) {
			$model->order_type = $orderType;
			$model->administrator = $this->adminUser('id');
			
			$model->user_id = !empty( Yii::$app->user->identity->id)? Yii::$app->user->identity->id:User::find()->where(['username'=>'guest'])->one()->id;

			$model->save(false);
			$contactModel = new Contact();
			if(isset($_POST['friend']) or Yii::$app->user->isGuest){
				$contactModel->status = $model->order_id;
				$contactModel->user_id = !empty( Yii::$app->user->identity->id)? Yii::$app->user->identity->id:User::find()->where(['username'=>'guest'])->one()->id;
				//$contactModel->phone_number = $_POST['Contact[phone_number]'];
				$contactModel->scenario = $contactModel::SCENARIO_req;
				if($contactModel->load(Yii::$app->request->post())){
					
					$contactModel->save(false);
					
				}
			}
			
			
            if ( $contactFormModel->sendFeedbackEmail(Yii::$app->params['supportEmail'])) {
               return 1;
            }
			return ;
        }
	}
	
	private function createBulkOrder($orderType,$paymentMode,$status)
	{
		$contactFormModel = new ContactForm();
		$contactFormModel->name = !empty(Yii::$app->user->identity->username)?Yii::$app->user->identity->username:'Guest' ;
    	$contactFormModel->email = !empty(Yii::$app->user->identity->id)?!empty(Contact::find()->where(['user_id' => Yii::$app->user->identity->id,'status'=>0])->one()->email)?Contact::find()->where(['user_id' => Yii::$app->user->identity->id,'status'=>0])->one()->email:Yii::$app->params['supportEmail']:Yii::$app->params['supportEmail'];
    	$contactFormModel->subject = $orderType ;
    	$contactFormModel->body = 'Mr/Miss/Mrs '.empty(Yii::$app->user->identity->username)?Yii::$app->params['supportEmail']:Yii::$app->user->identity->username.'You have placed a new order for '.$orderType.'pls note that based on your payment option, validating your payment might take close to 24hr after which you seleceted'.$orderType.' plan would be sent to you ';
    	$contactFormModel->bodyAdmin ='an order has been made and duties have been given to '.$this->adminUser('username').'to handle the request' ;
    	

	
			// loop cart details and instantly insert into the deb with value from the cart 
		$admin = $this->adminUser('id');
		foreach(Yii::$app->cart->positions as $key => $value){
			$model = new Order();
			$model->administrator = $admin;
			$model->user_id = !empty( Yii::$app->user->identity->id)? Yii::$app->user->identity->id:User::find()->where(['username'=>'guest'])->one()->id;
			$contactModel = new Contact();
			$model->order_type = $orderType;
			$model->plan_id = $value->plan_id;
			$model->status = $status;
			$model->payment_mode = $paymentMode;
			$model->save(false);
			$contactModel->status = $model->order_id;
			$contactModel->phone_number = $value->phone_number;
			$contactModel->user_id = !empty( Yii::$app->user->identity->id)? Yii::$app->user->identity->id:User::find()->where(['username'=>'guest'])->one()->id;
			$contactModel->scenario = $contactModel::SCENARIO_req;
			$contactModel->save(false);
		}

		if ( $contactFormModel->sendFeedbackEmail(Yii::$app->params['supportEmail'])) {
			Yii::$app->cart->removeAll();
		   return 1;
		}
		return ;

	}
	
	private function createAirtimSellOrder($model,$status)
	{
		if ($model->load(Yii::$app->request->post()) ) {
			$model->status = $status;
			$model->user_id =  Yii::$app->user->identity->id;

			
			return ;
        }
	}
	
	public function actionAddToCart($id,$phonenumber)
	{
		$cart = Yii::$app->cart;

    	$model = Plan::findOne($id);
		$model->phone_number = $phonenumber;
		if ($model) {
			\Yii::$app->cart->put($model, 1);
			return 1;
		}
		throw new NotFoundHttpException();
	}
}
