<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

// Models 
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\Contact;
use frontend\models\User;
use frontend\models\Person;



/**
 * Site controller
 */
class DashboardController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
		
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
		if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }
		
        return $this->render('index');
    }
	
	public function actionProfile()
    {
		
		$contact = new Contact();
		$person = new Person();
		$user = new User();
		$getPerson = $person->find()->where(['user_id' => Yii::$app->user->identity->id])->one();
		$getContact = $contact->find()->where(['user_id' => Yii::$app->user->identity->id])->one();
		$getUser = $user;
		$getAllUsersDetails =  $user->find()->where(['id' => Yii::$app->user->identity->id])->one();
		if(empty($getContact) ){
			$getContact = $contact;
		}
		if(empty($getPerson) ){
			$getPerson = $person;
		}
		
        return $this->render('profile',[
			'contactModel' => $getContact,
			'personModel' => $getPerson,
			'userModel' => $getUser,
			'userDetails' => $getAllUsersDetails,
			
		]);
    }
	
	public function actionProfilesave()
    {
		
		$contact = new Contact();
		$person = new Person();
		$getPerson = $person->find()->where(['user_id' => Yii::$app->user->identity->id])->one();
		$getContact = $contact->find()->where(['user_id' => Yii::$app->user->identity->id])->one();
		if(empty($getContact) ){
			$getContact = $contact;
		}
		if(empty($getPerson) ){
			$getPerson = $person;
		}
		$getPerson->user_id = yii::$app->user->identity->id ;
		$getContact->user_id = yii::$app->user->identity->id;
		if($getPerson->load(Yii::$app->request->post()) and $getContact->load(Yii::$app->request->post())){
			$getPerson->save(false);
			$getContact->save(false);
			return ['success'=>'true', 'status'=>1];
			
			
		}
        
    }
	public function actionFundwallet(){
		$user = new User();
		$getUser = $user->findOne(Yii::$app->user->identity->id);
		
		
		
		$wallets = $_POST['User']['wallet'] + $getUser->wallet;
		$getUser->wallet = $wallets;
		$getUser->save(false);
		
		return 1;
		

		
	}

  
}
