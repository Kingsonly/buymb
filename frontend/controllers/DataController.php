<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Json;

// Models 
use frontend\models\Order;
use frontend\models\Plan;
use frontend\models\Contact;



/**
 * Site controller
 */
class DataController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
		$model = new Order();
		$contactModel = new Contact();
		$contactModel->scenario = $contactModel::SCENARIO_DATA;
		$myOrder = $model->find()->where(['user_id'=> Yii::$app->user->identity->id,'order_type'=>'data'])->all();
        return $this->render('index',[
			'myOrder' => $myOrder,
			'model' => $model,
			'contactModel' => $contactModel,
		]);
    }
	
	public function actionListItems() 
	{
    	$result = [];
		$post = Yii::$app->request->post('Order');
		$parents = $_POST['depdrop_parents'];
		
		$result = Plan::find()->where(['plan_type_id'=>$parents[0]])->select(['plan_id As id','plan_name   As name'])->asArray()->all();
		
		return Json::encode(['output'=>$result, 'selected'=>'1000']);  
		
		
		
		
		
		
    	//echo Json::encode(['output'=>'', 'selected'=>'']);
	}
	

  
}

