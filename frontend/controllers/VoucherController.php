<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Voucher;
use frontend\models\User;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VoucherController implements the CRUD actions for Voucher model.
 */
class VoucherController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Voucher models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new Voucher();
		$myVoucher = $model->find()->where(['user_id'=>Yii::$app->user->identity->id])->all();

        return $this->render('index', [
            'myVoucher' => $myVoucher,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Voucher model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Voucher model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Voucher();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->voucher_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
	
	function randomString($length = 15) {
		$str = "";
		$characters = array_merge(range('0','9'));
		$max = count($characters) - 1;
		for ($i = 0; $i < $length; $i++) {
			$rand = mt_rand(0, $max);
			$str .= $characters[$rand];
		}
		return $str;
	}
	
	public function actionWalletpayment($unit,$quantity)
    {
        $model = new Voucher();
		$userId = Yii::$app->user->identity->id;
		$getUser = User::findOne($userId);
		$amount = (int)$unit * (int)$quantity;
		$wallet = $getUser->wallet;

        if ($model->load(Yii::$app->request->post())) {
			if($wallet >= $amount ){
				for($i = 1; $i <= (int)$quantity; $i++){
					$model->setIsNewRecord(true);
					$model->voucher_id = null;

					$model->pin = $this->randomString();
					$model->user_id = $userId;
					$model->save(false);
				}
				
				$getUser->wallet = $getUser->wallet - $amount;
				$getUser->save(false);
			$x = 1;
			} else {
				$x = 2;
			}
            
        } else {
            $x = 0;
        }
		return !empty($x)?$x:1;
    }
	
	
	
	public function actionCardpayment($unit,$quantity)
    {
        $model = new Voucher();
		$userId = Yii::$app->user->identity->id;
		$getUser = User::findOne($userId);
		$amount = (int)$unit * (int)$quantity;
		

        if ($model->load(Yii::$app->request->post())) {
			
				for($i = 1; $i <= (int)$quantity; $i++){
					$model->setIsNewRecord(true);
					$model->voucher_id = null;

					$model->pin = $this->randomString();
					$model->user_id = $userId;
					$model->save(false);
				}
				
				
			$x = 1;
			
            
        } else {
            $x = 0;
        }
		return !empty($x)?$x:1;
    }


    /**
     * Updates an existing Voucher model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->voucher_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Voucher model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Voucher model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Voucher the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Voucher::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
