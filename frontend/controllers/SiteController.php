<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
// Models 
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\PlanType;
use frontend\models\Order;
use frontend\models\Contact;
use frontend\models\Plan;
use frontend\models\UsersFeedback;


/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */


    /**
     * @inheritdoc
     */
	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
	
	
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
	

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
		$this->layout = 'Site';
		// fetch all plantaype where type is  = data
		$planTypeDataModel = new PlanType();
		$dataModel = $planTypeDataModel->find()->where(['type'=>'data'])->andWhere(['status_type'=>0])->all();
		$airtimeModel = $planTypeDataModel->find()->where(['type'=>'airtime'])->all();
		$loginModel = new LoginForm();
		$signupModel = new SignupForm();
		
        return $this->render('index',[
			'dataType'=>$dataModel,
			'airtimeType'=>$airtimeModel,
			'model'=> new Order(),
			'contactModel'=> new Contact(),
			'loginModel'=> $loginModel,
			'signupModel'=> $signupModel,
			
		]);
    }
	
	public function actionGuest()
    {
		$this->layout = 'Site';
		// fetch all plantaype where type is  = data
		$model = new Order();
		$contactModel = new Contact();
		if (Yii::$app->user->isGuest) {
           $contactModel->scenario = $contactModel::SCENARIO_req;
        }
		
        return $this->renderAjax('orderform',[
			
			'model' => $model,
			'contactModel' => $contactModel,
		]);
		
    }
	

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
		$this->layout = 'Login';
        
		
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            //return $this->goBack();
			//$dashboard = Url::to(['/dashboard/index']);
			return $this->redirect(['/dashboard/index']);
        } else{
			return $this->redirect(['/site/index']);
			
		}
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
		$this->layout = 'Site';
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post())) {
			$model->to = Yii::$app->params['adminEmail'];
			//$model->email = Yii::$app->params['adminEmail'];
            if ($model->sendEmail()) {
                return 1;
            } else {
                return 0;
            }

            
        } 
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
		$this->layout = 'Site';
        return $this->render('about');
    }
	
	public function actionUsersfeedback($mainId)
    {
		$model = new UsersFeedback();
		if ($model->load(Yii::$app->request->post()) && $model->sendEmail()) {
			return $model->hidden;
		} 
        return $this->renderAjax('_feedbackform',[
			'model'=>$model,
			'mainId'=>$mainId,
		]);
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
		$this->layout = 'Login';
        $model = new SignupForm();
        
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->redirect(['dashboard/index']);;
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
		$this->layout = 'Login';
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
		$this->layout = 'Login';
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
	
	public function actionUpdatecontactemail($id){
		$contactModelId = Yii::$app->user->identity->id;
		$contactModel = Contact::find()->where(['user_id'=>$contactModelId,'status'=>0])->one();
		if(!empty($contactModel)){
			$contactModel->email = $id;
		if($contactModel->save(false)){
			return true;
		}
		} else{
			$newContactModel = new Contact();
			$newContactModel -> user_id = $contactModelId;
			$newContactModel -> email = $id;
			
			
		}
		
		
	}
	
	public function actionActualamount($id){
		$planModel = Plan::findOne($id);
		$percentage = Yii::$app->params['rate'];
		$amount = $planModel->plan_amount;
		$getPer = ($percentage / 100) * $amount;
		$newAmount = $amount - $getPer;
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return  $newAmount;
		
	}
	public function actionActualamountdata($id){
		$planModel = Plan::findOne($id);
		$amount = $planModel->plan_amount;
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $amount;
		
	}
	
}
