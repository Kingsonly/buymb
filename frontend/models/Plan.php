<?php

namespace frontend\models;
use yz\shoppingcart\CartPositionInterface;

use Yii;

/**
 * This is the model class for table "bmb_plan".
 *
 * @property integer $plan_id
 * @property string $plan_name
 * @property string $plan_amount
 * @property integer $plan_type_id
 *
 * @property BmbOrder[] $bmbOrders
 * @property BmbPlanType $planType
 * @property BmbRate[] $bmbRates
 */
class Plan extends \yii\db\ActiveRecord implements CartPositionInterface
{
    /**
     * @inheritdoc
     */
	public $phone_number;
    public static function tableName()
    {
        return 'bmb_plan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['plan_name', 'plan_amount', 'plan_type_id','validity'], 'required'],
            [['plan_type_id'], 'integer'],
            [['bonus','phone_number'], 'safe'],
            [['plan_name', 'plan_amount'], 'string', 'max' => 20],
            [['plan_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => PlanType::className(), 'targetAttribute' => ['plan_type_id' => 'type_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'plan_id' => 'Plan ID',
            'plan_name' => 'Plan Name eg(MTN 10mb)',
            'plan_amount' => 'Plan Amount eg(200)',
            'plan_type_id' => 'Plan Type ',
            'bonus' => 'Bonus (optional) eg(200mb)',
            'validity' => 'Validity eg(30 days)',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBmbOrders()
    {
        return $this->hasMany(Order::className(), ['plan_id' => 'plan_id']);
    }
	
	public function getPlanstring(){
		return $this->plan_name.' ('.$this->plan_amount.')';
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlanType()
    {
        return $this->hasOne(PlanType::className(), ['type_id' => 'plan_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBmbRates()
    {
        return $this->hasMany(Rate::className(), ['plan_id' => 'plan_id']);
    }
	
	 public function getPrice()
    {
        return $this->plan_amount;
    }

    public function getId()
    {
        return $this->plan_id;
    }
	
	public function getCost($withDiscount = true)
    {
        return $this->getPrice();
    }
	
	public function setQuantity($quantity)
    {
        return $quantity;
    }
	
	public function getQuantity()
    {
        return $this->setQuantity(1);
    }
}
