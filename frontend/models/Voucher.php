<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "bmb_voucher".
 *
 * @property integer $voucher_id
 * @property string $pin
 * @property string $amount
 * @property string $status
 * @property integer $user_id
 * @property string $date_created
 *
 * @property BmbUser $user
 */
class Voucher extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bmb_voucher';
    }

    /**
     * @inheritdoc
     */
	public $quantity;
	public $payment_mode;
    public function rules()
    {
        return [
            [['pin', 'amount', 'status', 'user_id', 'date_created'], 'required'],
            [['user_id'], 'integer'],
            [['date_created','quantity','payment_mode'], 'safe'],
            [['pin'], 'string', 'max' => 60],
            [['amount', 'status'], 'string', 'max' => 20],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'voucher_id' => 'Voucher ID',
            'pin' => 'Pin',
            'amount' => 'Unit',
            'status' => 'Status',
            'user_id' => 'User ID',
            'date_created' => 'Date Created',
            'payment_mode' => 'Mode of payment ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
