<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "bmb_order".
 *
 * @property integer $order_id
 * @property integer $user_id
 * @property integer $plan_id
 * @property string $payment_mode
 * @property string $order_type
 * @property string $status
 * @property integer $administrator
 * @property string $date_created
 *
 * @property BmbPlan $plan
 * @property BmbUser $user
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	public $plan_id_selector;
	public $customer_email;
    public static function tableName()
    {
        return 'bmb_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'plan_id', 'payment_mode', 'order_type', 'status', 'administrator','customer_email', 'date_created'], 'required'],
            [['user_id', 'plan_id', 'administrator'], 'integer'],
            [['order_type','airtime_network'], 'string'],
            [['date_created','plan_id_selector'], 'safe'],
            [['payment_mode', 'status'], 'string', 'max' => 50],
            [['plan_id'], 'exist', 'skipOnError' => true, 'targetClass' => Plan::className(), 'targetAttribute' => ['plan_id' => 'plan_id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id' => 'Order ID',
            'user_id' => 'User ID',
            'plan_id' => 'Plan',
            'payment_mode' => 'Payment Mode',
            'order_type' => 'Order Type',
            'status' => 'Status',
            'administrator' => 'Administrator',
            'date_created' => 'Date Created',
            'plan_id_selector' => 'Network',
            'airtime_network' => 'Network',
            'customer_email' => 'Your Email',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlan()
    {
        return $this->hasOne(Plan::className(), ['plan_id' => 'plan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
	
	public function getContact()
    {
        return $this->hasOne(Contact::className(), ['user_id' => 'user_id']);
    }
	
	public function getContacts()
    {
        return $this->hasOne(Contact::className(), ['status' => 'order_id']);
    }
	
	function smsSend($owneremail,$subacct,$subacctpwd,$sendto,$sender,$message){
        // owners email = kingsonly13c@gmail.com
		//subacct = kingsonly
		//subacctpassword = firstoctober
		$url =
		"http://www.bulksmsbase.com/components/com_spc/smsapi.php?username=".UrlEncode('kingsonly') . "&password=".UrlEncode('P@55w)rd')."&sender=".UrlEncode($sender)."&recipient=".UrlEncode($sendto)."&message=".UrlEncode($message)."&msgid=4";
		
		if ($f = @fopen($url, "r")){ 
			$answer = fgets($f, 255);
			if (substr($answer, 0, 1) == "+"){
				$this->insertMessageInDb($sender,$sendto,$message);
				return "SMS to $dnr was successful.";
				
			} elseif($answer){
				$this->insertMessageInDb($sender,$sendto,$message);
				return $answer;
			} else {
				return 0;
			}
		} else {
			return 0;
		}   
       
    }
	
}
