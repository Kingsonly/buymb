<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "bmb_person".
 *
 * @property integer $person_id
 * @property string $first_name
 * @property string $last_name
 * @property integer $user_id
 * @property string $dob
 * @property string $account_number
 *
 * @property BmbUser $user
 */
class Person extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bmb_person';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'user_id', 'dob', 'account_number'], 'required'],
            [['user_id'], 'integer'],
            [['dob'], 'safe'],
            [['first_name', 'last_name'], 'string', 'max' => 50],
            [['account_number'], 'string', 'max' => 20],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'person_id' => 'Person ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'user_id' => 'User ID',
            'dob' => 'Dob',
            'account_number' => 'Account Number',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
