<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "bmb_contact".
 *
 * @property integer $contact_id
 * @property integer $user_id
 * @property string $email
 * @property string $address
 * @property string $phone_number
 *
 * @property BmbUser $user
 */
class Contact extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	const SCENARIO_DATA = 'data';
	const SCENARIO_req = 'req';
    public static function tableName()
    {
        return 'bmb_contact';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'email', 'address', 'phone_number'], 'required'],
            [['user_id'], 'integer'],
            [['email', 'address'], 'string', 'max' => 50],
            [['phone_number'], 'string', 'max' => 20],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'contact_id' => 'Contact ID',
            'user_id' => 'User ID',
            'email' => 'Email',
            'address' => 'Address',
            'phone_number' => 'Phone Number',
        ];
    }
	
	public function scenarios()
    {
        return [
            self::SCENARIO_DATA => [],
			self::SCENARIO_req => ['phone_number']
			
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
