<?php
namespace frontend\models;

use yii\base\Model;
use common\models\User as CommonUser;
use frontend\models\Contact;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
   
    public $password;
    public $phone;
    


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
			['phone', 'required'],
			
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            /*['email', 'trim'],
            ['email', 'safe'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],*/

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new CommonUser();
        $contact = new Contact();
        $user->username = $this->username;
        $contact->phone_number = $this->phone;
       // $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
		$user->save();
        $contact->user_id = $user->id;
        return $contact->save(false) ? $user : null;
    }
}
