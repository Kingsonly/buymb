<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "bmb_plan_type".
 *
 * @property integer $type_id
 * @property string $type
 * @property string $network
 *
 * @property BmbPlan[] $bmbPlans
 */
class PlanType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	
    public static function tableName()
    {
        return 'bmb_plan_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'network'], 'required'],
            [['type'], 'string'],
            [['network'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'type_id' => 'Type ID',
            'type' => 'Type',
            'network' => 'Network',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
	
    public function getPlans()
    {
        return $this->hasMany(Plan::className(), ['plan_type_id' => 'type_id'])->andWhere(['status'=>0]);
    }
}
