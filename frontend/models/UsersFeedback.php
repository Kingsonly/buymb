<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class UsersFeedback extends Model
{
    
    public $email;
    
    public $body;
    
	public $startRating;
	public $hidden;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'subject', 'body','startRating'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            ['hidden', 'safe'],
            // verifyCode needs to be entered correctl
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
            'body' => 'Comment',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return bool whether the email was sent
     */
    public function sendEmail()
    {
        return Yii::$app->mailer->compose(['html' => 'usersfeedbackreply', 'text' => 'usersfeedbacktextreply'],
                [
                    'email' => $this->email,
                    'body'  => $this->body,
                    'star'  => $this->startRating,
                    
                ])
            ->setTo(Yii::$app->params['supportEmail'])
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['siteName'] . ' Feedback bot'])
            ->setSubject('Users Feedback')
            ->setTextBody($this->body)
            ->send();
    }
	
	
	
}
