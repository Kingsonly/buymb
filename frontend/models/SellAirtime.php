<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "bmb_sell_airtime".
 *
 * @property integer $sell_id
 * @property string $airtime_brand
 * @property string $amount
 * @property string $airtime_pin
 * @property string $status
 * @property string $option
 */
class SellAirtime extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bmb_sell_airtime';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['airtime_brand', 'amount', 'airtime_pin', 'status', 'option'], 'required'],
            [['airtime_brand', 'status'], 'string', 'max' => 20],
            [['amount', 'option'], 'string', 'max' => 50],
            [['account_name','account_number'], 'safe'],
            [['airtime_pin'], 'string', 'max' => 60],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sell_id' => 'Sell ID',
            'airtime_brand' => 'Mobile Network',
            'amount' => 'Amount',
            'airtime_pin' => 'Airtime Pin',
            'status' => 'Status',
            'option' => 'Option',
        ];
    }
	
	public function getPlantype()
    {
        return $this->hasOne(PlanType::className(), ['type_id' => 'airtime_brand']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
	
	public function getContact()
    {
        return $this->hasOne(Contact::className(), ['user_id' => 'user_id']);
    }
}
