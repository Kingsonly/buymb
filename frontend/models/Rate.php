<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "bmb_rate".
 *
 * @property integer $rate_id
 * @property integer $plan_id
 * @property string $amount
 *
 * @property BmbPlan $plan
 */
class Rate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bmb_rate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['plan_id', 'amount'], 'required'],
            [['plan_id'], 'integer'],
            [['amount'], 'string', 'max' => 60],
            [['plan_id'], 'exist', 'skipOnError' => true, 'targetClass' => Plan::className(), 'targetAttribute' => ['plan_id' => 'plan_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'rate_id' => 'Rate ID',
            'plan_id' => 'Plan ID',
            'amount' => 'Amount',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlan()
    {
        return $this->hasOne(Plan::className(), ['plan_id' => 'plan_id']);
    }
}
