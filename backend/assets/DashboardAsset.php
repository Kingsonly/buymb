<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class DashboardAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        
		'dist/css/AdminLTE.min.css',
		'dist/css/skins/_all-skins.min.css',
		'css/fontawesome/css/font-awesome.css',
		'css/fontawesome/css/main.css',
    ];
    public $js = [
		//'plugins/jQuery/jquery-2.2.3.min.js',
		'plugins/slimScroll/jquery.slimscroll.min.js',
		'plugins/fastclick/fastclick.js',
		'dist/js/app.min.js',
		'dist/js/demo.js',
		'dist/js/sketch.js',
		'bootstrap/js/bootstrap.min.js',
		'js/datatables/jquery.dataTables.min.js',
        'js/datatables/dataTables.bootstrap.min.js',
		
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
