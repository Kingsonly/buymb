<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\BackendLoginForm;
use backend\models\BackendOrder;
use backend\models\BackendSellAirtime;
use backend\models\BackendPlanType;
use backend\models\BackendPlan;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error','signup'],
                        'allow' => true,
                    ],
                    [
                        
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
		$modelPlanType = new \backend\models\BackendPlanType();
		$modelPlan = new \backend\models\BackendPlan();
		$findAllPlanType = $modelPlanType->find()->all();
		$findAllPlan = $modelPlan->find()->all();
        return $this->render('index',[
			'planTypeModel' => $modelPlanType,
			'planModel' => $modelPlan,
			'allPlanType' => $findAllPlanType,
			'allPlan' => $findAllPlan,
		]);
    }
	
	
	public function actionOrder()
    {
		$model = new BackendOrder();
		$pendingOrder = $model -> find()->where(['status'=>1,'administrator'=>Yii::$app->user->identity->id])->orWhere(['status'=>0,'administrator'=>Yii::$app->user->identity->id])->all();
		$approvedOrder = $model -> find()->where(['status'=>2,'administrator'=>Yii::$app->user->identity->id])->all();
        return $this->render('order',[
			'pendingOrder'=>$pendingOrder,
			'approvedOrder'=>$approvedOrder
		]);
    }
	
	public function actionAirtime()
    {
		$model = new BackendSellAirtime();
		$pendingOrder = $model -> find()->where(['status'=>1])->orWhere(['status'=>0])->all();
		$approvedOrder = $model -> find()->where(['status'=>2])->all();
        return $this->render('airtime',[
			'pendingOrder'=>$pendingOrder,
			'approvedOrder'=>$approvedOrder
		]);
    }
	
	public function actionOrderdetails($id)
    {
		$main = new BackendOrder();
		
		$model = $main->findAll($id);
		
        return $this->renderAjax('orderdetails',[
			'model' => $model
		]);
    }
	
	public function actionApprove($id)
    {
		$main = new BackendOrder();
		
		$model = $main->findOne($id);
		//$model = $main->find()->where(['order_id' => $id])->one();
		$model->status = 2;
		if($model->save(false)){
			$owneremail="kingsonly13c@gmail.com";
			$subacct="KINGSONLY";
			$subacctpwd="firstoctober";
			$sendto = !empty($model->contact->phone_number)?$model->contact->phone_number:'08083430800'; 
			$sender = 'BUYMB';
			$message = !empty($model->contact->phone_number)? 'Order has been confirmed':'Message was not sent to the contact cause contact does not have a number';
			$model->smsModel($owneremail,$subacct,$subacctpwd,$sendto,$sender,$message);
			$data = 1;
		} else {
			$data = 2;
		}
		
        return $data;
    }
	public function actionApproveairtime($id)
    {
		$main = new BackendSellAirtime();
		
		$model = $main->findOne($id);
		//$model = $main->find()->where(['order_id' => $id])->one();
		$model->status = 2;
		if($model->save(false)){
			$owneremail="kingsonly13c@gmail.com";
			$subacct="BUYMB";
			$subacctpwd="firstoctober";
			$sendto = !empty($model->contact->phone_number)?$model->contact->phone_number:'08083430800'; 
			$sender = 'BUYMB';
			$message = !empty($model->contact->phone_number)? 'Thanks for your patronage , your order has been sent please dial *461*2# on mtn, *127*0# on glo *229*9# on 9m0bile and *140# on airtel to view data':'Message was not sent to the contact cause contact does not have a number';
			$model->smsModel($owneremail,$subacct,$subacctpwd,$sendto,$sender,$message);
			$data = 1;
		} else {
			$data = 2;
		}
		
        return $data;
    }
	
	public function actionApproveairtimeorder($id)
    {
		$main = new BackendOrder();
		$mainAirtime = new BackendSellAirtime();
		
		
		$modelAirtime = $mainAirtime->findOne($id);
		$model = $main->find()->where(['airtime_id'=>$modelAirtime->sell_id])->one();
		
		$model->status = 1;
		$modelAirtime->status = 2;
		
		if($model->save(false) and $modelAirtime->save(false)){
			$owneremail="kingsonly13c@gmail.com";
			$subacct="BUYMB";
			$subacctpwd="firstoctober";
			$sendto = $model->contact->phone_number; 
			$sender = 'BUYMB';
			$message = 'Order has been confirmed';
			$model->smsModel($owneremail,$subacct,$subacctpwd,$sendto,$sender,$message);
			$data = 1;
		} else {
			$data = 2;
		}
		
        return $data;
    }
	
	public function actionAirtimedetails($id)
    {
		$main = new BackendSellAirtime();
		
		$model = $main->findAll($id);
		
        return $this->renderAjax('airtimedetails',[
			'model' => $model
		]);
    }
	
	
    public function actionLogin()
    {
		$this->layout = 'loginsignup';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new BackendLoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
	
	public function actionPlan()
	{
		$model = new \backend\models\BackendPlan();

			if ($model->load(Yii::$app->request->post())) {
			if ($model->validate()) {
				// save data
				$model->save();
				return 1;
			}
		} else{
			return 0;
		}
	}
	
	public function actionPlantype()
	{
		$model = new \backend\models\BackendPlanType();

		if ($model->load(Yii::$app->request->post())) {
			if ($model->validate()) {
				// save data
				$model->save();
				return 1;
			}
		} else{
			return 0;
		}

	}
	
	/*
	* this section contains code to update and delete all activities on the admindashboard
	*
	**/
	public function actionUpdatePlan($id){
		$model = BackendPlan::findOne($id);
		if($model->status == 1){
			$model->status = 0;
			if($model->save(false)){
				return 1;
			} else{
				return 0;
			}
		} else{
			$model->status = 1;
			if($model->save(false)){
				return 1;
			} else{
				return 0;
			}
		}
		
	}
	public function actionDeletePlan($id){
		$model = BackendPlan::findOne($id);
		if($model->delete()){
			return 1;
		} else{
			return 0;
		}
	}
	
	public function actionUpdatePlanType($id){
		$model = BackendPlanType::findOne($id);
		if($model->status_type == 1){
			$model->status_type = 0;
			if($model->save()){
				return 1;
			} else{
				return 0;
			}
		} else{
			$model->status_type = 1;
			if($model->save()){
				return 1;
			} else{
				return 0;
			}
		}
	}
	public function actionDeletePlanType($id){
		$model = BackendPlanType::findOne($id);
		if($model->delete()){
			return 1;
		} else{
			return 0;
		}
		
	}
}
