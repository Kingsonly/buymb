<?php

namespace backend\models;

use Yii;
use common\models\CommonPlanType;

/**
 * This is the model class for table "bmb_plan_type".
 *
 * @property integer $type_id
 * @property string $type
 * @property string $network
 *
 * @property BmbPlan[] $bmbPlans
 */
class BackendPlanType extends CommonPlanType
{
   public function getNetworktype(){
	   return  'Network: '.$this->network.' ( Type: '.$this->type .' ).';
   }
}
