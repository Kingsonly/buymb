<?php

namespace backend\models;

use Yii;
use common\models\CommonOrder;

/**
 * This is the model class for table "bmb_order".
 *
 * @property integer $order_id
 * @property integer $user_id
 * @property integer $plan_id
 * @property string $payment_mode
 * @property string $order_type
 * @property string $status
 * @property integer $administrator
 * @property string $date_created
 *
 * @property BmbPlan $plan
 * @property BmbUser $user
 */
class BackendOrder extends CommonOrder
{
   public function getAirtime()
    {
        return $this->hasOne(BackendSellAirtime::className(), ['sell_id' => 'airtime_id']);
    }
	/*
	*used to send sms to contact
	*/
	public function  smsModel($owneremail,$subacct,$subacctpwd,$sendto,$sender,$message)
	{
		$url =
		"http://www.smslive247.com/http/index.aspx?" . "cmd=sendquickmsg"
		. "&owneremail=" . UrlEncode($owneremail)
		. "&subacct=" . UrlEncode($subacct)
		. "&subacctpwd=" . UrlEncode($subacctpwd) . "&message=" . UrlEncode($message)."&sendto=".UrlEncode($sendto)."&sender=".UrlEncode($sender);
		/* call the URL */
		
		if ($f = @fopen($url, "r")){
			$answer = fgets($f, 255);
		}
	}
	// End of sending sms 
	
}
