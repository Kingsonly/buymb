<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\DashboardAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

DashboardAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
  <meta charset="<?= Yii::$app->charset ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Fixed Layout</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
 
</head>
<body class="hold-transition skin-blue fixed sidebar-mini" >
<?php $this->beginBody() ?>
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>B</b>MB</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>BuyMb</b> Admin</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- <img src="../../dist/img/user2-160x160.jpg" class="user-image" alt="User Image"> -->
              <span class="hidden-xs"><?=Yii::$app->user->identity->username;?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <!--  <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> -->

                <p>
                  <?=Yii::$app->user->identity->username;?>
                  
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <?= Html::a('Logout', Url::to(['/site/logout']), ['data-method' => 'POST','class' => 'btn btn-default btn-flat']) ?>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel" style="height:50px;">
        <div class="pull-left image">
          <!-- <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">-->
        </div>
        <div class="pull-left info">
			<p><?=Yii::$app->user->identity->username;?></p> <!--   display users name       -->
          
        </div>
      </div>
      <!-- search form -->
      
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        
        
        <li>
			<?= Html::a(Html::tag('i', '', ['class' => 'fa fa-th','title' => 'Dashboard']). Html::tag('span', 'Dashboard', ['class' => '','title' => 'dashboard']), ['index'], ['class' => '']) ?>
		</li>
		  
		  <li>
			<?= Html::a(Html::tag('i', '', ['class' => 'fa fa-th','title' => 'order']). Html::tag('span', 'Order', ['class' => '','title' => 'Open folder']), ['order'], ['class' => '']) ?>
		</li>
		  
		 <li>
			<?= Html::a(Html::tag('i', '', ['class' => 'fa fa-th','title' => 'Airtime']). Html::tag('span', 'Airtime', ['class' => '','title' => 'Airtime']), ['airtime'], ['class' => '']) ?>
		</li>
		<li>
			<?= Html::a(Html::tag('i', '', ['class' => 'fa fa-th','title' => 'Voucher']). Html::tag('span', 'Voucher', ['class' => '','title' => 'Voucher']), ['#'], ['class' => '']) ?>
		</li>
		  
		  <li>
			<?= Html::a(Html::tag('i', '', ['class' => 'fa fa-th','title' => 'Bitcoin']). Html::tag('span', 'Bitcoin', ['class' => '','title' => 'Bitcoin']), ['#'], ['class' => '']) ?>
		</li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      
      
    </section>

    <!-- Main content -->
    <section class="content">
      
      <?= $content; ?>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.8
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>

  
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
