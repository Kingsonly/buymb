<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dasgboard-index">

	<section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
		  <div class="col-sm-12 col-lg-6">
			  <table id="example1" class="table table-bordered table-striped">
							
                        	<thead>
                            <tr>
                              <th>SN</th>
                              <th>Plan</th>
                              <th>Payment Option</th>
                              
                              <th>Status</th>
                              <th>Date</th>
                  
                            </tr>
                        
							</thead>
                        	<tbody>
                            <? $sn=1; foreach($pendingOrder as $k=>$v){ ?>
								
                                <tr>
								
                                  <td class="projecturl"><?=$sn;?></td>
                                  <td class="projecturl"><?=$v['plan']['plan_name'].' ('.$v['plan']['plan_amount'].')';?></td>
                                  <td class="projecturl"><?=$v['payment_mode'];?></td>
                                  <td class="projecturl <?= $v['status'] == 1?'danger':'success' ?>">
									  <?= $v['status'] == 1?'Pending':'Approved' ?>
									</td>
                                  <td class="projecturl"><?=$v['date_created'];?></td>

                                </tr>
								
                            <? $sn++; }?>
                   
                  
							</tbody>
                        	<tfoot>
								<tr>
								  <th>SN</th>
								  <th>Plan</th>
								  <th>Payment Option</th>
								  <th>Status</th>
								  <th>Date</th>

								</tr>
                			</tfoot>
                
                    
						</table>
		  </div>
		  
		  <div class="col-sm-12 col-lg-6"> 
			  <div class="box" id = "project-title">
				  <div class="box-body" id = "project-title">
			  	<table id="example1" class="table table-bordered table-striped">
							
                        	<thead>
                            <tr>
                              <th>SN</th>
                              <th>Plan</th>
                              <th>Payment Option</th>
                              <th>Status</th>
								<th>Action</th>
                              <th>Date</th>
                              
                  
                            </tr>
                        
							</thead>
                        	<tbody>
                            <? $sn=1; foreach($approvedOrder as $k=>$v){ ?>
								
                                <tr>
								
                                  <td class="projecturl"><?=$sn;?></td>
                                  <td class="projecturl"><?=$v['plan']['plan_name'].' ('.$v['plan']['plan_amount'].')';?></td>
                                  <td class="projecturl"><?=$v['payment_mode'];?></td>
                                  <td class="projecturl <?= $v['status'] == 1?'danger':'success' ?>">
									  <?= $v['status'] == 1?'Pending':'Approved' ?>
									</td>
                                  <td class="projecturl"><span class="viewdetails btn">View</span></td>
                                  <td class="projecturl"><?=$v['date_created'];?></td>

                                </tr>
								
                            <? $sn++; }?>
                   
                  
							</tbody>
                        	<tfoot>
								<tr>
								  <th>SN</th>
								  <th>Plan</th>
								  <th>Payment Option</th>
								  <th>Status</th>
									<th>Action</th>
								  <th>Date</th>

								</tr>
                			</tfoot>
                
                    
						</table>
			  </div>
			  </div>
		  </div>
        <!-- ./col -->
      </div>
	</section>
	</div>


<? 
		Modal::begin([
			'header' =>'<h1 id="headers"></h1>',
			'id' => 'details',
			'size' => 'modal-md',  
		]);
?>
<div id="formcontent"><img class="loadergif" src="images/loader.gif"  /></div>
<?
	Modal::end();
?>
    
</div>


<?


$order = <<<JS

$('.viewdetails').click(function(){
	
	var datas=$(this).data('id');
	$('.hideall').hide();
	$('#formcontent').show();
	$('#details').modal('show').find('#formcontent').html('<img class="loadergif" src="images/loader.gif"  />');
	$('#details').modal('show').find('#formcontent').load(datas);
	$('#details').modal('show').find('#headers').html(' ');
  	$('#details').modal('show').find('#headers').html('Create New Project');
});




JS;
 
$this->registerJs($order);
?>
