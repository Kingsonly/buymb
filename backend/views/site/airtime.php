<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
	#loaderspan{
		width: 40% !important;
		margin: 0 auto !important;
	}
	#formcontentsent{
		display:none;
	}
	.greens,.reds{
		width:100% !important;
		color: #fff;
	}
	.greens{
		background: green;
	}
	.reds{
		background: red;
	}
	#test{
		display: none;
	}
	.imgdivs{
		width: 40% !important;
		margin: 0 auto !important;
	}
</style>
<div class="dasgboard-index">

	<section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
		  <div class="col-sm-12 col-lg-6">
			  <div class="box" id = "project-title">
				  <div class="box-body" id = "project-title">
			  <table id="example1" class="table table-bordered table-striped">
							
                        	<thead>
                            <tr>
                              <th>SN</th>
                              <th>Network</th>
                              <th>Amount</th>
                              <th>Pin</th>
                              <th>Status</th>
                              <th>Action</th>
                  
                            </tr>
                        
							</thead>
                        	<tbody>
                            <? $sn=1; foreach($pendingOrder as $k=>$v){ ?>
								
                                <tr>
								
                                  <td class="projecturl"><?=$sn;?></td>
                                  <td class="projecturl"><?=$v['plantype']['network'];?></td>
                                  <td class="projecturl"><?=$v['amount'];?></td>
								<td class="projecturl"><?=$v['airtime_pin'];?></td>
                                  <td class="projecturl <?= $v['status'] <= 1?'danger':'success' ?>">
									  <?= $v['status'] <= 1?'Pending':'Approved' ?>
									</td>
                                  
									<td class="projecturl"><span data-id="<?=Url::to(['site/airtimedetails','id'=>$v["sell_id"]]) ?>" class="viewdetails btn">View</span></td>
                                </tr>
								
                            <? $sn++; }?>
                   
                  
							</tbody>
                        	<tfoot>
								<tr>
								  <th>SN</th>
                              <th>Network</th>
                              <th>Amount</th>
                              <th>Pin</th>
                              <th>Status</th>
                              <th>Action</th>

								</tr>
                			</tfoot>
                
                    
						</table>
				  </div>
			  </div>
		  </div>
		  
		  <div class="col-sm-12 col-lg-6"> 
			  <div class="box" id = "project-title">
				  <div class="box-body" id = "project-title">
			  	<table id="example1" class="table table-bordered table-striped">
							
                        	<thead>
                            <tr>
                              <th>SN</th>
                              <th>Network</th>
                              <th>Amount</th>
                              <th>Pin</th>
                              <th>Status</th>
                              <th>Action</th>
                  
                            </tr>
                        
							</thead>
                        	<tbody>
                            <? $sn=1; foreach($approvedOrder as $k=>$v){ ?>
								
                                 <tr>
								
                                  <td class="projecturl"><?=$sn;?></td>
                                  <td class="projecturl"><?=$v['plantype']['network'];?></td>
                                  <td class="projecturl"><?=$v['amount'];?></td>
								<td class="projecturl"><?=$v['airtime_pin'];?></td>
                                  <td class="projecturl <?= $v['status'] <= 1?'danger':'success' ?>">
									  <?= $v['status'] <= 1?'Pending':'Approved' ?>
									</td>
                                  
									<td class="projecturl"><span data-id="<?=Url::to(['site/airtimedetails','id'=>$v["sell_id"]]) ?>" class="viewdetails btn">View</span></td>
                                </tr>
								
                            <? $sn++; }?>
                   
                  
							</tbody>
                        	<tfoot>
								<tr>
								  <th>SN</th>
                              <th>Network</th>
                              <th>Amount</th>
                              <th>Pin</th>
                              <th>Status</th>
                              <th>Action</th>
									

								</tr>
                			</tfoot>
                
                    
						</table>
			  </div>
			  </div>
		  </div>
        <!-- ./col -->
      </div>
	</section>
    <? 
		Modal::begin([
			'header' =>'<h1 id="headers"></h1>',
			'id' => 'details',
			'size' => 'modal-md',  
		]);
?>
<div id="formcontent"></div>
<div id="formcontentsent"></div>
<?
	Modal::end();
?>
</div>

<?

$url = Url::to(['site/approveairtime']);
$order = <<<JS

$('.viewdetails').click(function(){
	
	var datas=$(this).data('id');
	$('.hideall').hide();
	$('#formcontentsent').hide();
	$('#formcontent').show();
	$('#details').modal('show').find('#formcontent').html('<div id="loaderspan"><img class="loadergif" src="../images/loader.gif"  /></div>');
	$('#details').modal('show').find('#formcontent').load(datas);
	
});

$(document).on('click','#approve',function(){
	id = $(this).data('id');
	$('#approvetxt').hide();
	$('#BtnLoaderp2').show();
	$('#BtnLoaderp').show();
	$.post('$url'+'?id='+id )
		.always(function(result){
	
			
   			
   			if(result==1){
	   			$('#BtnLoaderp2').hide();
	   			$('#BtnLoaderp').hide();
				$('#details').modal('show').find('#formcontent').hide();
				$('#details').modal('show').find('#formcontentsent').show();
	   			$('#details').find('#formcontentsent').html('<div class="imgdivs"><img class="imagess" src="../images/images.png"  /></div><br><h1>Request Have Been Approved</h1>').show().addClass('greens');
				
    		}else{
				$('#BtnLoader').hide();
				$('#details').modal('show').find('#formcontent').hide();
				$('#details').modal('show').find('#formcontentsent').show();
	   			$('#details').find('#formcontentsent').html('<div class="imgdivs"><img class="imagess" src="../images/images.png"  /><div><br><h1>An error occured</h1>').show().addClass('reds');
				
				
	
    		}
			
			
    	}).fail(function(){
    		console.log('Server Error');
    	});
		
		var countDownDate = new Date().getTime()+10000;

// Update the count down every 1 second
var x = setInterval(function() {

    // Get todays date and time
    var now = new Date().getTime();
    
    // Find the distance between now an the count down date
    var distance = countDownDate - now;
    
    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
    // Output the result in an element with id="demo"
    document.getElementById("demo").innerHTML = days + "d " + hours + "h "
    + minutes + "m " + seconds + "s ";
    
    // If the count down is over, write some text 
    if (distance < 0) {
        clearInterval(x);
        
    
		
		
		
	$('#details').modal().find('#formcontent').show();
	$('#details').modal().find('#formcontentsent').hide();
	$('#approvetxt').show();
	$(document).find('#submitButtonText').show();
	$('#details').modal('hide');
	$('#test').show();
	 function Particle( x, y, radius ) {
            this.init( x, y, radius );
        }

        Particle.prototype = {

            init: function( x, y, radius ) {

                this.alive = true;

                this.radius = radius || 10;
                this.wander = 0.15;
                this.theta = random( TWO_PI );
                this.drag = 0.92;
                this.color = '#fff';

                this.x = x || 0.0;
                this.y = y || 0.0;

                this.vx = 0.0;
                this.vy = 0.0;
            },

            move: function() {

                this.x += this.vx;
                this.y += this.vy;

                this.vx *= this.drag;
                this.vy *= this.drag;

                this.theta += random( -0.5, 0.5 ) * this.wander;
                this.vx += sin( this.theta ) * 0.1;
                this.vy += cos( this.theta ) * 0.1;

                this.radius *= 0.96;
                this.alive = this.radius > 0.5;
            },

            draw: function( ctx ) {

                ctx.beginPath();
                ctx.arc( this.x, this.y, this.radius, 0, TWO_PI );
                ctx.fillStyle = this.color;
                ctx.fill();
            }
        };

        // ----------------------------------------
        // Example
        // ----------------------------------------

        var MAX_PARTICLES = 280;
        var COLOURS = [ '#69D2E7', '#A7DBD8', '#E0E4CC', '#F38630', '#FA6900', '#FF4E50', '#F9D423' ];

        var particles = [];
        var pool = [];

        var demo = Sketch.create({
            container: document.getElementById( 'test' ),
            retina: 'auto'
        });

        demo.setup = function() {

            // Set off some initial particles.
            var i, x, y;

            for ( i = 0; i < 100; i++ ) {
                x = ( demo.width * 0.5 ) + random( -100, 100 );
                y = ( demo.height * 0.5 ) + random( -100, 100 );
                demo.spawn( x, y );
            }
        };

        demo.spawn = function( x, y ) {
            
            var particle, theta, force;

            if ( particles.length >= MAX_PARTICLES )
                pool.push( particles.shift() );

            particle = pool.length ? pool.pop() : new Particle();
            particle.init( x, y, random( 5, 40 ) );

            particle.wander = random( 0.5, 2.0 );
            particle.color = random( COLOURS );
            particle.drag = random( 0.9, 0.99 );

            theta = random( TWO_PI );
            force = random( 2, 8 );

            particle.vx = sin( theta ) * force;
            particle.vy = cos( theta ) * force;

            particles.push( particle );
        };

        demo.update = function() {

            var i, particle;

            for ( i = particles.length - 1; i >= 0; i-- ) {

                particle = particles[i];

                if ( particle.alive ) particle.move();
                else pool.push( particles.splice( i, 1 )[0] );
            }
        };

        demo.draw = function() {

            demo.globalCompositeOperation  = 'lighter';

            for ( var i = particles.length - 1; i >= 0; i-- ) {
                particles[i].draw( demo );
            }
        };
		
	$.pjax.reload({container:"#order_approved",async: false}); 
	$.pjax.reload({container:"#order_pending",async: false}); 
	$(".order1").DataTable({
        "aaSorting": [],
		"pagingType": "simple",
		"responsive": "true",
		
    });
	
	$(".order2").DataTable({
        "aaSorting": [],
		"pagingType": "simple",
		"responsive": "true",
		
    });
	$('#test').delay(1000).hide(0);
	
	//}, 5000);
	}
}, 1000);
    return false;
});



$(".table").DataTable({
        "aaSorting": [],
		"pagingType": "simple",
		"responsive": "true",
		
    });
	
	$(".order2").DataTable({
        "aaSorting": [],
		"pagingType": "simple",
		"responsive": "true",
		
    });


JS;
 
$this->registerJs($order);
?>
