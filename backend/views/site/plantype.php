<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\BackendPlanType */
/* @var $form ActiveForm */
?>
<div class="site-plantype">

    <?php $form = ActiveForm::begin(['id'=>'create_new_plan_type']); ?>

        <?= $form->field($model, 'type') ?>
        <?= $form->field($model, 'network') ?>
    
        <div class="form-group">
            <? //= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
			<?= Html::submitButton($model->isNewRecord ? '<span id="submitButtonTextpt">Submit</span>'.Html::img("@web/images/45.gif", ["alt" => "loader","id"=>"BtnLoaderpt", "class" => "user-image23" ]).' <span id="feedbackpt"><span>' : 'Update', ['class' => $model->isNewRecord ? 'btn btn btn-primary' : 'btn btn btn-primary','id'=>'submitbtnpt']) ?>

        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-plantype -->
