<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
	
	#BtnLoaderpt,#BtnLoaderp,.hiddenedite{
		display:none;
	}
	
	
</style>
<div class="dasgboard-index">
	<h1 style="padding:0px 15px;">
		Dashboard
	</h1>

	<section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
		  <a href='<?=Url::to(['order']) ?>' >
			  <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>Orders</h3>

              <p > View all data and airtime orders </p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href='<?=Url::to(['order']) ?>'  class="small-box-footer">Click Here <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
		  </a>
		  
		  <a href='<?=Url::to(['airtime']) ?>' >
			  <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>Air Time</h3>

              <p>View airtime sell request order</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="<?=Url::to(['airtime']) ?>" class="small-box-footer">Click Here <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
		  </a>
        <!-- ./col -->
		  <a href='<?=Url::to('../voucher/index') ?>' >
			  <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>Voucher</h3>

              <p>Buy/convert</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="<?=Url::to('voucher/index') ?>" class="small-box-footer">Click Here <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
		  </a>
        <!-- ./col -->
		  <a href='<?=Url::to('../bitcoin/index') ?>' >
			  <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>Bit Coin</h3>

              <p>Buy/Sell</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="<?=Url::to('bitcoin/index') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
		  </a>
        <!-- ./col -->
      </div>
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		<div class="row">
			<div class="col-sm-6">
				
				<div class="box box-solid">
				<div class="box box-black collapsed-box">
					<div class="box-header with-border" id = "box-header">
						<div  style="border: 1px solid #fff;width:30%;float: left">

							

						</div>
						<div style="border: 1px solid #fff;width:40%;float: left;text-align: center">
							<h3 class="box-title">New Network Option</h3>
						</div>
						<div class="box-tools  pull-right">
							<button  class="btn btn-box-tool" data-widget="collapse" style="background: #fff !important;font-size: 16px" ><i class="fa fa-caret-down"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" style="background: #fff !important"><i class="fa fa-times"></i></button>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<?php Pjax::begin(['id' => 'remark_reload']); ?>
							
						
						  <?= $this->render('plantype',[
						'model' => $planTypeModel,
					]);?>
						<?php Pjax::end(); ?>
					</div>			  
				</div>
			</div>
		
				
			</div>
			
			
			<div class="col-sm-6">
				<?php Pjax::begin(['id' => 'plan_type_pjax']); ?>
				
				
				<div class="box box-solid">
				<div class="box box-black collapsed-box">
					<div class="box-header with-border" id = "box-header">
						<div  style="border: 1px solid #fff;width:30%;float: left">

							

						</div>
						<div style="border: 1px solid #fff;width:40%;float: left;text-align: center">
							<h3 class="box-title">Network Options</h3>
						</div>
						<div class="box-tools  pull-right">
							<button  class="btn btn-box-tool" data-widget="collapse" style="background: #fff !important;font-size: 16px" ><i class="fa fa-caret-down"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" style="background: #fff !important"><i class="fa fa-times"></i></button>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table class="table  table-striped table-inverse table-hover network">
			<thead class="">
				<tr>
					<th>SN</th>
					<th>Type</th>
					<th>Network</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<? $sn=1;foreach($allPlanType as $k => $v){ ?>
				<tr>
					<td><?= $sn ?> </td>
					<td><?= $v['type']; ?></td>
					<td> <?= $v['network']; ?> </td>
					<td >
						<span class='updatearea'>
							<? if( $v['status_type']==0){?>
							
							<span>
							<i  class="fa fa-pencil-square-o typeupdate" aria-hidden="true" data-id="<?= $v['type_id']; ?>" style="color:red"></i>
								
								<i  class="fa fa-pencil-square-o typeupdate hiddenedite" aria-hidden="true" data-id="<?= $v['type_id']; ?>" style="color:green"></i>
							</span>
							<?}else{?>
							
								<span>
							<i  class="fa fa-pencil-square-o typeupdate" aria-hidden="true" data-id="<?= $v['type_id']; ?>" style="color:green"></i>
								
								<i  class="fa fa-pencil-square-o typeupdate hiddenedite" aria-hidden="true" data-id="<?= $v['type_id']; ?>" style="color:red"></i>
							</span>
							<?}?>
						</span>
						<span>
							<i  class="fa fa-trash typedelete" aria-hidden="true" data-id="<?= $v['type_id']; ?>" style="color:red"></i>

						</span> 
					</td>
				</tr>
				<? $sn++;}?>
			
			</tbody>
			</table>
					</div>			  
				</div>
			</div>
				
				
			
				<?php Pjax::end(); ?>
			</div>
		
		</div>
		
		<div class="row">
			<div class="col-sm-6">
				
				
				
				
				<div class="box box-solid">
				<div class="box box-black collapsed-box">
					<div class="box-header with-border" id = "box-header">
						<div  style="border: 1px solid #fff;width:30%;float: left">

							

						</div>
						<div style="border: 1px solid #fff;width:40%;float: left;text-align: center">
							<h3 class="box-title">Create A New Plan</h3>
						</div>
						<div class="box-tools  pull-right">
							<button  class="btn btn-box-tool" data-widget="collapse" style="background: #fff !important;font-size: 16px" ><i class="fa fa-caret-down"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" style="background: #fff !important"><i class="fa fa-times"></i></button>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<?= $this->render('plan',[
						'model' => $planModel,
					]);?>
					</div>			  
				</div>
			</div>
				
				
			</div>
			<div class="col-sm-6">
				<?php Pjax::begin(['id' => 'plan_pjax']); ?>
				
				
				<div class="box box-solid">
				<div class="box box-black collapsed-box">
					<div class="box-header with-border" id = "box-header">
						<div  style="border: 1px solid #fff;width:30%;float: left">

							

						</div>
						<div style="border: 1px solid #fff;width:40%;float: left;text-align: center">
							<h3 class="box-title">View All Plans</h3>
						</div>
						<div class="box-tools  pull-right">
							<button  class="btn btn-box-tool" data-widget="collapse" style="background: #fff !important;font-size: 16px" ><i class="fa fa-caret-down"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" style="background: #fff !important"><i class="fa fa-times"></i></button>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table class="table  table-striped table-inverse table-hover plans">
							<thead class="">
								<tr>
									<th>SN</th>
									<th>Name</th>
									<th>Amount</th>
									<th>Type</th>
									<th>Validity</th>
									<th>Bonus</th>
									<th>Status</th>
									<th>Action</th>

								</tr>
							</thead>
							<tbody>
								<? $sn=1;foreach($allPlan as $k => $v){ ?>
								<tr>
									<td><?= $sn ?> </td>
									<td><?= $v['plan_name']; ?></td>
									<td> <?= $v['plan_amount']; ?> </td>
									<td> <?= $v['planType']['type'].' ('.$v['planType']['network'] .')'; ?> </td>
									<td> <?= $v['validity']; ?> </td>
									<td> <?= $v['bonus']; ?> </td>
									<td> <?= $v['status']; ?> </td>
									<td>  
										<span class='updatearea'>
							<? if( $v['status']==0){?>
							
							<span>
							<i  class="fa fa-pencil-square-o planupdate" aria-hidden="true" data-id="<?= $v['plan_id']; ?>" style="color:red"></i>
								
								<i  class="fa fa-pencil-square-o planupdate hiddenedite" aria-hidden="true" data-id="<?= $v['plan_id']; ?>" style="color:green"></i>
							</span>
							<?}else{?>
							
								<span>
							<i  class="fa fa-pencil-square-o planupdate" aria-hidden="true" data-id="<?= $v['plan_id']; ?>" style="color:green"></i>
								
								<i  class="fa fa-pencil-square-o planupdate hiddenedite" aria-hidden="true" data-id="<?= $v['plan_id']; ?>" style="color:red"></i>
							</span>
							<?}?>
						</span>
						<span>
							<i  class="fa fa-trash plandelete" aria-hidden="true" data-id="<?= $v['plan_id']; ?>" style="color:red"></i>

						</span> 
									</td>
								</tr>
								<? $sn++;}?>

							</tbody>
						</table>
					</div>			  
				</div>
			</div>
				
				
				
				
			<?php Pjax::end(); ?>
			</div>
		
		</div>
	</section>
    
</div>

 


<?
$urlPlanType = Url::to(['site/plantype']);
$urlPlan = Url::to(['site/plan']);
$updateType = Url::to(['site/update-plan-type']);
$deleteType = Url::to(['site/delete-plan-type']);

$updatePlan = Url::to(['site/update-plan']);
$deletePlan = Url::to(['site/delete-plan']);
$backendIndexAjax = <<<JS

  $('.typeupdate').click(function(){
  		\$this=$(this);
  		$.post('$updateType'+'?id='+\$this.data('id'))
	.always(function(result){


		if(result==1){
		\$this.hide();
		\$this.parent().find('.hiddenedite').show();
		
			alert('Updated');
		}else{
			alert('Something went wrong');
		}
	}).fail(function(){
		console.log('Server Error');
	});
  })
  
  $('.typedelete').click(function(){
 
  		\$this=$(this);
  		$.post('$deleteType'+'?id='+\$this.data('id'))
	.always(function(result){


		if(result==1){
			alert('Deleted');
		}else{
			alert('Something went wrong');
		}
	}).fail(function(){
		console.log('Server Error');
	});
  })
  
  $('.planupdate').click(function(){
  		\$this=$(this);
  		$.post('$updatePlan'+'?id='+\$this.data('id'))
	.always(function(result){


		if(result==1){
		\$this.hide();
		\$this.parent().find('.hiddenedite').show();
		
			alert('Updated');
		}else{
			alert('Something went wrong');
		}
	}).fail(function(){
		console.log('Server Error');
	});
  })
  
  $('.plandelete').click(function(){
  		\$this=$(this);
  		$.post('$deletePlan'+'?id='+\$this.data('id'))
	.always(function(result){


		if(result==1){
			alert('Deleted');
		}else{
			alert('Something went wrong');
		}
	}).fail(function(){
		console.log('Server Error');
	});
  })
  
$('#create_new_plan_type').on('beforeSubmit', function (e) {
	$('#submitButtonTextpt').hide();
 	$('#BtnLoaderpt').show();
    var \$form = $(this);
	var planid = $('#plan_id').val();
	var paymentMode = $('#paymentmode').val();
	
	
	
	$.post('$urlPlanType',\$form.serialize())
	.always(function(result){


		if(result==1){
			$('#BtnLoaderpt').hide();
			$(document).find('#feedbackpt').html('Sent').show();
		}else{
			$('#BtnLoaderpt').hide();
			$(document).find('#feedbackpt').html('Not enought funds in wallet').show();

		}
	}).fail(function(){
		console.log('Server Error');
	});
	 
    
	
	setTimeout(function(){ 
	$(document).find('#feedbackpt').hide();
	$(document).find('#BtnLoaderpt').hide();
	$(document).find('#submitButtonTextpt').show();
	$.pjax.reload({container:"#plan_type_pjax",async: false}); 
	}, 5000);
    return false;
 	
    
});

$('#create_new_plan').on('beforeSubmit', function (e) {
	$('#submitButtonTextp').hide();
 	$('#BtnLoaderp').show();
    var \$form = $(this);
	
	
	
	
	$.post('$urlPlan',\$form.serialize())
	.always(function(result){


		if(result==1){
			$('#BtnLoaderp').hide();
			$(document).find('#feedbackp').html('Sent').show();
		}else{
			$('#BtnLoaderp').hide();
			$(document).find('#feedbackp').html('Something went wrong').show();

		}
	}).fail(function(){
		console.log('Server Error');
	});
	 
    
	
	setTimeout(function(){ 
	$(document).find('#feedbackp').hide();
	$(document).find('#BtnLoaderp').hide();
	$(document).find('#submitButtonTextp').show();
	$.pjax.reload({container:"#plan_pjax",async: false}); 
	}, 5000);
    return false;
 	
    
});

$(".plans").DataTable({
        "aaSorting": [],
		"pagingType": "simple",
		"responsive": "true",
		
    });
	
	$(".network").DataTable({
        "aaSorting": [],
		"pagingType": "simple",
		"responsive": "true",
		
    });

JS;
 
$this->registerJs($backendIndexAjax);
?>
