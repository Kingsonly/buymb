<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<style>
	#BtnLoaderp,#BtnLoaderp2{
		display: none;
	}
</style>

			  <div class="box" id = "project-title">
				  <div class="box-body" id = "project-title">
			  	<table id="example1" class="table table-bordered table-striped">
							
                        	
                        	<tbody>
                            <? $sn=1; foreach($model as $k=>$v){ ?>
								
								<tr>
								
                                  <td class="projecturl">Plan name</td>
                                  <? if($v['order_type'] == 'airtime'){?>
										<td class="projecturl">
									  N<?=$v['plan_id'];?>
									</td>
									<?}else{?>
                                  <td class="projecturl">
									  <?=$v['plan']['plan_name'].' ('.$v['plan']['plan_amount'].')';?>
									</td>
									<?}?>
								</tr>
								
								<tr>
								
                                  <td class="projecturl">Mode of payment</td>
                                  <td class="projecturl"><?=$v['payment_mode'];?></td>
								</tr>
								
								<tr>
								
                                  <td class="projecturl">Requested for</td>
                                  <td class="projecturl"><?=$v['order_type'];?></td>
								</tr>
								
								<tr>
								
                                  <td class="projecturl">Username</td>
                                  <td class="projecturl"><?=$v['user']['username']; ?></td>
								</tr>
								
								<tr>
								
                                  <td class="projecturl"><?=!empty($v['contacts']['phone_number'])?'Friends Phone Number':'Phone number'; ?></td>
                                  <td class="projecturl"><?=!empty($v['contacts']['phone_number'])?$v['contacts']['phone_number']:$v['contact']['phone_number']; ?></td>
								</tr>
								
								<tr>
								
                                  <td class="projecturl">Date</td>
                                  <td class="projecturl"><?=$v['date_created'];?></td>
								</tr>
								<tr>
								
                                  <td class="projecturl">Status</td>
                                  <td class="projecturl <?= $v['status'] == 1?'danger':'success' ?>">
									  <?= $v['status'] == 1?'Pending':'Approved' ?>
								</tr>
								
								<tr>
								
                                  <td colspan="2" class="projecturl" style="text-align :right">
									  <? if($v['payment_mode'] == 'airtime' and $v['status'] == 0){ ?>
									  
									  <span class="btn btn-danger" id='viewairtimeaproval' data-id="<?=$v['order_id']; ?>">Click To Confirm Airtime 
									  </span>
									  <? } else { ?>
								
									  
									  
									  
									  <span class="btn btn-danger" id='approve' data-id="<?=$v['order_id']; ?>">
										  
										  <span id="approvetxt">
											  Approve
										   </span>
										 <?= Html::img("@web/images/45.gif", ["alt" => "loader","id"=>"BtnLoaderp", "class" => "user-image23" ]);?>
										  <span id="feedbackp"><span>
									  </span>
									  
									  <? }?>
									  
									
									</td>
                                  
								</tr>
								<? if((int)$v['airtime_id'] > 0){?>
								<tr>
								
                                  <td class="projecturl">Airtime pin </td>
                                  <td class="projecturl">
									  <?= $v['airtime']['airtime_pin']; ?>
								</tr>
								
								<tr>
								
                                  <td class="projecturl">Airtime Amount </td>
                                  <td class="projecturl">
									  <?= $v['airtime']['amount']; ?>
								</tr>
								
								<tr>
								
                                
                                  <td class="projecturl " colspan="2">
									  
									   <span class="btn btn-<?= $v['airtime']['status'] == 1?'danger':'success' ?>" id='approveairtime' data-id="<?=$v['airtime_id']; ?>">
										  
										  <span id="approvetxt">
											  <?= $v['airtime']['status'] == 1?'Pending':'Approved' ?>
										   </span>
										 <?= Html::img("@web/images/45.gif", ["alt" => "loader","id"=>"BtnLoaderp2", "class" => "user-image23" ]);?>
										  <span id="feedbackp"><span>
									  </span>
								</tr>
								
								<? }?>
                                
								
                            <? $sn++; }?>
                   
                  
							</tbody>
                        	
                
                    
						</table>
			  </div>
			  </div>
		  