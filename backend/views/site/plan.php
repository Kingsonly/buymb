<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\backendPlanType;

/* @var $this yii\web\View */
/* @var $model backend\models\BackendPlan */
/* @var $form ActiveForm */
$planType = new backendPlanType();
?>
<div class="site-plan">

    <?php $form = ActiveForm::begin(['id'=>'create_new_plan']); ?>

        <?= $form->field($model, 'plan_name') ?>
        <?= $form->field($model, 'plan_amount') ?>
		<?= $form->field($model, 'plan_type_id')
			->dropDownList(ArrayHelper::map($planType->find()->all(), 'type_id', 'networktype'),[ 'prompt'=> 'Select...', 'class' => 'form-control', 'id' => 'plantype']) 
		?>
		<?= $form->field($model, 'validity') ?>
	 	<?= $form->field($model, 'bonus') ?>
    
	
		
    
        <div class="form-group">
            <? //= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
			<?= Html::submitButton($model->isNewRecord ? '<span id="submitButtonTextp">Submit</span>'.Html::img("@web/images/45.gif", ["alt" => "loader","id"=>"BtnLoaderp", "class" => "user-image23" ]).' <span id="feedbackp"><span>' : 'Update', ['class' => $model->isNewRecord ? 'btn btn btn-primary' : 'btn btn btn-primary','id'=>'submitbtnp']) ?>

        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-plan -->
