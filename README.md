# README #

This README would normally document whatever steps are necessary to get your application up and running.

/************************* What is this repository for? ***********************************

* This is the root repository for the project Buymb
### How do I set it up? ###

* First you need to have php setup on your computer as a "global" program to be able to run the command below. If you do not know how to install php globally on your computer, google it. 
* On your terminal run php init. Make sure you are within the context of the Buymb repository. In other words, you should navigate to where the repository is stored on your computer using your console/terminal/command prompt (in Administrator mode) (they are all the same thing for windows and linux users)

* Next you will see the following:  
	* Which environment do you want the application to be initialized in?

		  [0] Development
		  [1] Production
		 * Enter 0 for development
	 * Initialize the application under 'Development' environment? [yes|no]
		 * Select "yes"
 

[* Optional: 

	If you have executed the command before you may see the following:
	* exist backend/config/main-local.php
				...overwrite? [Yes|No|All|Quit]
	* Select Yes if you want a fresh start with new config file. Select No if you want to retain your old config files.
	
	* exist common/config/main-local.php
				...overwrite? [Yes|No|All|Quit] 
	* Select Yes if you want a fresh start with new config file. Select No if you want to retain your old config files.

	* exist frontend/config/main-local.php
				...overwrite? [Yes|No|All|Quit] 
	* Select Yes if you want a fresh start with new config file. Select No if you want to retain your old config files.
]

* Finally, you need to run a composer update to update your vendor files.
	* To run "composer update", you need to have "composer" as a global program. For Windows users, this mean that you have the composer.phar file and the location is set in your environment variable. 
	* Alternatively, you can run "php composer.phar update". In this case, php will run the composer.phar file and update your vendor files. 

* To view site on the browser enter 
			... http://localhost/buymb/frontend/web/ for (Front end)
			... http://localhost/buymb/backend/web/ for (backend end)


*********************** END *******************************/


/******************** Other information *****************************/

* Guidelines for Writing tests - coming soon 
* Guidelines for Code review - coming soon 

