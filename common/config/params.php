<?php
return [
    'adminEmail' => 'admin@buymb.ng',
    'supportEmail' => 'admin@buymb.ng',
	'siteName' => 'BuyMb',
    'user.passwordResetTokenExpire' => 3600,
];
