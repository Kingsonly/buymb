<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $name  string user name*/
/* @var $email string user email */
/* @var $body  string the review */
?>
<div class="user-feedback">
    <p><?= Html::encode($name) ?>, thank you for placing an order.</p>

    <p>You would get a feedback soon</p>
</div>