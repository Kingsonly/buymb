<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $name  string user name*/
/* @var $email string user email */
/* @var $body  string the review */
?>
<div class="admin-feedback">
    <p><?= Html::encode($name) ?>(<?= Html::encode($email) ?>) Made a new request.</p>

    <blockquote>
        <?= Html::encode($body) ?>
    </blockquote>
</div>