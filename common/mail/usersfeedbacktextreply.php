<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $name  string user name*/
/* @var $email string user email */
/* @var $body  string the review */
?>

<div class="admin-feedback">
    <p>Users Email <?= Html::encode($email) ?></p>
    <p>Buymb was rated <?= Html::encode($star) ?> Stars</p>

    <blockquote>
		<h3>Comment</h3>
        <?= Html::encode($body) ?>
    </blockquote>
</div>