<?php

namespace common\models;

use Yii;
use frontend\models\PlanType;
/**
 * This is the model class for table "bmb_plan_type".
 *
 * @property integer $type_id
 * @property string $type
 * @property string $network
 *
 * @property BmbPlan[] $bmbPlans
 */
class CommonPlanType extends PlanType
{
    
}
