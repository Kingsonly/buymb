<?php

namespace common\models;

use Yii;
use frontend\models\SellAirtime;

/**
 * This is the model class for table "bmb_sell_airtime".
 *
 * @property integer $sell_id
 * @property string $airtime_brand
 * @property string $amount
 * @property string $airtime_pin
 * @property string $status
 * @property string $option
 */
class CommonSellAirtime extends SellAirtime
{

}
