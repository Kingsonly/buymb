<?php

namespace Common\models;

use Yii;
use frontend\models\Plan;
/**
 * This is the model class for table "bmb_plan".
 *
 * @property integer $plan_id
 * @property string $plan_name
 * @property string $plan_amount
 * @property integer $plan_type_id
 *
 * @property BmbOrder[] $bmbOrders
 * @property BmbPlanType $planType
 * @property BmbRate[] $bmbRates
 */
class CommonPlan extends Plan
{
    
}
