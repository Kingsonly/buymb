<?php

namespace common\models;

use Yii;
use backend\models\BackendUser;

/**
 * This is the model class for table "bmb_order".
 *
 * @property integer $order_id
 * @property integer $user_id
 * @property integer $plan_id
 * @property string $payment_mode
 * @property string $order_type
 * @property string $status
 * @property integer $administrator
 * @property string $date_created
 *
 * @property BmbPlan $plan
 * @property BmbUser $user
 */
class CommonAdminUsers extends BackendUser
{
   
}
