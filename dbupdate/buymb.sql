-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 31, 2017 at 05:26 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `buymb`
--

-- --------------------------------------------------------

--
-- Table structure for table `bmb_contact`
--

CREATE TABLE `bmb_contact` (
  `contact_id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `phone_number` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bmb_order`
--

CREATE TABLE `bmb_order` (
  `order_id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `plan_id` int(255) NOT NULL,
  `payment_mode` varchar(50) NOT NULL,
  `order_type` enum('data','airtime','bitcoin','') NOT NULL,
  `status` varchar(50) NOT NULL,
  `administrator` int(100) NOT NULL,
  `date_created` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bmb_person`
--

CREATE TABLE `bmb_person` (
  `person_id` int(255) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `user_id` int(255) NOT NULL,
  `dob` date NOT NULL,
  `account_number` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bmb_plan`
--

CREATE TABLE `bmb_plan` (
  `plan_id` int(255) NOT NULL,
  `plan_name` varchar(20) NOT NULL,
  `plan_amount` varchar(20) NOT NULL,
  `plan_type_id` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bmb_plan_type`
--

CREATE TABLE `bmb_plan_type` (
  `type_id` int(11) NOT NULL,
  `type` enum('data','airtime','bitcoin','') NOT NULL,
  `network` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bmb_rate`
--

CREATE TABLE `bmb_rate` (
  `rate_id` int(255) NOT NULL,
  `plan_id` int(60) NOT NULL,
  `amount` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bmb_sell_airtime`
--

CREATE TABLE `bmb_sell_airtime` (
  `sell_id` int(255) NOT NULL,
  `airtime_brand` varchar(20) NOT NULL,
  `amount` varchar(50) NOT NULL,
  `airtime_pin` varchar(60) NOT NULL,
  `status` varchar(20) NOT NULL,
  `option` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bmb_user`
--

CREATE TABLE `bmb_user` (
  `id` int(255) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `access_level` int(50) NOT NULL,
  `auth` varchar(50) NOT NULL,
  `status` int(5) NOT NULL,
  `auth_key` varchar(200) NOT NULL,
  `password_hash` varchar(200) NOT NULL,
  `password_reset_token` varchar(200) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bmb_user`
--

INSERT INTO `bmb_user` (`id`, `username`, `password`, `access_level`, `auth`, `status`, `auth_key`, `password_hash`, `password_reset_token`, `created_at`, `updated_at`) VALUES
(3, 'demo', '', 0, '', 10, 'DAeZBHPJtgP79K11uOkjuY7Pm9qcqwM1', '$2y$13$TY9To5vsp5fqEVLU.kUK3uALjOJ2lXZGrMTUP/3bRqPV8WM1sgWh2', '', '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `bmb_voucher`
--

CREATE TABLE `bmb_voucher` (
  `voucher_id` int(255) NOT NULL,
  `pin` varchar(60) NOT NULL,
  `amount` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL,
  `user_id` int(255) NOT NULL,
  `date_created` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bmb_contact`
--
ALTER TABLE `bmb_contact`
  ADD PRIMARY KEY (`contact_id`),
  ADD KEY `con_user_id` (`user_id`);

--
-- Indexes for table `bmb_order`
--
ALTER TABLE `bmb_order`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `orderuser` (`user_id`),
  ADD KEY `orderplan` (`plan_id`);

--
-- Indexes for table `bmb_person`
--
ALTER TABLE `bmb_person`
  ADD PRIMARY KEY (`person_id`),
  ADD KEY `personuser` (`user_id`);

--
-- Indexes for table `bmb_plan`
--
ALTER TABLE `bmb_plan`
  ADD PRIMARY KEY (`plan_id`),
  ADD KEY `plantype` (`plan_type_id`);

--
-- Indexes for table `bmb_plan_type`
--
ALTER TABLE `bmb_plan_type`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `bmb_rate`
--
ALTER TABLE `bmb_rate`
  ADD PRIMARY KEY (`rate_id`),
  ADD KEY `rateplan` (`plan_id`);

--
-- Indexes for table `bmb_sell_airtime`
--
ALTER TABLE `bmb_sell_airtime`
  ADD PRIMARY KEY (`sell_id`);

--
-- Indexes for table `bmb_user`
--
ALTER TABLE `bmb_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bmb_voucher`
--
ALTER TABLE `bmb_voucher`
  ADD PRIMARY KEY (`voucher_id`),
  ADD KEY `voucheruser` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bmb_contact`
--
ALTER TABLE `bmb_contact`
  MODIFY `contact_id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bmb_order`
--
ALTER TABLE `bmb_order`
  MODIFY `order_id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bmb_person`
--
ALTER TABLE `bmb_person`
  MODIFY `person_id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bmb_plan`
--
ALTER TABLE `bmb_plan`
  MODIFY `plan_id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bmb_plan_type`
--
ALTER TABLE `bmb_plan_type`
  MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bmb_rate`
--
ALTER TABLE `bmb_rate`
  MODIFY `rate_id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bmb_sell_airtime`
--
ALTER TABLE `bmb_sell_airtime`
  MODIFY `sell_id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bmb_user`
--
ALTER TABLE `bmb_user`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `bmb_voucher`
--
ALTER TABLE `bmb_voucher`
  MODIFY `voucher_id` int(255) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `bmb_contact`
--
ALTER TABLE `bmb_contact`
  ADD CONSTRAINT `con_user_id` FOREIGN KEY (`user_id`) REFERENCES `bmb_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bmb_order`
--
ALTER TABLE `bmb_order`
  ADD CONSTRAINT `orderplan` FOREIGN KEY (`plan_id`) REFERENCES `bmb_plan` (`plan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `orderuser` FOREIGN KEY (`user_id`) REFERENCES `bmb_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bmb_person`
--
ALTER TABLE `bmb_person`
  ADD CONSTRAINT `personuser` FOREIGN KEY (`user_id`) REFERENCES `bmb_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bmb_plan`
--
ALTER TABLE `bmb_plan`
  ADD CONSTRAINT `plantype` FOREIGN KEY (`plan_type_id`) REFERENCES `bmb_plan_type` (`type_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bmb_rate`
--
ALTER TABLE `bmb_rate`
  ADD CONSTRAINT `rateplan` FOREIGN KEY (`plan_id`) REFERENCES `bmb_plan` (`plan_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bmb_voucher`
--
ALTER TABLE `bmb_voucher`
  ADD CONSTRAINT `voucheruser` FOREIGN KEY (`user_id`) REFERENCES `bmb_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
