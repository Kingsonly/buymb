-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 30, 2018 at 12:19 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `buymb`
--

-- --------------------------------------------------------

--
-- Table structure for table `bmb_admin_user`
--

CREATE TABLE `bmb_admin_user` (
  `id` int(255) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `access_level` int(50) NOT NULL,
  `auth` varchar(50) NOT NULL,
  `status` int(5) NOT NULL,
  `auth_key` varchar(200) NOT NULL,
  `password_hash` varchar(200) NOT NULL,
  `password_reset_token` varchar(200) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `wallet` int(255) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bmb_admin_user`
--

INSERT INTO `bmb_admin_user` (`id`, `username`, `password`, `access_level`, `auth`, `status`, `auth_key`, `password_hash`, `password_reset_token`, `created_at`, `updated_at`, `wallet`) VALUES
(3, 'demo', '', 0, '', 10, 'DAeZBHPJtgP79K11uOkjuY7Pm9qcqwM1', '$2y$13$bALWS4jJCxPAJK3NuAQ7Q.ohjx5n5rWt0AIywhcpi7xSp31ovrQNe', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 49000),
(4, 'demo2', '', 0, '', 10, 'Q01KPW5Ffknux_4nUBoW2q8lAAwJrbHY', '$2y$13$bALWS4jJCxPAJK3NuAQ7Q.ohjx5n5rWt0AIywhcpi7xSp31ovrQNe', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(5, 'demo23', '', 0, '', 10, 'dTqgcWl9UHQ3BKC8KEc5pz1F-33Df-oQ', '$2y$13$QH5inFZhLLCElKzDWclWg.IwaD4tG019Dud0n47HjExfLsfH1PwpC', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(6, 'demo237', '', 0, '', 10, 'EmXDzhkt0K2zXnbCtbhqye2dbRWC_1b4', '$2y$13$/0TpoUoAPBdl8BoomElWvOWTSc/dPFv2A5Vq98CnSXapemIpbsUhW', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(7, 'fake', '', 0, '', 10, 'pwkmK5lw-5tgJdiXsP_e5BQuOMaQqaGn', '$2y$13$UGTTOt0Htu.9JVo9ju8eRe2qAuyBr2Irb4rhu6Gtr01KI8YYrU7fS', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(8, 'fake2', '', 0, '', 10, '4TWELzVJCeXFgMhcOuXD98Nrshh3oU2n', '$2y$13$JJqE4BQe/XrGLT3TYRRW6.96tix6N9y8IgMJwWtBjP1bYSXKui37G', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bmb_contact`
--

CREATE TABLE `bmb_contact` (
  `contact_id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `status` int(5) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bmb_contact`
--

INSERT INTO `bmb_contact` (`contact_id`, `user_id`, `email`, `address`, `phone_number`, `status`) VALUES
(1, 6, '', '', '0987654345678', 0),
(2, 7, '', '', '0909090909', 0),
(3, 3, 'kingsonly13c@gmail.com', '', '08083430800', 0),
(6, 3, '', '', '2345678', 55),
(7, 3, '', '', '1', 58),
(8, 8, '', '', '2', 60),
(9, 8, '', '', '3', 61),
(10, 8, '', '', '4', 62),
(11, 3, '', '', '5', 87),
(12, 8, '', '', '6', 11),
(13, 8, '', '', '7', 12),
(14, 8, '', '', '8', 13),
(15, 3, '', '', '9', 17),
(16, 3, '', '', '10', 18),
(17, 3, '', '', '11', 22),
(18, 3, '', '', '12', 23),
(19, 3, '', '', '13', 24),
(20, 3, '', '', '14', 25),
(21, 3, '', '', '12345678', 26),
(22, 3, '', '', '081532000000', 27),
(23, 8, '', '', '081532000000', 28),
(24, 8, '', '', '12345678', 29),
(25, 8, '', '', '081535111111', 30),
(26, 8, '', '', '12345678', 37),
(27, 8, '', '', '12345678', 38),
(28, 8, '', '', '12345678', 39),
(29, 8, '', '', '12345678', 40),
(30, 8, '', '', '12345678', 41),
(31, 8, '', '', '12345678', 42),
(32, 8, '', '', '12345678', 43),
(33, 8, '', '', '12345678', 44),
(34, 8, '', '', '12345678', 45),
(35, 8, '', '', '12345678', 46);

-- --------------------------------------------------------

--
-- Table structure for table `bmb_order`
--

CREATE TABLE `bmb_order` (
  `order_id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `plan_id` float NOT NULL,
  `payment_mode` varchar(50) NOT NULL,
  `order_type` enum('data','airtime','bitcoin','') NOT NULL,
  `status` varchar(50) NOT NULL,
  `administrator` int(100) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `airtime_id` int(100) NOT NULL,
  `airtime_network` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bmb_order`
--

INSERT INTO `bmb_order` (`order_id`, `user_id`, `plan_id`, `payment_mode`, `order_type`, `status`, `administrator`, `date_created`, `airtime_id`, `airtime_network`) VALUES
(1, 3, 1, 'wallet', 'data', '2', 3, '2018-04-12 00:53:29', 0, '0'),
(2, 3, 100, 'transfer', 'airtime', '1', 3, '2018-04-12 01:06:55', 0, '0'),
(3, 8, 1, 'transfer', 'data', '1', 3, '2018-04-23 11:01:38', 0, '0'),
(4, 3, 200, 'transfer', 'airtime', '1', 4, '2018-04-29 01:20:11', 0, '0'),
(5, 3, 200, 'transfer', 'airtime', '1', 5, '2018-04-29 01:24:01', 0, '0'),
(6, 3, 200, 'transfer', 'airtime', '1', 4, '2018-04-29 01:27:18', 0, ''),
(7, 3, 200, 'transfer', 'airtime', '1', 8, '2018-04-29 01:30:09', 0, '3'),
(8, 3, 200, 'wallet', 'airtime', '1', 6, '2018-04-29 01:31:02', 0, '3'),
(9, 8, 12345, 'transfer', 'airtime', '1', 3, '2018-05-02 15:40:00', 0, '3'),
(10, 8, 5, 'transfer', 'data', '1', 8, '2018-05-02 15:43:24', 0, ''),
(11, 8, 2, 'transfer', 'data', '1', 8, '2018-05-02 15:47:10', 0, ''),
(12, 8, 12345, 'transfer', 'airtime', '1', 4, '2018-05-02 15:57:57', 0, '3'),
(13, 8, 12345, 'transfer', 'airtime', '1', 3, '2018-05-02 16:06:42', 0, '3'),
(14, 8, 12345, 'transfer', 'airtime', '1', 7, '2018-05-02 16:11:41', 0, '3'),
(15, 8, 12345, 'transfer', 'airtime', '1', 7, '2018-05-02 16:12:49', 0, '3'),
(16, 3, 200, 'transfer', 'airtime', '1', 5, '2018-05-02 16:14:29', 0, ''),
(17, 3, 200, 'transfer', 'airtime', '1', 4, '2018-05-02 16:14:42', 0, ''),
(18, 3, 1, 'transfer', 'data', '1', 4, '2018-05-02 16:15:38', 0, ''),
(19, 3, 1, 'transfer', 'data', '1', 6, '2018-05-02 16:18:54', 0, ''),
(20, 3, 1, 'wallet', 'data', '1', 5, '2018-05-02 16:21:43', 0, ''),
(21, 3, 1, 'wallet', 'data', '1', 8, '2018-05-02 16:21:52', 0, ''),
(22, 3, 1, 'wallet', 'data', '1', 5, '2018-05-02 16:22:52', 0, ''),
(23, 3, 1, 'wallet', 'data', '1', 8, '2018-05-02 16:27:49', 0, ''),
(24, 3, 1, 'wallet', 'data', '1', 6, '2018-05-02 16:33:32', 0, ''),
(25, 3, 1, 'wallet', 'data', '1', 3, '2018-05-02 16:40:47', 0, ''),
(26, 3, 1, 'wallet', 'data', '1', 5, '2018-05-02 16:45:01', 0, ''),
(27, 3, 1233, 'transfer', 'airtime', '1', 5, '2018-05-02 16:46:05', 0, '3'),
(28, 8, 200, 'transfer', 'airtime', '1', 3, '2018-05-02 16:46:59', 0, '3'),
(29, 8, 1, 'transfer', 'data', '1', 3, '2018-05-02 16:47:58', 0, ''),
(30, 8, 2000, 'transfer', 'airtime', '1', 4, '2018-05-02 16:55:11', 0, '3'),
(31, 3, 200, 'transfer', 'airtime', '1', 4, '2018-05-02 17:09:34', 0, '3'),
(32, 3, 1, 'transfer', 'data', '1', 6, '2018-05-02 17:10:12', 0, ''),
(33, 3, 200, 'transfer', 'airtime', '1', 3, '2018-05-02 17:10:52', 0, ''),
(34, 3, 200, 'transfer', 'airtime', '1', 3, '2018-05-02 17:20:57', 0, ''),
(35, 3, 200, 'transfer', 'airtime', '1', 8, '2018-05-02 17:26:39', 0, ''),
(36, 3, 1, 'transfer', 'data', '1', 3, '2018-05-02 17:28:28', 0, ''),
(37, 8, 200, 'transfer', 'airtime', '1', 7, '2018-05-03 00:15:45', 0, '3'),
(38, 8, 200, 'transfer', 'airtime', '1', 6, '2018-05-03 00:21:35', 0, '3'),
(39, 8, 200, 'transfer', 'airtime', '1', 5, '2018-05-03 00:31:15', 0, ''),
(40, 8, 200, 'transfer', 'airtime', '1', 7, '2018-05-03 00:35:58', 0, ''),
(41, 8, 200677000, 'transfer', 'airtime', '1', 5, '2018-05-03 00:37:40', 0, ''),
(42, 8, 200, 'transfer', 'airtime', '1', 3, '2018-05-03 00:39:14', 0, ''),
(43, 8, 200, 'transfer', 'airtime', '1', 6, '2018-05-03 00:41:14', 0, ''),
(44, 8, 200, 'transfer', 'airtime', '1', 3, '2018-05-03 00:41:57', 0, ''),
(45, 8, 1, 'transfer', 'data', '1', 3, '2018-05-04 07:43:34', 0, ''),
(46, 8, 1, 'transfer', 'data', '1', 5, '2018-05-04 07:45:21', 0, ''),
(47, 3, 1, 'transfer', 'data', '1', 4, '2018-05-04 07:52:51', 0, ''),
(48, 3, 1, 'transfer', 'data', '1', 3, '2018-05-04 07:52:59', 0, ''),
(49, 3, 1, 'transfer', 'data', '1', 6, '2018-05-04 07:53:16', 0, ''),
(50, 3, 1, 'transfer', 'data', '1', 4, '2018-05-04 07:54:21', 0, ''),
(51, 3, 1, 'transfer', 'data', '1', 7, '2018-05-04 07:56:13', 0, ''),
(52, 3, 1, 'transfer', 'data', '1', 6, '2018-05-04 08:01:46', 0, ''),
(53, 3, 1, 'transfer', 'data', '1', 5, '2018-05-04 08:06:02', 0, ''),
(54, 3, 1, 'transfer', 'data', '1', 7, '2018-05-04 08:10:24', 0, ''),
(55, 3, 1, 'transfer', 'data', '1', 7, '2018-05-04 08:11:44', 0, ''),
(56, 3, 1, 'transfer', 'data', '1', 5, '2018-05-04 08:13:03', 0, ''),
(57, 3, 1, 'transfer', 'data', '1', 8, '2018-05-04 08:16:25', 0, ''),
(58, 3, 1, 'transfer', 'data', '1', 4, '2018-05-04 08:18:40', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `bmb_person`
--

CREATE TABLE `bmb_person` (
  `person_id` int(255) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `user_id` int(255) NOT NULL,
  `dob` date NOT NULL,
  `account_number` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bmb_person`
--

INSERT INTO `bmb_person` (`person_id`, `first_name`, `last_name`, `user_id`, `dob`, `account_number`) VALUES
(1, 't4t4t54t', 't4gg4tg', 3, '0000-00-00', '345'),
(2, 'test', 'test', 8, '0000-00-00', '9876545678');

-- --------------------------------------------------------

--
-- Table structure for table `bmb_plan`
--

CREATE TABLE `bmb_plan` (
  `plan_id` int(255) NOT NULL,
  `plan_name` varchar(20) NOT NULL,
  `plan_amount` varchar(20) NOT NULL,
  `plan_type_id` int(50) NOT NULL,
  `bonus` varchar(60) NOT NULL,
  `validity` varchar(60) NOT NULL,
  `plan_name_amount` varchar(60) NOT NULL,
  `status` int(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bmb_plan`
--

INSERT INTO `bmb_plan` (`plan_id`, `plan_name`, `plan_amount`, `plan_type_id`, `bonus`, `validity`, `plan_name_amount`, `status`) VALUES
(1, '10mb', '100', 1, '20mb', '30 days', '10mb (N200)', 0),
(2, '20mb', '100', 1, '20mb', '30 days', '20mb (N400)', 0),
(3, 'MTN (N300)', '300', 3, '', '', '', 0),
(4, 'GLO (N300)', '300', 4, '', '', '', 0),
(5, 'dfghjkl', '5678', 2, '', '', '', 0),
(6, 'test', '456', 2, '200 mb', '30 days ', '', 0),
(7, 'try', '33', 3, '32mb', '30 days', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bmb_plan_type`
--

CREATE TABLE `bmb_plan_type` (
  `type_id` int(11) NOT NULL,
  `type` enum('data','airtime','bitcoin','') NOT NULL,
  `network` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bmb_plan_type`
--

INSERT INTO `bmb_plan_type` (`type_id`, `type`, `network`) VALUES
(1, 'data', 'glo'),
(2, 'data', 'mtn'),
(3, 'airtime', 'mtn'),
(4, 'airtime', 'glo'),
(8, '', 'uyt'),
(9, '', 'test'),
(10, 'data', 'test'),
(11, '', 'testagain'),
(12, 'data', 'mtn'),
(13, '', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `bmb_rate`
--

CREATE TABLE `bmb_rate` (
  `rate_id` int(255) NOT NULL,
  `plan_id` int(60) NOT NULL,
  `amount` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bmb_sell_airtime`
--

CREATE TABLE `bmb_sell_airtime` (
  `sell_id` int(255) NOT NULL,
  `airtime_brand` varchar(20) NOT NULL,
  `amount` varchar(50) NOT NULL,
  `airtime_pin` varchar(60) NOT NULL,
  `status` varchar(20) NOT NULL,
  `option` varchar(50) NOT NULL,
  `user_id` int(255) NOT NULL,
  `account_name` varchar(200) NOT NULL,
  `account_number` varchar(200) NOT NULL,
  `expected_amount` varchar(200) NOT NULL,
  `balance` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bmb_sell_airtime`
--

INSERT INTO `bmb_sell_airtime` (`sell_id`, `airtime_brand`, `amount`, `airtime_pin`, `status`, `option`, `user_id`, `account_name`, `account_number`, `expected_amount`, `balance`) VALUES
(1, '3', '67', '56789098', '2', 'cash', 3, '', '', '', ''),
(2, '3', '67', '56789098', '2', 'cash', 3, '', '', '', ''),
(3, '3', '67', '56789098', '2', 'cash', 3, '', '', '', ''),
(4, '3', '67', '56789098', '2', 'cash', 3, '', '', '', ''),
(5, '3', '67', '56789098', '2', 'cash', 3, '', '', '', ''),
(6, '3', '67', '56789098', '2', 'cash', 3, '', '', '', ''),
(7, '3', '67', '56789098', '2', 'cash', 3, '', '', '', ''),
(8, '3', '67', '56789098', '2', 'cash', 3, '', '', '', ''),
(9, '3', '67', '56789098', '2', 'cash', 3, '', '', '', ''),
(10, '3', '67', '56789098', '1', 'cash', 3, '', '', '', ''),
(11, '3', '67', '56789098', '1', 'cash', 3, 'hgfd', '09876', '', ''),
(12, '3', '89', '8767', '1', 'cash', 3, 'hgfdfghj', '87654', '', ''),
(13, '3', '56', '987654', '1', 'cash', 3, '87654', '879675645', '', ''),
(14, '3', '8976543', '87654', '1', 'wallet', 3, '', '', '', ''),
(15, '3', '560', '990990000999', '1', 'wallet', 3, '', '', '', ''),
(16, '3', '234', '123456', '1', 'wallet', 3, '', '', '', ''),
(17, '3', '234', '123456', '1', 'wallet', 3, '', '', '', ''),
(18, '3', '300', '3432322323', '1', 'data', 3, '', '', '', ''),
(19, '3', '800', '3432322323', '2', 'data', 3, '', '', '', ''),
(20, '3', '500', '988765434', '2', 'data', 3, '', '', '', ''),
(21, '3', '450', '345678908765', '2', 'data', 3, '', '', '', ''),
(22, '3', '400', '76545678', '2', 'data', 3, '', '', '', ''),
(23, '3', '400', '76545678', '2', 'data', 3, '', '', '', ''),
(24, '3', '400', '76545678', '1', 'data', 3, '', '', '', '100'),
(25, '3', '400', '76545678', '1', 'data', 3, '', '', '', '100'),
(26, '3', '400', '76545678', '2', 'data', 3, '', '', '', '100'),
(27, '3', '400', '76545678', '2', 'data', 3, '', '', '', '100'),
(28, '3', '100', '76545678', '1', 'wallet', 3, '', '', '', ''),
(29, '3', '100', '56789098', '1', 'wallet', 3, '', '', '', ''),
(30, '3', '400', '56789098', '1', 'wallet', 3, '', '', '', ''),
(31, '3', '100', '7799899', '1', 'wallet', 3, '', '', '', ''),
(32, '3', '300', '76545678', '1', 'wallet', 3, '', '', '', ''),
(33, '3', '100', '76545678', '1', 'wallet', 3, '', '', '', ''),
(34, '3', '100', '123456', '1', 'wallet', 3, '', '', '', ''),
(35, '3', '67', '56789098', '1', 'wallet', 3, '', '', '', ''),
(36, '3', '100', '56789098', '1', 'wallet', 3, '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `bmb_user`
--

CREATE TABLE `bmb_user` (
  `id` int(255) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `access_level` int(50) NOT NULL,
  `auth` varchar(50) NOT NULL,
  `status` int(5) NOT NULL,
  `auth_key` varchar(200) NOT NULL,
  `password_hash` varchar(200) NOT NULL,
  `password_reset_token` varchar(200) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `wallet` int(255) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bmb_user`
--

INSERT INTO `bmb_user` (`id`, `username`, `password`, `access_level`, `auth`, `status`, `auth_key`, `password_hash`, `password_reset_token`, `created_at`, `updated_at`, `wallet`) VALUES
(3, 'demo', '', 0, '', 10, 'DAeZBHPJtgP79K11uOkjuY7Pm9qcqwM1', '$2y$13$bALWS4jJCxPAJK3NuAQ7Q.ohjx5n5rWt0AIywhcpi7xSp31ovrQNe', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 41499),
(4, 'demo2', '', 0, '', 10, 'Q01KPW5Ffknux_4nUBoW2q8lAAwJrbHY', '$2y$13$e3RDcd4hTI/UQvdF1r1oK.At4MZ6EN9kxnyGSiqxtI4R0dxLm4Wri', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(5, 'demo23', '', 0, '', 10, 'dTqgcWl9UHQ3BKC8KEc5pz1F-33Df-oQ', '$2y$13$QH5inFZhLLCElKzDWclWg.IwaD4tG019Dud0n47HjExfLsfH1PwpC', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(6, 'demo237', '', 0, '', 10, 'EmXDzhkt0K2zXnbCtbhqye2dbRWC_1b4', '$2y$13$/0TpoUoAPBdl8BoomElWvOWTSc/dPFv2A5Vq98CnSXapemIpbsUhW', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(7, 'fake', '', 0, '', 10, 'pwkmK5lw-5tgJdiXsP_e5BQuOMaQqaGn', '$2y$13$UGTTOt0Htu.9JVo9ju8eRe2qAuyBr2Irb4rhu6Gtr01KI8YYrU7fS', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(8, 'guest', '', 0, '', 10, '4TWELzVJCeXFgMhcOuXD98Nrshh3oU2n', '$2y$13$JJqE4BQe/XrGLT3TYRRW6.96tix6N9y8IgMJwWtBjP1bYSXKui37G', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bmb_voucher`
--

CREATE TABLE `bmb_voucher` (
  `voucher_id` int(255) NOT NULL,
  `pin` varchar(60) NOT NULL,
  `amount` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT '0',
  `user_id` int(255) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bmb_voucher`
--

INSERT INTO `bmb_voucher` (`voucher_id`, `pin`, `amount`, `status`, `user_id`, `date_created`) VALUES
(19, 'vOPyLo', '100', '0', 3, '2018-01-18 23:50:56'),
(20, 'zMylZU', '100', '0', 3, '2018-01-18 23:50:56'),
(21, 'yA1dWW', '100', '0', 3, '2018-01-18 23:50:56'),
(22, 'pgyzAHaVMEj8lUo', '100', '0', 3, '2018-01-18 23:52:14'),
(23, 'DhJKaTnipXQVLfq', '100', '0', 3, '2018-01-18 23:52:14'),
(24, 'D6r6ELxXat3gkgU', '100', '0', 3, '2018-01-18 23:52:14'),
(25, '7JC5UkeIA01aRA1', '200', '0', 3, '2018-01-27 14:53:26'),
(26, 'wQUCK1tYKdcmsmu', '200', '0', 3, '2018-01-27 14:53:26'),
(27, '3LLiD53YBeCKKsd', '200', '0', 3, '2018-01-27 14:53:26'),
(28, 'L0MBcSsoeoWZy40', '200', '0', 3, '2018-01-27 14:53:26'),
(29, '4lfAttFPxmpCUKA', '200', '0', 3, '2018-01-27 14:53:26'),
(30, 'ivsZlqY8PNoJP5T', '200', '0', 3, '2018-01-27 14:53:26'),
(31, 'pZBTzs1iL7bIcV7', '200', '0', 3, '2018-01-27 14:53:26'),
(32, '0j4s1NEgQ3HdzGl', '100', '0', 3, '2018-01-27 14:53:26'),
(33, 'mPARj9a35ECCwM6', '200', '0', 3, '2018-01-27 14:53:26'),
(34, '5To5lUJP1kynaSo', '200', '0', 3, '2018-01-27 14:53:26'),
(35, 'san57wHZXm7kb5P', '100', '0', 3, '2018-02-10 09:01:05'),
(36, 'a7lYGI5uQb8XM1z', '100', '0', 3, '2018-02-10 09:01:05'),
(37, 'huyvnqcvwbjji6j', '100', '0', 3, '2018-02-10 09:01:05'),
(38, 'LpYMuqW5zXV33yf', '100', '0', 3, '2018-02-10 09:01:05'),
(39, 'bsri3wa4OVQtMHx', '100', '0', 3, '2018-02-10 09:01:05'),
(40, '3WmUSwJbio5rNj9', '100', '0', 3, '2018-02-10 09:01:05'),
(41, 'jO4fO3DE6xyoPzh', '200', '1', 3, '2018-02-10 09:01:05'),
(42, 'feYpEnIoCo8zuA0', '100', '0', 3, '2018-02-10 09:01:05'),
(43, 'sKoWGMg2bzlUvda', '100', '0', 3, '2018-02-10 09:01:05'),
(44, 'FuAuI9kyt5u8p10', '100', '0', 3, '2018-02-10 09:01:05'),
(45, 'TANptiALtKClACk', '100', '0', 3, '2018-02-12 21:12:40'),
(46, '2X7mlEkvqS8nEj7', '100', '0', 3, '2018-02-12 21:12:40'),
(47, 'QrQ3giKyA4zue0U', '100', '0', 3, '2018-02-18 12:19:31'),
(48, 'EYbKcyPccjr5LB2', '100', '0', 3, '2018-02-18 12:19:31'),
(49, '0uj3ckLRqAzlLZJ', '100', '0', 3, '2018-02-18 12:19:31'),
(50, 'E4hTCarrEylSDXB', '100', '0', 3, '2018-02-18 12:19:31'),
(51, 'TpItGo38vpl5uNz', '100', '0', 3, '2018-02-18 12:19:31'),
(52, 'jU8Py3JpqwZ07Ec', '200', '0', 3, '2018-03-16 14:29:11'),
(53, 'CI5bA8SCbcVO9BY', '200', '1', 3, '2018-03-16 14:29:11'),
(54, '9M4iYnEbODtPenV', '100', '0', 3, '2018-04-01 13:08:28'),
(55, 'MI4tqvgnqRsOWyL', '100', '0', 3, '2018-04-01 13:08:28'),
(56, 'cEXlB1CeWsL4Nsm', '100', '0', 3, '2018-04-01 13:08:28'),
(57, '732496627476472', '100', '0', 3, '2018-04-09 21:33:51'),
(58, '888305116330109', '200', '0', 3, '2018-04-09 21:33:51');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bmb_admin_user`
--
ALTER TABLE `bmb_admin_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bmb_contact`
--
ALTER TABLE `bmb_contact`
  ADD PRIMARY KEY (`contact_id`),
  ADD KEY `con_user_id` (`user_id`);

--
-- Indexes for table `bmb_order`
--
ALTER TABLE `bmb_order`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `orderuser` (`user_id`),
  ADD KEY `orderplan` (`plan_id`);

--
-- Indexes for table `bmb_person`
--
ALTER TABLE `bmb_person`
  ADD PRIMARY KEY (`person_id`),
  ADD KEY `personuser` (`user_id`);

--
-- Indexes for table `bmb_plan`
--
ALTER TABLE `bmb_plan`
  ADD PRIMARY KEY (`plan_id`),
  ADD KEY `plantype` (`plan_type_id`);

--
-- Indexes for table `bmb_plan_type`
--
ALTER TABLE `bmb_plan_type`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `bmb_rate`
--
ALTER TABLE `bmb_rate`
  ADD PRIMARY KEY (`rate_id`),
  ADD KEY `rateplan` (`plan_id`);

--
-- Indexes for table `bmb_sell_airtime`
--
ALTER TABLE `bmb_sell_airtime`
  ADD PRIMARY KEY (`sell_id`);

--
-- Indexes for table `bmb_user`
--
ALTER TABLE `bmb_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bmb_voucher`
--
ALTER TABLE `bmb_voucher`
  ADD PRIMARY KEY (`voucher_id`),
  ADD KEY `voucheruser` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bmb_admin_user`
--
ALTER TABLE `bmb_admin_user`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `bmb_contact`
--
ALTER TABLE `bmb_contact`
  MODIFY `contact_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `bmb_order`
--
ALTER TABLE `bmb_order`
  MODIFY `order_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `bmb_person`
--
ALTER TABLE `bmb_person`
  MODIFY `person_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bmb_plan`
--
ALTER TABLE `bmb_plan`
  MODIFY `plan_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `bmb_plan_type`
--
ALTER TABLE `bmb_plan_type`
  MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `bmb_rate`
--
ALTER TABLE `bmb_rate`
  MODIFY `rate_id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bmb_sell_airtime`
--
ALTER TABLE `bmb_sell_airtime`
  MODIFY `sell_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `bmb_user`
--
ALTER TABLE `bmb_user`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `bmb_voucher`
--
ALTER TABLE `bmb_voucher`
  MODIFY `voucher_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bmb_contact`
--
ALTER TABLE `bmb_contact`
  ADD CONSTRAINT `con_user_id` FOREIGN KEY (`user_id`) REFERENCES `bmb_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bmb_order`
--
ALTER TABLE `bmb_order`
  ADD CONSTRAINT `orderuser` FOREIGN KEY (`user_id`) REFERENCES `bmb_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bmb_person`
--
ALTER TABLE `bmb_person`
  ADD CONSTRAINT `personuser` FOREIGN KEY (`user_id`) REFERENCES `bmb_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bmb_plan`
--
ALTER TABLE `bmb_plan`
  ADD CONSTRAINT `plantype` FOREIGN KEY (`plan_type_id`) REFERENCES `bmb_plan_type` (`type_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bmb_rate`
--
ALTER TABLE `bmb_rate`
  ADD CONSTRAINT `rateplan` FOREIGN KEY (`plan_id`) REFERENCES `bmb_plan` (`plan_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bmb_voucher`
--
ALTER TABLE `bmb_voucher`
  ADD CONSTRAINT `voucheruser` FOREIGN KEY (`user_id`) REFERENCES `bmb_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
