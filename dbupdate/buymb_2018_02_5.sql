-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 05, 2018 at 11:08 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `buymb`
--

-- --------------------------------------------------------

--
-- Table structure for table `bmb_contact`
--

CREATE TABLE `bmb_contact` (
  `contact_id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `status` int(5) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bmb_contact`
--

INSERT INTO `bmb_contact` (`contact_id`, `user_id`, `email`, `address`, `phone_number`, `status`) VALUES
(1, 6, '', '', '0987654345678', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bmb_order`
--

CREATE TABLE `bmb_order` (
  `order_id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `plan_id` int(255) NOT NULL,
  `payment_mode` varchar(50) NOT NULL,
  `order_type` enum('data','airtime','bitcoin','') NOT NULL,
  `status` varchar(50) NOT NULL,
  `administrator` int(100) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `airtime_id` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bmb_order`
--

INSERT INTO `bmb_order` (`order_id`, `user_id`, `plan_id`, `payment_mode`, `order_type`, `status`, `administrator`, `date_created`, `airtime_id`) VALUES
(1, 3, 2, 'wallet', 'data', '2', 1, '2018-01-10 00:00:00', 0),
(2, 3, 1, 'wallet', 'data', '2', 0, '0000-00-00 00:00:00', 0),
(3, 3, 1, 'wallet', 'data', '2', 0, '2018-01-10 20:22:00', 0),
(4, 3, 2, 'wallet', 'data', '2', 0, '2018-01-10 20:25:18', 0),
(5, 3, 1, 'wallet', 'data', '1', 0, '2018-01-10 20:39:46', 0),
(6, 3, 1, 'wallet', 'data', '1', 0, '2018-01-10 21:38:37', 0),
(7, 3, 1, 'voucher', 'data', '1', 0, '2018-01-10 21:43:19', 0),
(8, 3, 1, 'voucher', 'data', '1', 0, '2018-01-10 21:44:31', 0),
(9, 3, 1, 'voucher', 'data', '1', 0, '2018-01-10 21:46:21', 0),
(10, 3, 1, 'voucher', 'data', '1', 0, '2018-01-10 21:47:15', 0),
(11, 3, 1, 'wallet', 'data', '1', 0, '2018-01-11 10:30:09', 0),
(12, 3, 2, 'wallet', 'data', '1', 0, '2018-01-11 11:01:33', 0),
(13, 3, 1, 'wallet', 'data', '1', 0, '2018-01-11 11:07:45', 0),
(14, 3, 3, 'wallet', 'data', '1', 0, '2018-01-11 12:34:48', 0),
(15, 3, 3, 'wallet', 'airtime', '1', 0, '2018-01-11 12:59:26', 0),
(16, 3, 3, 'voucher', 'airtime', '1', 0, '2018-01-11 13:05:47', 0),
(17, 3, 3, 'wallet', 'airtime', '1', 0, '2018-01-11 15:16:50', 0),
(18, 3, 1, 'wallet', 'data', '1', 0, '2018-01-11 17:18:06', 0),
(19, 3, 2, 'voucher', 'data', '1', 0, '2018-01-11 17:20:26', 0),
(20, 3, 3, 'wallet', 'airtime', '1', 0, '2018-01-12 12:03:51', 0),
(21, 3, 3, '', 'data', '1', 0, '2018-01-12 13:14:10', 0),
(22, 3, 3, 'airtime', 'data', '0', 0, '2018-01-12 13:17:55', 1),
(23, 3, 2, 'wallet', 'data', '1', 0, '2018-01-18 21:07:22', 0),
(24, 3, 3, 'airtime', 'data', '0', 0, '2018-01-18 21:09:06', 2),
(25, 3, 1, 'card', 'data', '1', 0, '2018-01-27 22:13:33', 0),
(26, 3, 1, 'card', 'data', '1', 0, '2018-01-27 22:15:53', 0),
(27, 3, 1, 'card', 'data', '1', 0, '2018-01-27 22:16:38', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bmb_person`
--

CREATE TABLE `bmb_person` (
  `person_id` int(255) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `user_id` int(255) NOT NULL,
  `dob` date NOT NULL,
  `account_number` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bmb_plan`
--

CREATE TABLE `bmb_plan` (
  `plan_id` int(255) NOT NULL,
  `plan_name` varchar(20) NOT NULL,
  `plan_amount` varchar(20) NOT NULL,
  `plan_type_id` int(50) NOT NULL,
  `bonus` varchar(60) NOT NULL,
  `validity` varchar(60) NOT NULL,
  `plan_name_amount` varchar(60) NOT NULL,
  `status` int(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bmb_plan`
--

INSERT INTO `bmb_plan` (`plan_id`, `plan_name`, `plan_amount`, `plan_type_id`, `bonus`, `validity`, `plan_name_amount`, `status`) VALUES
(1, '10mb', '200', 1, '20mb', '30 days', '10mb (N200)', 0),
(2, '20mb', '400', 1, '20mb', '30 days', '20mb (N400)', 0),
(3, 'MTN (N300)', '300', 3, '', '', '', 0),
(4, 'GLO (N300)', '300', 4, '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bmb_plan_type`
--

CREATE TABLE `bmb_plan_type` (
  `type_id` int(11) NOT NULL,
  `type` enum('data','airtime','bitcoin','') NOT NULL,
  `network` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bmb_plan_type`
--

INSERT INTO `bmb_plan_type` (`type_id`, `type`, `network`) VALUES
(1, 'data', 'glo'),
(2, 'data', 'mtn'),
(3, 'airtime', 'mtn'),
(4, 'airtime', 'glo');

-- --------------------------------------------------------

--
-- Table structure for table `bmb_rate`
--

CREATE TABLE `bmb_rate` (
  `rate_id` int(255) NOT NULL,
  `plan_id` int(60) NOT NULL,
  `amount` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bmb_sell_airtime`
--

CREATE TABLE `bmb_sell_airtime` (
  `sell_id` int(255) NOT NULL,
  `airtime_brand` varchar(20) NOT NULL,
  `amount` varchar(50) NOT NULL,
  `airtime_pin` varchar(60) NOT NULL,
  `status` varchar(20) NOT NULL,
  `option` varchar(50) NOT NULL,
  `user_id` int(255) NOT NULL,
  `account_name` varchar(200) NOT NULL,
  `account_number` varchar(200) NOT NULL,
  `expected_amount` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bmb_sell_airtime`
--

INSERT INTO `bmb_sell_airtime` (`sell_id`, `airtime_brand`, `amount`, `airtime_pin`, `status`, `option`, `user_id`, `account_name`, `account_number`, `expected_amount`) VALUES
(1, '3', '67', '56789098', '1', 'cash', 3, '', '', ''),
(2, '3', '67', '56789098', '1', 'cash', 3, '', '', ''),
(3, '3', '67', '56789098', '1', 'cash', 3, '', '', ''),
(4, '3', '67', '56789098', '1', 'cash', 3, '', '', ''),
(5, '3', '67', '56789098', '1', 'cash', 3, '', '', ''),
(6, '3', '67', '56789098', '1', 'cash', 3, '', '', ''),
(7, '3', '67', '56789098', '1', 'cash', 3, '', '', ''),
(8, '3', '67', '56789098', '1', 'cash', 3, '', '', ''),
(9, '3', '67', '56789098', '1', 'cash', 3, '', '', ''),
(10, '3', '67', '56789098', '1', 'cash', 3, '', '', ''),
(11, '3', '67', '56789098', '1', 'cash', 3, 'hgfd', '09876', ''),
(12, '3', '89', '8767', '1', 'cash', 3, 'hgfdfghj', '87654', ''),
(13, '3', '56', '987654', '1', 'cash', 3, '87654', '879675645', ''),
(14, '3', '8976543', '87654', '1', 'wallet', 3, '', '', ''),
(15, '3', '560', '990990000999', '1', 'wallet', 3, '', '', ''),
(16, '3', '234', '123456', '1', 'wallet', 3, '', '', ''),
(17, '3', '234', '123456', '1', 'wallet', 3, '', '', ''),
(18, '3', '300', '3432322323', '1', 'data', 3, '', '', ''),
(19, '3', '800', '3432322323', '2', 'data', 3, '', '', ''),
(20, '3', '500', '988765434', '2', 'data', 3, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `bmb_user`
--

CREATE TABLE `bmb_user` (
  `id` int(255) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `access_level` int(50) NOT NULL,
  `auth` varchar(50) NOT NULL,
  `status` int(5) NOT NULL,
  `auth_key` varchar(200) NOT NULL,
  `password_hash` varchar(200) NOT NULL,
  `password_reset_token` varchar(200) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `wallet` int(255) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bmb_user`
--

INSERT INTO `bmb_user` (`id`, `username`, `password`, `access_level`, `auth`, `status`, `auth_key`, `password_hash`, `password_reset_token`, `created_at`, `updated_at`, `wallet`) VALUES
(3, 'demo', '', 0, '', 10, 'DAeZBHPJtgP79K11uOkjuY7Pm9qcqwM1', '$2y$13$bALWS4jJCxPAJK3NuAQ7Q.ohjx5n5rWt0AIywhcpi7xSp31ovrQNe', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2800),
(4, 'demo2', '', 0, '', 10, 'Q01KPW5Ffknux_4nUBoW2q8lAAwJrbHY', '$2y$13$e3RDcd4hTI/UQvdF1r1oK.At4MZ6EN9kxnyGSiqxtI4R0dxLm4Wri', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(5, 'demo23', '', 0, '', 10, 'dTqgcWl9UHQ3BKC8KEc5pz1F-33Df-oQ', '$2y$13$QH5inFZhLLCElKzDWclWg.IwaD4tG019Dud0n47HjExfLsfH1PwpC', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(6, 'demo237', '', 0, '', 10, 'EmXDzhkt0K2zXnbCtbhqye2dbRWC_1b4', '$2y$13$/0TpoUoAPBdl8BoomElWvOWTSc/dPFv2A5Vq98CnSXapemIpbsUhW', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bmb_voucher`
--

CREATE TABLE `bmb_voucher` (
  `voucher_id` int(255) NOT NULL,
  `pin` varchar(60) NOT NULL,
  `amount` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT '0',
  `user_id` int(255) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bmb_voucher`
--

INSERT INTO `bmb_voucher` (`voucher_id`, `pin`, `amount`, `status`, `user_id`, `date_created`) VALUES
(19, 'vOPyLo', '100', '0', 3, '2018-01-18 23:50:56'),
(20, 'zMylZU', '100', '0', 3, '2018-01-18 23:50:56'),
(21, 'yA1dWW', '100', '0', 3, '2018-01-18 23:50:56'),
(22, 'pgyzAHaVMEj8lUo', '100', '0', 3, '2018-01-18 23:52:14'),
(23, 'DhJKaTnipXQVLfq', '100', '0', 3, '2018-01-18 23:52:14'),
(24, 'D6r6ELxXat3gkgU', '100', '0', 3, '2018-01-18 23:52:14'),
(25, '7JC5UkeIA01aRA1', '200', '0', 3, '2018-01-27 14:53:26'),
(26, 'wQUCK1tYKdcmsmu', '200', '0', 3, '2018-01-27 14:53:26'),
(27, '3LLiD53YBeCKKsd', '200', '0', 3, '2018-01-27 14:53:26'),
(28, 'L0MBcSsoeoWZy40', '200', '0', 3, '2018-01-27 14:53:26'),
(29, '4lfAttFPxmpCUKA', '200', '0', 3, '2018-01-27 14:53:26'),
(30, 'ivsZlqY8PNoJP5T', '200', '0', 3, '2018-01-27 14:53:26'),
(31, 'pZBTzs1iL7bIcV7', '200', '0', 3, '2018-01-27 14:53:26'),
(32, '0j4s1NEgQ3HdzGl', '200', '0', 3, '2018-01-27 14:53:26'),
(33, 'mPARj9a35ECCwM6', '200', '0', 3, '2018-01-27 14:53:26'),
(34, '5To5lUJP1kynaSo', '200', '0', 3, '2018-01-27 14:53:26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bmb_contact`
--
ALTER TABLE `bmb_contact`
  ADD PRIMARY KEY (`contact_id`),
  ADD KEY `con_user_id` (`user_id`);

--
-- Indexes for table `bmb_order`
--
ALTER TABLE `bmb_order`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `orderuser` (`user_id`),
  ADD KEY `orderplan` (`plan_id`);

--
-- Indexes for table `bmb_person`
--
ALTER TABLE `bmb_person`
  ADD PRIMARY KEY (`person_id`),
  ADD KEY `personuser` (`user_id`);

--
-- Indexes for table `bmb_plan`
--
ALTER TABLE `bmb_plan`
  ADD PRIMARY KEY (`plan_id`),
  ADD KEY `plantype` (`plan_type_id`);

--
-- Indexes for table `bmb_plan_type`
--
ALTER TABLE `bmb_plan_type`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `bmb_rate`
--
ALTER TABLE `bmb_rate`
  ADD PRIMARY KEY (`rate_id`),
  ADD KEY `rateplan` (`plan_id`);

--
-- Indexes for table `bmb_sell_airtime`
--
ALTER TABLE `bmb_sell_airtime`
  ADD PRIMARY KEY (`sell_id`);

--
-- Indexes for table `bmb_user`
--
ALTER TABLE `bmb_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bmb_voucher`
--
ALTER TABLE `bmb_voucher`
  ADD PRIMARY KEY (`voucher_id`),
  ADD KEY `voucheruser` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bmb_contact`
--
ALTER TABLE `bmb_contact`
  MODIFY `contact_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `bmb_order`
--
ALTER TABLE `bmb_order`
  MODIFY `order_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `bmb_person`
--
ALTER TABLE `bmb_person`
  MODIFY `person_id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bmb_plan`
--
ALTER TABLE `bmb_plan`
  MODIFY `plan_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `bmb_plan_type`
--
ALTER TABLE `bmb_plan_type`
  MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `bmb_rate`
--
ALTER TABLE `bmb_rate`
  MODIFY `rate_id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bmb_sell_airtime`
--
ALTER TABLE `bmb_sell_airtime`
  MODIFY `sell_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `bmb_user`
--
ALTER TABLE `bmb_user`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `bmb_voucher`
--
ALTER TABLE `bmb_voucher`
  MODIFY `voucher_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `bmb_contact`
--
ALTER TABLE `bmb_contact`
  ADD CONSTRAINT `con_user_id` FOREIGN KEY (`user_id`) REFERENCES `bmb_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bmb_order`
--
ALTER TABLE `bmb_order`
  ADD CONSTRAINT `orderplan` FOREIGN KEY (`plan_id`) REFERENCES `bmb_plan` (`plan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `orderuser` FOREIGN KEY (`user_id`) REFERENCES `bmb_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bmb_person`
--
ALTER TABLE `bmb_person`
  ADD CONSTRAINT `personuser` FOREIGN KEY (`user_id`) REFERENCES `bmb_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bmb_plan`
--
ALTER TABLE `bmb_plan`
  ADD CONSTRAINT `plantype` FOREIGN KEY (`plan_type_id`) REFERENCES `bmb_plan_type` (`type_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bmb_rate`
--
ALTER TABLE `bmb_rate`
  ADD CONSTRAINT `rateplan` FOREIGN KEY (`plan_id`) REFERENCES `bmb_plan` (`plan_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bmb_voucher`
--
ALTER TABLE `bmb_voucher`
  ADD CONSTRAINT `voucheruser` FOREIGN KEY (`user_id`) REFERENCES `bmb_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
